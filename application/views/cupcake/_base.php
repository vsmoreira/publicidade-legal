<!DOCTYPE html> 
<html xmlns="https://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" xmlns:fb="https://www.facebook.com/2008/fbml">
	<head>
		<title><?=isset($seo) ? $seo['title'] : 'OCA - Ordem de Confecção de Arte'?></title>
		<meta name="description" content=""/>
		<meta name="keywords" content="<?=isset($seo) ? $seo['keywords'] : ''?>" />
		<meta name="description" content="<?=isset($seo) ? $seo['description'] : ''?>" />
		<meta name="copyright" content="<?=isset($seo) ? $seo['copyright'] : ''?>" />
		<meta name="loadtime" content="<?=isset($seo) ? $seo['loadtime'] : ''?>" />
		<meta http-equiv="content-type" content="text/html; charset=UTF-8" />

	    <link rel="stylesheet" type="text/css" href="/media/cupcake/style/reset.css"> 
	    <link rel="stylesheet" type="text/css" href="/media/cupcake/style/root.css"> 
	    <link rel="stylesheet" type="text/css" href="/media/cupcake/style/grid.css"> 
	    <link rel="stylesheet" type="text/css" href="/media/cupcake/style/typography.css"> 
	    <link rel="stylesheet" type="text/css" href="/media/cupcake/style/jquery-ui.css">
	    <link rel="stylesheet" type="text/css" href="/media/cupcake/style/jquery-plugin-base.css">
	    <link rel="stylesheet" type="text/css" href="/media/cupcake/style/prettyPhoto.css">
	    
	    <!--[if IE 7]>	<link rel="stylesheet" type="text/css" href="style/ie7-style.css" />	<![endif]-->
	    
		<script type="text/javascript" src="/media/cupcake/js/jquery-ui/jquery-1.7.2.min.js"></script>
		<script type="text/javascript" src="/media/cupcake/js/jquery-ui/jquery-ui-1.8.22.custom.min.js"></script>

		<script type="text/javascript" src="/media/cupcake/js/jquery-settings.js"></script>
		<script type="text/javascript" src="/media/cupcake/js/jquery.tablesorter.min.js"></script>
		<script type="text/javascript" src="/media/cupcake/js/jquery.tipsy.js"></script>
		<script type="text/javascript" src="/media/cupcake/js/jquery.wysiwyg.js"></script>
		<script type="text/javascript" src="/media/cupcake/js/fullcalendar.min.js"></script>
		
		<script type="text/javascript" src="/media/cupcake/js/jquery.uniform.min.js"></script>
		<script type="text/javascript" src="/media/cupcake/js/prettyphotomods.js"></script>
		
			<script src="/media/cupcake/js/jsapi.js" type="text/javascript"></script>
		
		<?
		/**
		 * 
		<link rel="stylesheet" type="text/css" href="/media/cupcake/style/ui-lightness/jquery-ui-1.8.22.custom.css" />
		<script type="text/javascript" src="/media/cupcake/js/jquery.ui.core.js"></script>
		<script type="text/javascript" src="/media/cupcake/js/jquery.ui.widget.js"></script>
		<script type="text/javascript" src="/media/cupcake/js/jquery.ui.datepicker.js"></script>
		<script type="text/javascript" src="/media/cupcake/js/jquery.prettyPhoto.js"></script>
		<script type="text/javascript" src="/media/cupcake/js/jquery-ui-1.8.11.custom.min.js"></script>
		<script type="text/javascript" src="/media/cupcake/js/toogle.js"></script>
		<script type="text/javascript" src="/media/cupcake/js/raphael.js"></script>
		<script type="text/javascript" src="/media/cupcake/js/analytics.js"></script>
		<script type="text/javascript" src="/media/cupcake/js/popup.js"></script>
		<script type="text/javascript" src="/media/cupcake/js/jquery.ui.mouse.js"></script>
		<script type="text/javascript" src="/media/cupcake/js/jquery.ui.slider.js"></script>
		<script type="text/javascript" src="/media/cupcake/js/jquery.ui.tabs.js"></script>
		<script type="text/javascript" src="/media/cupcake/js/jquery.ui.accordion.js"></script>
		<script src="chrome://pixelperfect/content/browserscripts/dom-drag.js"></script>
		<script src="chrome://pixelperfect/content/browserscripts/publicDocumentEvents.js"></script>
		 */
		?>
		<style type="text/css" charset="utf-8">
		/**
		MARCELOS HACKS
		**/
		.dataTables_wrapper .ui-toolbar .ui-state-default {margin-left: 1px;}
		
		.dataTables_wrapper .ui-toolbar { border-top: 1px solid #CBDAE8; }
		.simplebox .titleh { border-bottom: none; }
		
		.ui-tabs-panel .st-form-line {border:none;padding: 8px 8px;}
		
		div.uploader {
		    cursor: pointer;
		    width: 310px;
		}
		div.uploader span.filename {
		    width: 202px;
		}
		.dataTables_length {
		    width: 350px;
		}
		textarea.code{
		    font-family: monospace;
		    height: 100px;
		    width: 500px !important;
		}
		input[disabled]{
			color:#A5A5A5;
		}
		.sexy_select {border: medium none;
		    height: 268px;
		    padding: 0;
		    width: 550px;}
		.sexy_select option{ 
		    background: url("/media/cupcake/img/simplebox-title-bg.png") repeat-x scroll center bottom transparent;
		    border: 1px solid #CBDAE8;
		    color: #40515E;
		    cursor: pointer;
		    font: bold 12px Arial,Helvetica,sans-serif;
		    list-style: none outside none;
		    margin: 0px 5px 2px 0px;
		    padding: 10px;
		}
		.sexy_select option.selected{ 
		    background: none;
		    background-color: #40515E;
		    color: #CBDAE8;
		}
		.popin_buttons {
		    left: 16px;
		    position: relative;
		    top: 28px;
		        width: 200px;
		}
		.hackedtablesorter td {
		    vertical-align: middle !important;
		}
		.hackedtablesorter thead tr .header {
    		background-image: none !important;
		}
		.hackedtablesorter div.uploader {
		    cursor: pointer;
		    width: 85px;
		}
		.hackedtablesorter div.uploader span.filename {
		    display:none;
		}
		.hackedtablesorter div.uploader .action {
		    background: none repeat scroll 0 0 #EDEDED;
		    border: 1px solid #CCCCCC;
		    border-radius: 4px 4px 4px 4px;
		    margin: 0px;
		    padding: 1px;
		    width: 81px;
		}
		.hackedtablesorter .close {
		    float:right;
		}
		.grid360 textarea#body {
			height:96px;
		}
		
		</style>
		
		<?php if(isset($styles)) foreach($styles as $file => $type) { echo HTML::style($file, array('media' => $type), isset($protocolo)?$protocolo:'http'), "\n\t\t"; } ?>		
		<?php if(isset($scripts)) foreach($scripts as $file) { echo HTML::script($file, array(), isset($protocolo)?$protocolo:'http', TRUE), "\n\t\t"; } ?>
		
	</head>
	<body>
		<?=isset($inner_view)?$inner_view:null?>
	</body>
</html>





