<!-- start page title -->
<div class="page-title">
	<div class="in">
		<div class="titlebar">
			<h2><?=strtoupper($page_title)?></h2>
			<p><?=$page_subtitle?></p>			
		</div>
		<div class="shortcuts-icons">
		
			<a class="icon-button" href="<?=$url?>/" style="margin: -3px;"><img width="18" height="18" alt="icon" src="/media/cupcake/img/icons/button/create.png"><span>Adicionar uma nova OCA</span></a>
		
			<?=isset($page_button_refresh) ? '<a href="#" class="shortcut tips" original-title="Refresh"><img width="25" height="25" alt="icon" src="/media/cupcake/img/icons/shortcut/refresh.png"></a>' : '' ?>
			<?=isset($page_button_dashboard) ? '<a href="#" class="shortcut tips" original-title="Dashboard"><img width="25" height="25" alt="icon" src="/media/cupcake/img/icons/shortcut/dashboard.png"></a>' : '' ?>
			<?=isset($page_button_addnew) ? '<a href="#" class="shortcut tips" original-title="Add New"><img width="25" height="25" alt="icon" src="/media/cupcake/img/icons/shortcut/plus.png"></a>' : '' ?>
			<?=isset($page_button_search) ? '<a href="#" class="shortcut tips" original-title="Search"><img width="25" height="25" alt="icon" src="/media/cupcake/img/icons/shortcut/search.png"></a>' : '' ?>
			<?=isset($page_button_help) ? '<a href="#" class="shortcut tips" original-title="Refresh"><img width="25" height="25" alt="icon" src="/media/cupcake/img/icons/shortcut/question.png"></a>' : '' ?>
		</div>
		<div class="clear"></div>
	</div>
</div>
<!-- end page title -->
	
<!-- START CONTENT -->
<div class="content">

	<? include "_result_message.php" ?>
	
	<!-- START TABLE -->
	<div class="simplebox grid740">
	
	<?=isset($page_list_title) ? "<div class='titleh'><h3>{$page_list_title}</h3></div>" : '' ?>	
	
	<div class="dataTables_wrapper" id="example_wrapper">
	
		<div class="fg-toolbar ui-toolbar ui-widget-header ui-corner-tl ui-corner-tr ui-helper-clearfix" style="background: url('/media/cupcake/style/jquery.ui/images/ui-bg_highlight-soft_75_cccccc_1x100.png') repeat-x scroll 10% 0% #EEE;">
			<div id="example_length" class="dataTables_length"></div>
				<form method='get'>
			<div class="dataTables_filter" id="example_filter">
					<label>Busca: <input type="text" name="search" value="<?=isset($_GET['search'])?$_GET['search']:''?>"></label>
					
					<a href="javascript:void(0);" id="advanced_search">Busca avançada</a>
					<script>
					$(function(){
						$('a#advanced_search').bind('click', function(){
							$('div[rel="advanced_search"]').toggle('slideup');
						})
					})
					</script>
			</div>
					<? 
					$show_advanced_search = ($intersect_fields = array_intersect(array_keys($_GET), $advanced_seach_fields));
					?>
					
					<br clear="all"/>
					
					<div rel="advanced_search" style="<?=empty($show_advanced_search)?'display:none;':''?>clear:both;margin-top:10px;boder-top:1px solid #666;padding:10px 0px 0px 0px;border-top:1px solid #ccc;padding-top:10px;margin-top:10px;clear:both;">
						<table style="width:100%;">
							<tr>
								<td style="text-align:right;padding:3px;"><label>Vendedor: <select name="user_seller_id" style="width:180px;"><option></option><?foreach($sources['sellers'] as $key=>$label){ $s=Arr::get($_GET,'user_seller_id')==$key?' selected':''; echo "<option value='{$key}'{$s}>{$label}</option>"; }?></select></label></td>
								<td style="text-align:right;padding:3px;"><label>Designer: <select name="user_designer_id" style="width:180px;"><option></option><?foreach($sources['designers'] as $key=>$label){ $s=Arr::get($_GET,'user_designer_id')==$key?' selected':''; echo "<option value='{$key}'{$s}>{$label}</option>"; }?></select></label></td>
								<td style="text-align:right;padding:3px;"><p>Cliente: <select name="client_id" style="width:180px;"><option></option><?foreach($sources['clients'] as $key=>$label){ $s=Arr::get($_GET,'client_id')==$key?' selected':''; echo "<option value='{$key}'{$s}>{$label}</option>"; }?></select></label></td>
							</tr>
							<tr>
								
								<script>
								
								var fill_book_format = function(data)
								{
									var selected_format_id = <?=isset($_GET['format_id']) && $_GET['format_id'] ? $_GET['format_id'] : -1?>;
									
									$('option','select[name="format_id"]').remove();
									$('select[name="format_id"]').append("<option value=''>selecione...</option>");
									$.each(data.formats, function(i){
										var selected = (selected_format_id==i) ? ' selected ' : '';
										$('select[name="format_id"]').append("<option value='"+i+"'"+selected+">"+data.formats[i]+"</option>");
									});

									$('span', $('select[name="format_id"]').parent()).html($('select[name="format_id"]').find(":selected").text());
								};

								var on_change_book_id = function()
								{
									var url = '/admin/Order/book_formats/' + $(this).val();
									
									$.ajax({
										dataType: "json",
										url: url,
										data: [],
										success: fill_book_format,
										error: function(data) { $('option','select[name="format_id"]').remove(); }
									});
									
									return true;
								};
								
								$(document).ready(function(){
									$('select#book_id').bind('change', on_change_book_id);

									<?
									if(Arr::get($_GET,'book_id'))
									{
										echo "$('select#book_id').trigger('change');";
									}
									?>
								});
					
								</script>
								<td style="text-align:right;padding:3px;"><p>Caderno: <select name="book_id" id="book_id" style="width:180px;"><option></option><?foreach($sources['books'] as $key=>$label){ $s=Arr::get($_GET,'book_id')==$key?' selected':''; echo "<option value='{$key}'{$s}>{$label}</option>"; }?></select></td>
								<td style="text-align:right;padding:3px;"><p>Formato: <select name="format_id" style="width:180px;"></select></td>
								<td style="text-align:right;padding:3px;"><p>Status: <select name="status" style="width:180px;"><option></option><?foreach(Model_Order::$_status as $key=>$label){ $s=Arr::get($_GET,'status')==$key?' selected':''; echo "<option value='{$key}'{$s}>{$label}</option>"; }?></select></td>
							</tr>
							<tr>
								<td style="text-align:right;padding:3px;"><p>Destino: <select name="destination" style="width:180px;"><option></option><?foreach(Model_Order::$_destination as $key=>$label){ $s=Arr::get($_GET,'destination')==$key?' selected':''; echo "<option value='{$key}'{$s}>{$label}</option>"; }?></select></td>
								<td style="text-align:right;padding:3px;"><p>Tipo: <select name="type_ad" style="width:180px;"><option></option><?foreach(Model_Order::$_types_ad as $key=>$label){ $s=Arr::get($_GET,'type_ad')==$key?' selected':''; echo "<option value='{$key}'{$s}>{$label}</option>"; }?></select></td>
								<td style="text-align:right;padding:3px;">
									<script type="text/javascript">
										$(function() { 
												$( "#date_publication" ).datepicker({
													dateFormat: 'dd/mm/yy'
												});
										});
									</script>
									<p>Publicação: 
									<style>
									#ui-datepicker-div {z-index: 900 !important;}
									</style>
										<input id="date_publication" class="datepicker-input tips-right" type="text" style="width:130px;" value="<?=Arr::get($_GET,'date_publication')?>" name="date_publication">
										<div class="clear" style="z-index: 880;"></div>
									</p>
								</td>
							</tr>
							<tr>
								<td style="text-align:right;padding:3px;" colspan="3">
									<input type="submit" class="st-button" value="pesquisar..." id="button" name="button-submit">
								</td>
							</tr>
						</table>
					</div>
				</form>
		</div>
		
		<table cellspacing="0" cellpadding="0" border="0" id="example" class="display data-table">
			<?
			
			$user = Auth::instance()->get_user()->type;
			
			if($list->count() > 0)
			{
				$list_order_field = isset($list_order_field) ? $list_order_field : null;
				$list_order_drctn = isset($list_order_drctn) ? $list_order_drctn : null;
				
				if(isset($labels))
				{
					?>
					<thead>
						<tr>
							<th class="header ui-state-default"><div class="DataTables_sort_wrapper"><?=$labels['id']?><span class="DataTables_sort_icon css_right ui-icon <?=$list_order_field=='id' ? 'ui-icon-triangle-1-n' : 'ui-icon ui-icon-carat-2-n-s'?>"></span></div></th>
							<th class="header ui-state-default"><div class="DataTables_sort_wrapper"><?=$labels['moment_creation']?> / <?=$labels['title']?><span class="DataTables_sort_icon css_right <?=$list_order_field=='moment_creation' ? 'ui-icon-triangle-1-n' : 'ui-icon ui-icon-carat-2-n-s'?>"></span></div></th>
							<th class="header ui-state-default"><div class="DataTables_sort_wrapper"><?=$labels['date_publication']?> / <?=$labels['user_seller_id']?></div></th>
							<th class="header ui-state-default"><div class="DataTables_sort_wrapper"><?=$labels['status']?><span class="DataTables_sort_icon css_right <?=$list_order_field=='status' ? 'ui-icon-triangle-1-n' : 'ui-icon ui-icon-carat-2-n-s'?>"></span></div></th>
							<th class="header ui-state-default"><div class="DataTables_sort_wrapper"><?=$labels['user_designer_id']?><span class="DataTables_sort_icon css_right <?=$list_order_field=='user_designer_id' ? 'ui-icon-triangle-1-n' : 'ui-icon ui-icon-carat-2-n-s'?>"></span></div></th>
							<?
								if($user == 'PREPRESS')
								{
							?>
									<th class="header ui-state-default"><div class="DataTables_sort_wrapper">Arquivo</div></th>
							<?
								}
							?>
						</tr>
					</thead>
					<?
				}
				?>
				<tbody>
				<?
				$la = $list->as_array();
				for($i=$pagination->page_from-1; $i<$pagination->page_to; $i++)
				{
					$item = $la[$i];
					$eo = (!isset($eo)||$eo=='even')?'odd':'even';
					
					$treatments = $item->treatments->find();
					
					?>
					<tr class="gradeA <?=$eo?>">
						<td class="<?=$list_order_field=='id' ? 'sorting_1':''?>"><a href='<?="{$url}/{$item->id}"?>'><?=$item->id?></a></td>
						<td class="<?=$list_order_field=='moment_creation' ? 'sorting_1':''?>">
							<a href='<?="{$url}/{$item->id}"?>'>
							<sub style="font-size:10px;"><?=date('d/m/Y',strtotime($item->moment_creation))?></sub>
							<br>
							<strong><?=$item->title?></strong>
							</a>
						</td>
						<td class="<?=$list_order_field=='seller_username' ? 'sorting_1':''?>">
							<sub style="font-size:10px;"><a href='<?="{$url}/{$item->id}"?>'><?=date('d/m/Y',strtotime($item->date_publication))?></a></sub>
							<br>
							<a href='<?="{$url}/{$item->id}"?>'><?=$item->seller->name?></a>
						</td>
						<td><a href='<?="{$url}/{$item->id}"?>'><?=($item->status != null)? $status[$item->status] : '' ?></a></td>

						<td><a href='<?="{$url}/{$item->id}"?>'><?=$item->designer->name?></a></td>
						<?
							if($user == 'PREPRESS')
							{
						?>
							<td>
								<?
								if($treatments->attachment_raw_path != '' && $item->status == 'COMPLETED')
								{
								?>
									<a href="/admin/Order/download_raw_image/<?=$item->id?>" class="icon-button button-green" target="_blank" onclick="return download_raw_image(this);">Baixar</a>
								<?
								}
								?>
							</td>
						<?
							}
						?>
					</tr>
					<?
				}
				?>
				</tbody>
				<script>
					function download_raw_image(anchor)
					{
						$.ajax({
							dataType: "json",
							url: $(anchor).attr('href'),
							success: function(data) 
								{
									if(data.success)
									{
										location.href = '/' + data.file;
									}
									else
									{
										alert(data.message)
									}
								},
							error: function(data) {alert('Erro desconhecido');}
						});
						return false;
					}
				</script>
				<?
			}
			else
			{
				?>
				<tr class="gradeA"><td colspan="6" align="center"><?=_('No record found')?></td></tr>
				<?
			}
			?>
		</table>
		
		<?=$pagination->render_html()?>
		
	</div>
		
	<br clear="all"/>
	
</div>
<!-- END TABLE -->

</div>
<!-- END CONTENT -->