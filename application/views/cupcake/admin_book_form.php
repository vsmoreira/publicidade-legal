					<!-- start page title -->
					<div class="page-title">
						<div class="in">
							<div class="titlebar">
								<h2><?=strtoupper($page_title)?></h2>
								<p><?=$page_subtitle?></p>			
							</div>
							<div class="shortcuts-icons">
								<a class="icon-button" href="<?=$page_url_list?>" style="margin: -3px;"><img width="18" height="18" alt="icon" src="/media/cupcake/img/icons/button/arrow-left.png"><span><?=(isset($page_back_label))?$page_back_label:_('Back to List')?></span></a>
								<?=isset($page_button_refresh) ? '<a href="#" class="shortcut tips" original-title="Refresh"><img width="25" height="25" alt="icon" src="/media/cupcake/img/icons/shortcut/refresh.png"></a>' : '' ?>
								<?=isset($page_button_dashboard) ? '<a href="#" class="shortcut tips" original-title="Dashboard"><img width="25" height="25" alt="icon" src="/media/cupcake/img/icons/shortcut/dashboard.png"></a>' : '' ?>
								<?=isset($page_button_addnew) ? '<a href="#" class="shortcut tips" original-title="Add New"><img width="25" height="25" alt="icon" src="/media/cupcake/img/icons/shortcut/plus.png"></a>' : '' ?>
								<?=isset($page_button_search) ? '<a href="#" class="shortcut tips" original-title="Search"><img width="25" height="25" alt="icon" src="/media/cupcake/img/icons/shortcut/search.png"></a>' : '' ?>
								<?=isset($page_button_help) ? '<a href="#" class="shortcut tips" original-title="Refresh"><img width="25" height="25" alt="icon" src="/media/cupcake/img/icons/shortcut/question.png"></a>' : '' ?>
							</div>
							<div class="clear"></div>
						</div>
					</div>
					<!-- end page title -->
                
                
                	<!-- START CONTENT -->
                    <div class="content">
                    
                    	<?   
						if($form->message)
	                    {
                    	$message=$form->message;
	                    }
						include "_result_message.php" 
						?>
						<form method="post" enctype='multipart/form-data'>
                    
							<div class="simplebox grid740">
								<div class="titleh">
									<h3><?=$titleh_form?></h3>
									<div class="shortcuts-icons">
										<a original-title="This is Simple Form" class="shortcut tips" href="#"><img src="/media/cupcake/img/icons/shortcut/question.png" alt="icon" height="25" width="25"></a>
									</div>
								</div>
								<div class="body">
								<? 
									$form->render('description');
									$form->render('max_height');
								?>
								</div>
							</div>
							
							
							<div class="simplebox grid740">
								<div class="titleh">
									<h3><?=$titleday_form?></h3>
									<div class="shortcuts-icons">
										<a original-title="This is Simple Form" class="shortcut tips" href="#"><img src="/media/cupcake/img/icons/shortcut/question.png" alt="icon" height="25" width="25"></a>
									</div>
								</div>
								<div class="body">
								<? 
									$form->render('sunday');
									$form->render('monday');
									$form->render('tuesday');
									$form->render('wednesday');
									$form->render('thursday');
									$form->render('friday');
									$form->render('saturday');
								?>
								</div>
							</div>

							
							
							<!-- ----------------------------------- -->
							
							<script>
							var formats = <?php
												$formats = array();
												foreach($form->object->formats->find_all() as $format)
												{
													array_push($formats, array('id'=>$format->pk(),'name'=>$format->name,'width'=>$format->width));
												}
												echo json_encode($formats);
											?>;
							
							$(document).ready(function(){
								
								var counter = 0;
								
								var $list_formats = $('tbody#list_formats');
								
								var add_format_html = function(id, width, name)
								{
									$tritem = $(
										"<tr class='format' rel='"+id+"'>"+
										" <input type='hidden' name='formats[id][]' value='"+(id!=null?id:'')+"'>"+
										" <td><input type='text' name='formats[name][]' value='"+(name!=null?name:'')+"'></td>"+
										" <td><input type='text' name='formats[width][]' value='"+width+"'></td>"+
										" <td><a href='#' class='button-red remove_format' rel='"+id+"'>excluir</a></td>"+
										"</tr>");
									$list_formats.append($tritem);	
								};
								
								$('a.remove_format').live('click', function(){
									if(confirm('Tem certeza que deseja excluir este formato?'))
									{
										$('tr.format[rel="'+$(this).attr('rel')+'"]').hide('slow', function(){
											$(this).remove();
											
											if($("tr.format",$('tr.noitem', $list_formats).parent()).length == 0)
											{
												$('tr.noitem', $list_formats).show();
											}
										});
									}
									return false;
								});
								
								$('a.add_format').live('click', function(){
									add_format_html('new_'+counter, '', '');
									counter++;
									return false;
								});
								
								if(formats.length > 0)
								{
									$('tr.noitem', $list_formats).hide();
									
									$(formats).each(function(i){
										add_format_html(formats[i]['id'], formats[i]['width'], formats[i]['name']);
									});
								}
							});
							</script>
							<style>
								tbody#list_formats tr td {padding:5px;}
								tbody#list_formats tr a {display: block;height: 14px;margin: 1px;text-align: center;width: 40px;}
								tbody#list_formats tr input {width:220px;}
							</style>
							<div style="z-index: 270;" class="simplebox grid740">
	                        	<div class="titleh" style="z-index: 260;">
	                        		<h3>Formatos</h3>
	                        		<div class="shortcuts-icons" style="z-index: 250;">
	                        		<a href="#" class="button-aqua add_format" style="position: relative; top: -3px;">adicionar novo</a>	
	                        		</div>
	                        	</div>
	                        	<div class="body">
	                            <table class="tablesorter" id="myTable"> 
	                            	<thead> 
	                            		<tr>
                            			    <th class="header">Nome</th> 
	                            			<th class="header">Lagura (cm)</th> 
	                            			<th class="header">Excluir</th> 
	                            		</tr> 
	                            	</thead> 
	                                <tbody id="list_formats">
	                                	<tr class="noitem"><td colspan="6">Nenhum formato cadastrado</td></tr>
	                                </tbody> 
	                            </table>
	                        	</div>
							</div>
													
							<!-- ----------------------------------- -->
								
							<? 
								$form->render_buttons();
							?>
							
						</form>
						
						<?
						if(isset($extra_sections))
						{
							foreach($extra_sections as $section)
							{
								echo $section;
							}
						}
						?>
                    
                    </div>
                    
                   						
<!-- END CONTENT -->
