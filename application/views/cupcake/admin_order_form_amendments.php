		<style>
		#myTable tr.odd td {background-color: #F5F5F5;}
		</style>
		<script>
		$(function(){
			$('a.add_new_amendment').unbind().bind('click', function(){
				$('#form_new').toggle('slideup');
				return false;
			});
		})
		</script>
		<div class="simplebox grid740">
        	<div class="titleh">
        		<h3><?=_('Atendimentos, comentários e emendas')?></h3>
        		<div class="shortcuts-icons">
				<?
				if($form->object->status == 'DEVELOPING')
				{
					if(Auth::instance()->get_user()->type == 'DESIGNER')
					{
						?>
		        		<a style="position: relative; top: -3px;" class="button-aqua add_new_amendment" href="#">adicionar novo</a>	
						<?
					}
				}
				?>
        		</div>
        	</div>
        	
        	<div class="body">
        	
			<?
			if($form->object->status == 'DEVELOPING')
			{
				if(Auth::instance()->get_user()->type == 'DESIGNER')
				{
					?>
					<div id="form_new" <?=($form->errors) ? 'style="display:block;"' : 'style="display:none;"'?>>
		
						<div class="st-form-line">	
							<table border="1" style="width:100%;">
								<tbody>
									<tr valign="middle">
										<td rowspan="2" style="width:150px;"><span class="st-labeltext">Preview</span></td>
										<td>
											<div id="uniform-undefined" class="">
												<div class="" id="uniform-undefined">
													<input type="file" style="opacity: 0;" size="19" name="amendment[low]" class="uniform ">
													<span class="action" style="-moz-user-select: none;"></span>
												</div>
											</div>
										</td>
									</tr>
								</tbody>
							</table>
							<div class="clear"></div> 
						</div>
						
						
					
					
						<div class="st-form-line">	
							<table border="1" style="width:100%;">
								<tbody>
									<tr valign="middle">
										<td rowspan="2" style="width:150px;"><span class="st-labeltext">Alta</span></td>
										<td>
											<div id="uniform-undefined" class="">
												<div class="" id="uniform-undefined2">
													<input type="file" style="opacity: 0;" size="19" name="amendment[high]" class="uniform ">
												</div>
											</div>
										</td>
									</tr>
								</tbody>
							</table>
							<div class="clear"></div> 
						</div>
						
		
						<div class="st-form-line">	
							<span class="st-labeltext">Comentário</span>	
							<textarea name="amendment[designer_comment]" class="st-forminput tips-right" id="description_ad" style="width:510px;" rows="3" cols="47" original-title=""><?=isset($_POST['amendment']) && isset($_POST['amendment']['designer_comment']) ? $_POST['amendment']['designer_comment'] : '';?></textarea> 
							<div class="clear"></div>
						</div>
						
						<div class="st-form-line">
						
							<input type="submit" name="button-submit-amendment" id="button" value="Salvar emenda" class="st-button">
							
						</div>
						
					</div>
					<?
				}
			}
			?>

        	
	            <table id="myTable" class="tablesorter"> 
	            	<thead> 
	            		<tr>
	        			    <th width="220" class="">Designer</th> 
	        			    <th width="100" class="">Preview</th> 
	        			    <th width="100" class="">Alta</th> 
	            			<th width="300" class="">Situação/Vendedor/Observações</th> 
	            		</tr> 
	            	</thead> 
	                <tbody id="list_formats">
	                <?

		                $treatments = $form->object->treatments->find_all();
		                
	                	$total_treeatments_shown = 0;
	                	
		                if(count($treatments) > 0)
		                {
			                foreach($treatments as $treatment)
			                {
			                	if(Auth::instance()->get_user()->type=='PREPRESS')
			                	{
			                		if($treatment->status!='ACCEPTED')
				                	{
				                		continue;
				                	}
				                	
				                	if($form->object->user_prepress_id!=Auth::instance()->get_user()->id)
				                	{
				                		?>
	                		            <tr>
	                		                <td colspan="5" style="font-weight:bold;color:red;">
	                		                Apenas o analista de pre-impressao que assumiu esta ordem pode 
	                		                visualizar os arquivos gerados pelo designer. Por favor assuma 
	                		                a ordem ou solicite ao analista que a assumiu o acesso aos arquivos.
	                		            	</td> 
	                		            </tr> 
	                		            <?
	                		            $total_treeatments_shown++;
	                		            continue;
				                	}
			                	}

			                	$total_treeatments_shown++;
			                	
			                	$eo = isset($eo) && $eo=='odd' ? 'even' : 'odd';
			                	?>
				                <tr class="<?=$eo?>" valign="middle"> 
				                	<td>
				                		<b><?=$treatment->designer->username?></b><br><?=date('d/m/Y H:i:s', strtotime($treatment->date_create))?>
				                		<div><a href="javascript:void(0)" class="comment" rel="observation_<?= $treatment->id ?>">veja coment&aacute;rios...</a></div>
				                		
				                		<div id="observation_<?= $treatment->id ?>" style="display:none;position:inherit;" class="albox informationbox"><?= (!empty($treatment->observation))?$treatment->observation:'Sem coment&aacute;rios' ?></div>
				                	</td> 
				                	<td align="center">
				                	<?
				                	if(is_file(DOCROOT.$treatment->attachment_image_path))
				                	{
				                		?>
				                		<a href="/<?=$treatment->attachment_image_path?>" target="_blank"><img src="/media/img/jpeg.png" width="48" height="48"></a>
				                		<?
				                	}
				                	else
				                	{
				                		echo '[removido]';
				                	}
				                	?>
				                	</td> 
				                	<td align="center">
				                	<?
				                	if(is_file(DOCROOT.$treatment->attachment_raw_path))
				                	{
				                		if(Auth::instance()->get_user()->type == 'SELLER')
				                		{
				                			?>
				                			<h1>Não Diponível</h1>
				                			<?
										}
										else 
										{
                					     	?>
                							<a href="/<?=$treatment->attachment_raw_path?>" target="_blank"><img src="/media/img/tiff.png" width="48" height="48"></a>
                							<?
                					    }
				                	}
				                	else
				                	{
				                		echo '[removido]';
				                	}
				                	?>
				                	</td> 
				                	<td colspan="2" align="center" style="vertical-align: middle!important;">
				                	<?
				                	if ($treatment->status == 'WAITING')
				                	{
										if(Auth::instance()->get_user()->type == 'SELLER')
										{
											?>
											<input type="hidden" name="button-accrej-treatment-id" value="<?=$treatment->id?>">
											<input type="hidden" name="button-reject-treatment-reason" value="">
											<input type="submit" name="button-accept-treatment" id="button" value="Aceitar" class="st-button button-blue">
											<input type="submit" name="button-reject-treatment" id="button" value="Rejeitar" class="st-button button-red" onclick="javascript:return get_reason();" >
											<script>
											var get_reason = function()
											{
												$('input[name=\"button-reject-treatment-reason\"]').val(prompt('Informe o motivo da rejeição'));
												
												if($('input[name=\"button-reject-treatment-reason\"]').val() == '')
												{
													alert('Só é possível rejeitar uma emenda informando o motivo da rejeição');
													return false;
												}
												else
												{
													return true;
												}
											};
											</script>
											<?
										}
										else if(Auth::instance()->get_user()->type == 'DESIGNER')
										{
											?>
											<input type="hidden" name="button-remove-treatment-id" value="<?=$treatment->id?>">
											<input type="submit" name="button-remove-treatment" id="button" value="Excluir" class="st-button button-red" onclick="javascript:return confirm('Tem certeza que deseja remover esta emenda?');" >
											<?
										}
									}
									elseif($treatment->status == 'DENIED')
									{
										?>
				                		<b>Rejeitado</b> <br> <?=date('d/m/Y H:i:s', strtotime($treatment->date_response))?>
				                		<div style="border-top:1px solid #D8E4E9;padding:5px;margin-top:5px;">
					                		<b><?=$treatment->seller->username?></b>
					                		<div><a href="javascript:void(0);" class="comment" rel="reason_denial_<?= $treatment->id ?>">veja comentários...</a></div>
				                			<div id="reason_denial_<?= $treatment->id ?>" style="display:none;position:inherit;" class="albox errorbox"><?=$treatment->reason_denial?></div>
				                		</div>
										<?
									}
									else
									{
					                	?>
				                		<b>Atendimento finalizado</b> <br> <?=date('d/m/Y H:i:s', strtotime($form->object->moment_closed))?>
					                	<?
				                	}
				                	?>
				                	</td>
				            	</tr>
			                	<?
			                }
			                

                			                
			                
			                if($total_treeatments_shown==0)
		                	#Aqui eu sei que houveram emendas mas por via de regras elas nao foram exibidas
			                {
		                	?>
		            		<tr>
		        			    <td colspan="5">Nao ha nenhuma emenda disponivel para sua visualzacao</td> 
		            		</tr> 
		                	<?
			                }
		                }
		                else
		                {
		                	?>
		            		<tr>
		        			    <td colspan="5">Nenhum atendimento ou emenda foi feita para esta ordem.</td> 
		            		</tr> 
		                	<?
		                }
	                ?>
	            </table>
	            <script>
	            	$(function(){
	            		$('.comment').bind('click', function(){
	            			$('div#' + $(this).attr('rel')).toggle('slideup');
	            		});
	            	});
	            </script>
        	</div>
		</div>
		