		<div class="wrapper">
			<? include "_header.php"; ?>
			
		    <!-- START MAIN -->
		    <div id="main">
				<? include "_sidebar.php"; ?>
	
		        <!-- START PAGE -->
		        <div id="page">
					<?=isset($inner_view)?$inner_view:null?>
		        </div>
		        <!-- END PAGE -->

			    <div class="clear"></div>
		    </div>
		    <!-- END MAIN -->

			<? include "_footer.php"; ?>
		</div>