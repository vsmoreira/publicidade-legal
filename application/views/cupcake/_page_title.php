
                <!-- start page title -->
                <div class="page-title" style="z-index: 740;">
                	<div class="in" style="z-index: 730;">
                    		<div class="titlebar" style="z-index: 720;">
                    			<h2><?=$title?></h2>
                    			<p><?=$desc?></p>
                    		</div>
                            
                            <div class="shortcuts-icons" style="z-index: 710;">
	                            <?
	                            if(isset($help))
	                            {
	                            	echo '<a original-title="'.__('Tips and help').'" href="'.$help.'" class="shortcut tips"><img width="25" height="25" alt="icon" src="/media/cupcake/img/icons/shortcut/question.png"></a>';
	                            }
	                            ?>
	                            <?
	                            if(isset($history))
	                            {
	                            	echo '<a original-title="'.__('Full history').'" href="'.$history.'" class="shortcut tips"><img width="25" height="25" alt="icon" src="/media/cupcake/img/icons/shortcut/clock.png"></a>';
	                            }
	                            ?>
	                            <?
	                            if(isset($dash))
	                            {
	                            	echo '<a original-title="'.__('Session dhasboard').'" href="'.$dash.'" class="shortcut tips"><img src="/media/cupcake/img/icons/shortcut/dashboard.png" alt="icon" height="25" width="25"></a>';
	                            }
	                            ?>
                            </div>
                            
                            <div class="clear" style="z-index: 700;"></div>
                    </div>
                </div>
                <!-- end page title -->
 
