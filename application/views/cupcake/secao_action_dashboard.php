        <!-- START PAGE -->
        <div id="page">
	        <?=$page_header?>
        
        	<!-- START CONTENT -->
            <div class="content">
                <?
                /***
                <!-- start dashbutton -->
                <div class="grid740" style="z-index: 650;">
                	<a class="dashbutton tips" href="#" original-title="Dashbutton with tipbox"><img width="44" height="32" alt="icon" src="/media/cupcake/img/icons/dashbutton/bubbles.png"><?=__('<b>Support Center</b> contact with support')?></a>
                	<a class="dashbutton" href="#"><img width="44" height="32" alt="icon" src="/media/cupcake/img/icons/dashbutton/graph.png"><?=__('<b>Daily Statistics</b> see whats happened')?></a>
                	<a class="dashbutton" href="#"><img width="44" height="32" alt="icon" src="/media/cupcake/img/icons/dashbutton/image.png"><?=__('<b>Upload A Photo</b> add a new photo')?></a>
                	<a class="dashbutton" href="#"><img width="44" height="32" alt="icon" src="/media/cupcake/img/icons/dashbutton/map.png"><?=__('<b>Google Maps</b> check your location')?></a>
                	<a class="dashbutton" href="#"><img width="44" height="32" alt="icon" src="/media/cupcake/img/icons/dashbutton/personal-folder.png"><?=__('<b>User Profiles</b> user profiles details')?></a>
                	<a class="dashbutton" href="#"><img width="47" height="32" alt="icon" src="/media/cupcake/img/icons/dashbutton/creadit-card.png"><?=__('<b>Creadit Card</b> my credit cards')?></a>
                	<a class="dashbutton" href="#"><img width="39" height="32" alt="icon" src="/media/cupcake/img/icons/dashbutton/settings.png"><?=__('<b>Settings</b> theme settings')?></a>
                	<a class="dashbutton" href="#"><img width="41" height="32" alt="icon" src="/media/cupcake/img/icons/dashbutton/users.png"><?=__('<b>Users</b> new Members')?></a>
                	<a class="dashbutton" href="#"><img width="41" height="32" alt="icon" src="/media/cupcake/img/icons/dashbutton/frames.png"><?=__('<b>Widgets</b> manage your widgets')?></a>
                	<a class="dashbutton" href="#"><img width="32" height="32" alt="icon" src="/media/cupcake/img/icons/dashbutton/applications.png"><?=__('<b>Applications</b> manage your application')?></a>
                   	<div class="clear" style="z-index: 640;"></div>
                </div>
                 * 
                 */
                ?>

				<div class="grid360-left">
					<?
					$TotalContents  = DB::query(Database::SELECT, 'select count(*) as total, language from contents_details group by language')->execute()->as_array();
					$TotalApprovals = DB::query(Database::SELECT, 'select count(*) as total, status from approvals group by status')->execute()->as_array();
					$OnlineContents = DB::query(Database::SELECT, "select count(content_id) as total from contents c, approvals a where a.content_id=c.id and now() between c.published_from and c.published_to and a.status in ('automatically-approved','moderately-approved') group by content_id")->execute()->current();
					$OverallAverage = DB::query(Database::SELECT, 'select avg(rating) as rating from approvals where rating is not null')->execute()->current();
					$LastUpdates    = DB::query(Database::SELECT, 'select now() as now')->execute()->current();
					?>   
					<div class="st-tinytitle">
						<h3><?=__('Statistics')?></h3>
						<p><?=__('Some statistics from application')?></p>
					</div>
					<!-- start statistics codes -->
					<div class="simplebox">
						<div class="titleh"><h3>Statistics</h3></div>
						<div class="body">
							<ul class="statistics">
								<li>Total Contents		<p>	<?
															$arr = array();
															foreach($TotalContents as $app)
															{
																array_push($arr, "<span class='blue'><b>{$app['total']}</b> {$app['language']}</span> ");
															}
															echo implode(' - ',$arr);
															?>
														</p> </li>
								<li>Online Contents		<p>	<span class="blue"><b><?=$OnlineContents['total']?></b> contents</span>	</p>	</li>
								<li>Total Approvals		<p>	<?
															$ok=$wt=$nt=0;
															foreach($TotalApprovals as $app)
															{
																switch($app['status']){
																	case Model_Approval::STATUS_WAITING       : $wt+=$app['total']; break;
																	case Model_Approval::STATUS_AUTO_APPROVED : $ok+=$app['total']; break;
																	case Model_Approval::STATUS_AUTO_REJECTED : $nt+=$app['total']; break;
																	case Model_Approval::STATUS_MDRT_APPROVED : $ok+=$app['total']; break;
																	case Model_Approval::STATUS_MDRT_REJECTED : $nt+=$app['total']; break;
																}
															}
															?> 
															<span class="blue"><b><?=$wt?></b> waiting</span> - <span class="red"><b><?=$wt?></b> unapproved</span> - <span class="green"><b><?=$ok?></b> approved</span>
														</p> </li>
								<li>Overall Average		<p>	<span class="blue"><b><?=$OverallAverage['rating']?></b></span>	</p>	</li>
								<li>Last Updates		<p>	<?=date('d/m/Y H:i:s', strtotime($LastUpdates['now']))?>	</p>	</li>
							</ul>
						</div>
					</div>
					<!-- end statistics codes -->
				</div>


				<div class="grid360-right">
				<div class="st-tinytitle">
				<h3><?=__('Random Graph')?></h3>
				<p><?=__('Important statistical informations about contents and users')?></p>
				</div>
				<!-- start Image Box codes -->
				<div class="simplebox">
					<div class="titleh"><h3><?=__('Content per editorial')?></h3></div>
					<div class="body">
						<!-- start pie chart javascript codes -->        
						<?
						$query = DB::query(Database::SELECT, 'select count(*) as total, e.name from editorial_contents ec, editorials e where e.id=ec.editorial_id group by editorial_id')->execute()->as_array();
						$arrit = array();
						foreach($query as $item){
							array_push($arrit, "['{$item['name']}', {$item['total']}]");
						}
						?>                     
						<script type="text/javascript">
						
						  // Load the Visualization API and the piechart package.
						  google.load('visualization', '1.0', {'packages':['corechart']});
						  
						  // Set a callback to run when the Google Visualization API is loaded.
						  google.setOnLoadCallback(drawChart);
						  
						  // Callback that creates and populates a data table, 
						  // instantiates the pie chart, passes in the data and
						  // draws it.
						  function drawChart() {
						
						  // Create the data table.
						  var data = new google.visualization.DataTable();
						  data.addColumn('string', 'Topping');
						  data.addColumn('number', 'Slices');
						  data.addRows(<?="[".implode(',', $arrit)."]"?>);
						
						  // Set chart options
						  var options = {'title':'<?=__('How much content was published')?>', 'width':350, 'height':205};
						
						  // Instantiate and draw our chart, passing in some options.
						  var chart = new google.visualization.PieChart(document.getElementById('piechart_div'));
						  chart.draw(data, options);
						}
						</script>
						   <!-- end pie chart javascript codes -->   
						    <!-- start chart div -->
						<div id="piechart_div" style="position: relative;"><iframe scrolling="no" frameborder="0" marginheight="0" marginwidth="0"></iframe><div></div></div>
						<!-- end chart div -->
					</div>
				</div>
				<!-- end Image Box codes -->
				</div>





            
            </div>
            <!-- END CONTENT -->
            
        </div>
        <!-- END PAGE -->