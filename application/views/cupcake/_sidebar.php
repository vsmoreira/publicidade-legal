        <!-- START SIDEBAR -->
        <div id="sidebar" style="z-index: 850;">
        	
            <!-- start searchbox -->
            <div id="searchbox" style="z-index: 840;">
				<center>
				<img height="55" src="/media/img/mainheader.png" alt="logo">
				</center>
            <?
            /*
            	<div class="in" style="z-index: 830;">
               	  <form action="" method="post" name="form1" id="form1">
                  	<input type="text" disabled onblur="$(this).attr('class','input')" onfocus="$(this).attr('class','input-hover')" id="textfield" class="input" name="textfield">
               	  </form>
            	</div>
             */
            ?>
            </div>
            <!-- end searchbox -->
            
            <!-- start sidemenu -->
            <div id="sidemenu" style="z-index: 820;">
            	<ul>
            	<?
				$elements = array();
				$files = Kohana::list_files('classes/Controller/Admin');
				
				$user = Auth::instance()->get_user();
				$rgts = ORM::factory('RoleRight')->where('role_id','in', array_keys($user->roles->find_all()->as_array('id')))->find_all()->as_array('right');
				
				
				 $usr_rgts = array();
				 
				foreach ($rgts as $value)
				{
					array_push($usr_rgts, $value->right);	
				}
				
				foreach($files as $file){
					$kontroller = 'Controller_Admin_'.basename($file,".php");
					if( method_exists($kontroller,'rights') )
					{
				        $method = new ReflectionMethod($kontroller, 'rights');
				        $passed = $method->invokeArgs(NULL, array());
						
				        if(!empty($passed['requires']) && in_array($passed['requires'], $usr_rgts))
				        {
				        	$k = $passed['controller'] == Request::current()->controller() ? "class='active'" : NULL;
				        	
				        	while( array_key_exists($passed['order'], $elements)) $passed['order'].='_a';
				        	
					        $elements[$passed['order']] = "<li order='{$passed['order']}' {$k}><a href='{$passed['url']}'><img alt='icon' src='{$passed['icon']}' height='16' width='16'>{$passed['label']}</a></li>";
				        }
					}
				}
				
				ksort($elements);
				foreach($elements as $element)
				{
					echo $element;
				}
            	
            	?>
				</ul>
            </div>
            <!-- end sidemenu -->
        </div>
        <!-- END SIDEBAR -->