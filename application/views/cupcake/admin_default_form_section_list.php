						<div class="simplebox grid740" style="z-index: 460;">
	                    	<div class="titleh" style="z-index: 450;">
		                        <h3><?=$section_title?></h3>
		                        <div class="shortcuts-icons" style="z-index: 440;">
		                        <?
		                        if(isset($add_button))
		                        {
		                        	?>
		                            <a href="<?=$add_button?>" class="shortcut tips" original-title="<?=_('Add New Item')?>"><img width="25" height="25" alt="icon" src="/media/cupcake/img/icons/shortcut/plus.png"></a>
		                        	<?
		                        	
		                        }
		                        /*
		                        	<a href="#" class="shortcut tips" original-title="Info About This Widget"><img width="25" height="25" alt="icon" src="/media/cupcake/img/icons/shortcut/question.png"></a>
		                            <a href="#" class="shortcut tips" original-title="Search"><img width="25" height="25" alt="icon" src="/media/cupcake/img/icons/shortcut/search.png"></a>
		                            <a href="#" class="shortcut tips" original-title="Refresh Data"><img width="25" height="25" alt="icon" src="/media/cupcake/img/icons/shortcut/refresh.png"></a>
		                        */
		                        ?>
		                        </div>
	                        </div>
	                        
                            <table class="tablesorter" id="myTable"> 
                            	<thead> 
                            		<tr> 
                            		<?
                            		foreach($list_titles as $title)
                            		{
                            			?>
                            			<th class="header"><?=$title?></th> 
                            			<?
                            		}
                            		?>
                            		</tr> 
                            	</thead> 
                                
                                <tbody>
                            		<?
                            		if(!empty($list_values))
                            		{
	                            		foreach($list_values as $value)
	                            		{
	                            			echo '<tr>';
		                            		foreach($list_titles as $index=>$title)
		                            		{
		                            			echo isset($value['default_url']) ? "<td><a href='{$value['default_url']}'>{$value[$index]}</a></td>" : "<td>{$value[$index]}</td>"; 
		                            		}
		                            		echo '</tr>';
	                            		}
                            		}
                            		else
                            		{
                            			echo '<tr><td colspan="'.count($list_titles).'" align="center">'.$list_empty_message.'</td></tr>';
                            		}
                            		?>
                                 </tbody> 
                            </table>
	                    </div>
