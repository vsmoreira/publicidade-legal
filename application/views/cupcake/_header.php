	<!-- START HEADER -->
    <div id="header" style="z-index: 950;">
    
    
    	<!-- logo -->
    	<div class="logo" style="padding:18px 0px;pasition:relative;height: 10px;">
    		<a href="/admin/"><img width="112" alt="logo" src="/media/cupcake/img/logo.png"></a>
    	</div>
        
        <!-- notifications -->
        <div id="notifications">
        
			<h3 style="font-size: 40px; font-family: monospace; font-weight: lighter; color: rgb(119, 119, 119);">
			<?
				switch(Kohana::$environment)
				{
					case Kohana::STAGING: echo 'STAGING'; break;
					case Kohana::PRODUCTION: echo '<!--PRODUCTION-->'; break;
					case Kohana::DEVELOPMENT: echo 'DEVELOPMENT'; break;
				}
			?>
			</h3>
			
        <?
        /**
        	<a class="qbutton-left" href="index.html"><img width="16" height="15" alt="dashboard" src="/media/cupcake/img/icons/header/dashboard.png"></a>
        	<a class="qbutton-normal tips" href="#" original-title=""><img width="19" height="13" alt="message" src="/media/cupcake/img/icons/header/message.png"> <span class="qballon">23</span> </a>
        	<a class="qbutton-right" href="#"><img width="19" height="13" alt="support" src="/media/cupcake/img/icons/header/support.png"> <span class="qballon">8</span> </a>
          <div class="clear" style="z-index: 920;"></div>
         */
        ?>
        </div>
        
        
        <!-- quick menu -->
        <div id="quickmenu">
            <div class="clear"></div>
        </div>
        
        
        <!-- profile box -->
        <div id="profilebox" style="z-index: 890;">
        
        	<? if(isset($user)){ ?>
        	<a class="display" href="#">
            	<img width="33" height="33" alt="profile" src="<?=Arr::get($user->as_array(), 'avatar', '/media/cupcake/img/simple-profile-img.jpg')?>">
            	<b>Logged in as</b>
				<span><?=$user->username?></span>
            </a>
        	<? } ?>
            
            <div class="profilemenu" style="z-index: 880;">
            	<ul>
                	<li><a href="/admin/User/edit/<?=$user->pk()?>"><?=__('Edit profile')?></a></li>
                	<li><a href="/admin/logout"><?=__('Logout')?></a></li>
                </ul>
            </div>
            
        </div>
        
        <div class="clear" style="z-index: 870;"></div>
    </div>
    <!-- END HEADER -->