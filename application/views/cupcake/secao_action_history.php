        <!-- START PAGE -->
        <div id="page" style="z-index: 810;">
	        <?=$page_header?>
        
        	<!-- START CONTENT -->
            <div class="content">
                <?
                if(isset($history))
                {
            	?>
				<div class="simplebox grid740" style="z-index: 460;">
					<?
					if($history->count()>0)
					{
					?>
					<table class="tablesorter" id="myTable" style="border: 1px solid #CBDAE8;"> 
						<thead> 
							<tr> 
								<th class="header"><?=__('Who')?></th> 
								<th class="header"><?=__('Action')?></th>
								<th class="header"><?=__('Moment')?></th> 
							</tr> 
						</thead> 
						<tbody> 
						<?
						foreach($history->as_array() as $item)
						{
							$link = $_SERVER['REQUEST_URI'] . "/{$item['id']}";
							switch($item['action']){
								case 'create': $message = __('User has create an item'); break;
								case 'update':
									if($undt = json_decode($item['change'])){
										$names = array();
							            foreach($undt as $key => $value){
							                array_push($names, $key);
							            } 
										$message = __('User has updated str of an item', array('str'=>implode($names,', '))) ;
									}else{
										$message = __('User has update ralationships of an item'); 
									}
									break;
								case 'delete': $message = __('User has deleted an item');  break;
							}
							?>
							<tr> 
								<td><a href='<?=$link?>'><?=$item['user_name']?></a></td> 
								<td><a href='<?=$link?>'><?=$message?></a></td> 
								<td><a href='<?=$link?>'><?=date('d/m/Y H:i:s',strtotime($item['moment']))?></a></td> 
							</tr> 
							<?
						}
						?>
						</tbody> 
					</table>
					<?
					}
					?>
				</div>
            	<?
                }
                ?>
            
            </div>
            <!-- END CONTENT -->
            
        </div>
        <!-- END PAGE -->