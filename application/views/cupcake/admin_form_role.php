						<div class="simplebox grid740" style="z-index: 460;">
	                    	<div class="titleh" style="z-index: 450;">
	                        <h3><?=_('Rights')?></h3>
	                        <div class="shortcuts-icons" style="z-index: 440;">
	                        	<a href="#" class="shortcut tips" original-title="Info About This Widget"><img width="25" height="25" alt="icon" src="/media/cupcake/img/icons/shortcut/question.png"></a>
	                            <a href="#" class="shortcut tips" original-title="Add New Item"><img width="25" height="25" alt="icon" src="/media/cupcake/img/icons/shortcut/plus.png"></a>
	                            <a href="#" class="shortcut tips" original-title="Search"><img width="25" height="25" alt="icon" src="/media/cupcake/img/icons/shortcut/search.png"></a>
	                            <a href="#" class="shortcut tips" original-title="Refresh Data"><img width="25" height="25" alt="icon" src="/media/cupcake/img/icons/shortcut/refresh.png"></a>
	                        </div>
	                        </div>
	                        
                            <table class="tablesorter" id="myTable"> 
                            	<thead> 
                            		<tr> 
                            			<th class="header">Last Name</th> 
                            			<th class="header">First Name</th> 
                            			<th class="header">Email</th>
                            			<th class="header">Due</th> 
                            			<th class="header">Web Site</th> 
                            		</tr> 
                            	</thead> 
                                
                                <tbody> 
                                	<tr> 
                                		<td>Smith</td> 
                                		<td>John</td> 
                                		<td>jsmith@gmail.com</td> 
                                		<td>$50.00</td> 
                                		<td>http://www.jsmith.com</td> 
                                	</tr> 
                                
                                	<tr> 
                                		<td>Bach</td> 
                                    	<td>Frank</td> 
                                    	<td>fbach@yahoo.com</td> 
                                    	<td>$545.00</td> 
                                    	<td>http://www.frank.com</td> 
                                    </tr> 
                                    
                                    <tr> 
                                    	<td>Doe</td> 
                                    	<td>Jason</td> 
                                    	<td>jdoe@hotmail.com</td> 
                                    	<td>$12.00</td> 
                                    	<td>http://www.jdoe.com</td> 
                                    </tr> 
                                    
                                    <tr> 
                                    	<td>Conway</td> 
                                    	<td>Tim</td> 
                                    	<td>tconway@earthlink.net</td> 
                                    	<td>$150.00</td> 
                                    	<td>http://www.timconway.com</td> 
                                    </tr> 
                                
                                	<tr> 
                                		<td>Burak</td> 
                                    	<td>Vanli</td> 
                                    	<td>bshank@gmail.com</td> 
                                    	<td>$533.00</td> 
                                    	<td>http://www.frank.com</td> 
                                    </tr> 
                                    
                                    <tr> 
                                    	<td>Doe</td> 
                                    	<td>Jason</td> 
                                    	<td>shekc@mail.com</td> 
                                    	<td>$30.00</td> 
                                    	<td>http://www.jdoe.com</td> 
                                    </tr> 
                                    
                                    <tr> 
                                    	<td>Derp</td> 
                                    	<td>Derpie</td> 
                                    	<td>derp@earthlink.net</td> 
                                    	<td>$390.00</td> 
                                    	<td>http://www.timconway.com</td> 
                                    </tr> 
                                    
                                    <tr> 
                                    	<td>Matt</td> 
                                    	<td>Daeson</td> 
                                    	<td>immat@earthlink.net</td> 
                                    	<td>$120.00</td> 
                                    	<td>http://www.timconway.com</td> 
                                    </tr> 
                                    
                                    <tr> 
                                    	<td>Conway</td> 
                                    	<td>Tim</td> 
                                    	<td>tconway@earthlink.net</td> 
                                    	<td>$20.00</td> 
                                    	<td>http://www.timconway.com</td> 
                                    </tr> 
                                    
                                    <tr> 
                                    	<td>Conway</td> 
                                    	<td>Tim</td> 
                                    	<td>tconway@earthlink.net</td> 
                                    	<td>$10.00</td> 
                                    	<td>http://www.timconway.com</td> 
                                    </tr> 
                                    
                                </tbody> 
                            </table>
	                    </div>
