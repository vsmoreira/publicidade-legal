					<!-- start page title -->
					<div class="page-title">
						<div class="in">
							<div class="titlebar">
								<h2><?=strtoupper($page_title)?></h2>
								<p><?=$page_subtitle?></p>			
							</div>
							<div class="shortcuts-icons">
								<a class="icon-button" href="<?=$page_url_list?>" style="margin: -3px;"><img width="18" height="18" alt="icon" src="/media/cupcake/img/icons/button/arrow-left.png"><span><?=(isset($page_back_label))?$page_back_label:_('Back to List')?></span></a>
								<?=isset($page_button_refresh) ? '<a href="#" class="shortcut tips" original-title="Refresh"><img width="25" height="25" alt="icon" src="/media/cupcake/img/icons/shortcut/refresh.png"></a>' : '' ?>
								<?=isset($page_button_dashboard) ? '<a href="#" class="shortcut tips" original-title="Dashboard"><img width="25" height="25" alt="icon" src="/media/cupcake/img/icons/shortcut/dashboard.png"></a>' : '' ?>
								<?=isset($page_button_addnew) ? '<a href="#" class="shortcut tips" original-title="Add New"><img width="25" height="25" alt="icon" src="/media/cupcake/img/icons/shortcut/plus.png"></a>' : '' ?>
								<?=isset($page_button_search) ? '<a href="#" class="shortcut tips" original-title="Search"><img width="25" height="25" alt="icon" src="/media/cupcake/img/icons/shortcut/search.png"></a>' : '' ?>
								<?=isset($page_button_help) ? '<a href="#" class="shortcut tips" original-title="Refresh"><img width="25" height="25" alt="icon" src="/media/cupcake/img/icons/shortcut/question.png"></a>' : '' ?>
							</div>
							<div class="clear"></div>
						</div>
					</div>
					<!-- end page title -->
                
                
                	<!-- START CONTENT -->
                    <div class="content">
                    
                    	<?   
						if($form->message)
	                    {
                    	$message=$form->message;
	                    }
						include "_result_message.php" 
						?>
                    
						<div class="simplebox grid740">
							<div class="titleh">
								<h3><?=$titleh_form?></h3>
								<div class="shortcuts-icons">
									<a original-title="This is Simple Form" class="shortcut tips" href="#"><img src="/media/cupcake/img/icons/shortcut/question.png" alt="icon" height="25" width="25"></a>
								</div>
							</div>
							<div class="body">
								<form method="post" enctype='multipart/form-data'>
								<? 
									$form->render_all();
									$form->render_buttons();
								?>
								</form>
							</div>
						</div>
						
						<?
						if(isset($extra_sections))
						{
							foreach($extra_sections as $section)
							{
								echo $section;
							}
						}
						?>
                    
                    </div>
                    <!-- END CONTENT -->
                
            
            
            
            
            
            
        