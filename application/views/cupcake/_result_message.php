                    <?
                    if(isset($message))
                    {
                    	$_message = $message['message'];
                    	$_class = 'informationbox'; $_title = 'Information';
                    	if($message['success'] === false) {
	                    	$_class = 'errorbox'; $_title = 'Error';
                    	}
                    	if($message['success'] === true) {
	                    	$_class = 'succesbox'; $_title = 'Success';
                    	}
                    	if($message['success'] === null) {
	                    	$_class = 'warningbox'; $_title = 'Warning';
                    	}
                    	?>
						<div>
                            <div class="albox <?=$_class?>"> <b><?=$_title?> :</b> <?=$_message?> <a class="close tips" href="#" original-title="close">close</a> </div>
                         	<script>
						      $(document).ready(function() {
						      	$('*[original-title="close"]').bind('click', function(){
						      		$(this).parents('div.albox').slideUp('slow', function(){
						      			$(this).remove();
						      		})
							      	return false;
						      	})
						      });
                         	</script>
                         	<div class="clear"></div>   
                        </div>
                    	<?
                    }
                    ?>
                    