<!-- start page title -->
<div class="page-title">
	<div class="in">
		<div class="titlebar">
			<h2><?=strtoupper($page_title)?></h2>
			<p><?=$page_subtitle?></p>			
		</div>
		<div class="shortcuts-icons">
			<a class="icon-button" href="<?=$page_url_list?>" style="margin: -3px;"><img width="18" height="18" alt="icon" src="/media/cupcake/img/icons/button/arrow-left.png"><span><?=(isset($page_back_label))?$page_back_label:_('Back to List')?></span></a>
			<?=isset($page_button_refresh) ? '<a href="#" class="shortcut tips" original-title="Refresh"><img width="25" height="25" alt="icon" src="/media/cupcake/img/icons/shortcut/refresh.png"></a>' : '' ?>
			<?=isset($page_button_dashboard) ? '<a href="#" class="shortcut tips" original-title="Dashboard"><img width="25" height="25" alt="icon" src="/media/cupcake/img/icons/shortcut/dashboard.png"></a>' : '' ?>
			<?=isset($page_button_addnew) ? '<a href="#" class="shortcut tips" original-title="Add New"><img width="25" height="25" alt="icon" src="/media/cupcake/img/icons/shortcut/plus.png"></a>' : '' ?>
			<?=isset($page_button_search) ? '<a href="#" class="shortcut tips" original-title="Search"><img width="25" height="25" alt="icon" src="/media/cupcake/img/icons/shortcut/search.png"></a>' : '' ?>
			<?=isset($page_button_help) ? '<a href="#" class="shortcut tips" original-title="Refresh"><img width="25" height="25" alt="icon" src="/media/cupcake/img/icons/shortcut/question.png"></a>' : '' ?>
		</div>
		<div class="clear"></div>
	</div>
</div>
<!-- end page title -->


<!-- START CONTENT -->
<div class="content">

	<?   
	if($form->message)
    {
	$message=$form->message;
    }
	include "_result_message.php" 
	?>
	
	<form method="post" enctype='multipart/form-data'>
		<div class="simplebox grid740">
			<div class="titleh" style="height: 50px">
				<h3><?=_('Dados da ordem')?></h3>
			</div>
			<div class="body">
				<? 
					$form->render('id');
					$form->render('moment_creation');
					$form->render('user_seller_id');
	//				
					if($form->object->loaded())
					{
					?>
					<div class="st-form-line" style="z-index: 190;">
					<table width="100%">
						<tr>
							<td>
						    	<span class="st-labeltext">Status</span>	
		    				    <div style="padding-top:5px;" id="uniform-status">
					    		 <?
					    		 	
									if($form->object->loaded() && $form->object->status != '')
									{
										?>
											<strong><?=$order = Model_Order::$_status[$form->object->status]?></strong>
										<?
										if($form->object->status == 'STUDIO')
										{
										?>
											<br />
											<span>Data de envio</span>
											<span><i><?= date('d/m/Y H:i:s', strtotime($form->object->moment_sent_studio))?></i></span>
										<?
										}
										if($form->object->status == 'DEVELOPING')
										{
										?>
											<br />
											<span>iniciado</span>
											<span><i><?= date('d/m/Y H:i:s', strtotime($form->object->moment_last_development))?></i></span>
										<?	
										}
									}
		    		 			?>
		    				    
					    		</div>
							</td>
							<td>
								<?
								if($form->object->status == 'ELABORATING')
								{
									if(Auth::instance()->get_user()->type == 'SELLER')
									{
										?>
										<input type="submit" name="button-send-studio" id="button" value="Enviar para o estúdio" class="st-button button-blue">
										<?
									}
								}
								elseif($form->object->status == 'STUDIO')
								{
									if(Auth::instance()->get_user()->type == 'DESIGNER')
									{
										?>
										<input type="submit" name="button-own-order" id="button" value="Assumir ordem" class="st-button button-blue">
										<?
									}
								}
								elseif($form->object->status == 'WITH_DESIGNER')
								{
									if(Auth::instance()->get_user()->type == 'DESIGNER')
									{
										?>
										<input type="submit" name="button-start-development" id="button" value="Iniciar desenvolvimento" class="st-button button-blue">
										<?
									}
								}
								elseif($form->object->status == 'COMPLETED')
								{
									if(Auth::instance()->get_user()->type == 'PREPRESS' && empty($form->object->user_prepress_id) && $form->object->user_prepress_id == NULL)
									{
										?>
										<input type="submit" name="button-awn-preprint" id="button" value="Assumir Pré-impressão" class="st-button button-blue">
										<?
									}
								}
								?>
							</td>
						</tr>
					</table>	
					    <div class="clear" style="z-index: 180;"></div>
					</div>
					<?
					}
	//				$form->render('user_prepress_id');
					
	//								$form->render('moment_taken_designer');
	//				$form->render('moment_taken_prepressor');
	
					if($form->object->loaded())
					{
						?>
						<div class="st-form-line" style="z-index: 640;">
						<table width="100%">
							<tr>
								<td>
							    	<span class="st-labeltext">Designer</span>	
				    				    <span style="display: block;padding-top: 6px;"><?=$form->object->designer->username?></span>
								    <div class="clear" style="z-index: 630;"></div>
								</td>
							<td>
							    	<span class="st-labeltext">Data assumida</span>	
				    				    <span style="display: block;padding-top: 6px;"><?=$form->object->moment_taken_designer ? date('d/m/Y H:i:s',strtotime($form->object->moment_taken_designer)): NULL?></span>
								    <div class="clear" style="z-index: 630;"></div>
							</td>
							</tr>
						</table>	
						</div>
						<div class="st-form-line" style="z-index: 640;">
						<table width="100%">
							<tr>
								<td>
							    	<span class="st-labeltext">Pré-impressor</span>	
				    				    <span style="display: block;padding-top: 6px;"><?=$form->object->prepress->username?></span>
								    <div class="clear" style="z-index: 630;"></div>
								</td>
							<td>
							    	<span class="st-labeltext">Data assumida</span>	
				    				    <span style="display: block;padding-top: 6px;"><?=$form->object->moment_taken_prepressor ? date('d/m/Y H:i:s',strtotime($form->object->moment_taken_prepressor)): NULL?></span>
								    <div class="clear" style="z-index: 630;"></div>
							</td>
							</tr>
						</table>	
						</div>
						<?
					}
					else
					{
						$form->render('user_designer_id');
						$form->render('user_prepress_id');
					}
	
					
					
					$form->render('client_id');
					$form->render('destination');
					?>
					<style>
					#td_format {border-left:solid 1px #ddd;}
					#td_format .st-labeltext {width:80px;}
					#td_format #uniform-book_id {width:198px !important;display:block;}
					#td_book .st-form-error {display:block;}
					#td_format .st-form-error {display:block;}
					
					#td_book .st-form-line {border: none !important}
					#td_book {border-bottom: 1px solid #ddd;}
					</style>
					<table>
						<tr>
							<td id="td_book" rowspan="2">
								<? $form->render('book_id'); ?>
								
								<script>
								var original_format_id = <?= is_numeric($form->object->format_id)?$form->object->format_id:0?>;
								var original_height_ad = <?= is_numeric($form->object->height_ad)?$form->object->height_ad:1?>;
								
								var fill_book_format = function(data)
								{
									$('option','select#format_id').remove();
									$('select#format_id').append("<option selected>selecione...</option>");
									
									$.each(data.formats, function(i){
										var selected = (original_format_id==i) ? ' selected ' : '';
										$('select#format_id').append("<option value='"+i+"' "+selected+">"+data.formats[i]+"</option>");
									});

									original_format_id = -1;
									$('span', $('select#format_id').parent()).html($('select#format_id').find(":selected").text());

									var max_height = parseInt(data.book['max_height']);

									for(var i=1 ; i<= max_height ; i++)
									{
										var selected = (original_height_ad==i) ? ' selected ' : '';
										$('select#height_ad').append("<option value='"+i+"'>"+i+" cm</option>");
									}

									$('select#height_ad').val(original_height_ad);
									$('span', $('select#height_ad').parent()).html($('select#height_ad').find(":selected").text());

									//Closing days
									closing_days_draw([data.book.sunday,data.book.monday,data.book.tuesday,data.book.wednesday,data.book.thursday,data.book.friday,data.book.saturday]);

								};

								var on_change_book_id = function()
								{
									var url = '/admin/Order/book_formats/' + $(this).val();
									
									$.ajax({
										dataType: "json",
										url: url,
										data: [],
										success: fill_book_format,
										error: function(data) { alert('Erro ao tentar obter a lista de formatos para este caderno'); }
									});
									
									return true;
								};
								
								$(document).ready(function(){
									$('select#book_id').bind('change', on_change_book_id);
									
									<?
									if($form->object->book_id)
									{
										?>
									$('select#book_id').trigger('change');
										<?
									}
									?>
								});
								</script>
								
								
								
								<div class="st-form-line">
									<span class="st-labeltext" style=" width: 80px;" >Fechamentos</span>
									<style>
									.tynitable {width:265px;}
									.tynitable th , .tynitable td {border:1px solid #ddd;text-align:center;;padding:3px;}
									.tynitable th {background-color:#efefef;}
									.tynitable td {background-color:#fdfdfd;}
									.tynitable th.selected {background-color:#FF9999;}
									.tynitable td.selected {background-color:#FFF5F5;}
									</style>
									<script>
									if(!Date.prototype.adjustDate){
									    Date.prototype.adjustDate = function(days){
									        var date;

									        days = days || 0;

									        if(days === 0){
									            date = new Date( this.getTime() );
									        } else if(days > 0) {
									            date = new Date( this.getTime() );

									            date.setDate(date.getDate() + days);
									        } else {
									            date = new Date(
									                this.getFullYear(),
									                this.getMonth(),
									                this.getDate() - Math.abs(days),
									                this.getHours(),
									                this.getMinutes(),
									                this.getSeconds(),
									                this.getMilliseconds()
									            );
									        }

									        this.setTime(date.getTime());

									        return this;
									    };
									}
									var closing_days_select = function()
									{
// 										var day = $('#sdate_publication').val();
										
// 										var dt = new Date(day.replace(/(\d{2})\/(\d{2})\/(\d{4})/,'$3/$2/$1'));
// 										var nd = dt.getDay();

// 										$tynitable = $('table.tynitable','div#closing_days_container');

// 										$('.selected', $tynitable).removeClass('selected');

// 										$('th[rel="'+nd+'"]', $tynitable).addClass('selected');
// 										$('td[rel="'+nd+'"]', $tynitable).addClass('selected');

										var dy = $('#sdate_publication').val();
										var dt = new Date(dy.replace(/(\d{2})\/(\d{2})\/(\d{4})/,'$3/$2/$1'));
										var $tdtarget = null;
										var $thtarget = null;
										var $tynitable = $('table.tynitable','div#closing_days_container');

										$('.selected', $tynitable).removeClass('selected');

										for(i=1; i<=7; i++)
										{
											var weekday = dt.adjustDate(0-1).getDay();
											if($('td[rel="'+weekday+'"]', $tynitable).html() != '-')
											{
												$tdtarget = $('td[rel="'+weekday+'"]', $tynitable);
												$thtarget = $('th[rel="'+weekday+'"]', $tynitable);
												break;
											}
										}

										if($tdtarget!=null)
										{
	 										$tdtarget.addClass('selected');
	 										$thtarget.addClass('selected');
										}
										else
										{
											alert('Nao ha data de fechamento prevista para este caderno')
										}										
									};
									var closing_days_draw = function(week_times)
									{
										$('div#closing_days_container').html('');

										$tynitable = $('<table class="tynitable"></table>');
										$tynitable.append('<thead><tr><th rel="0">Dom</th><th rel="1">Seg</th><th rel="2">Ter</th><th rel="3">Qua</th><th rel="4">Qui</th><th rel="5">Sex</th><th rel="6">Sab</th></tr></thead>');
										$tynitable.append('<tbody><tr></tr><tbody>');

										$(week_times).each(function(i){
											var label = (week_times[i]!=null) && (week_times[i]!='') ? week_times[i] : '-';
											$('tbody > tr', $tynitable).append('<td rel="'+i+'">'+label+'</td>');
										});
	
										$('div#closing_days_container').append($tynitable);

										closing_days_select();
									};
									$(document).ready(function(){

										$('#sdate_publication').bind('change', function(){
											closing_days_select();
										});

										$('#sdate_publication').trigger('change');

									});
									</script>
									<div id="closing_days_container"></div>
								</div>
								
								
								
							</td>
							<td id="td_format">
								<? $form->render('format_id'); ?>
							</td>
							</tr>
							<tr>
							<td id="td_format">
								<div style=" padding:20px;">
									<span class="st-labeltext">Altura</span>	
									<select class="st-forminput uniform tips-right" id="height_ad" name="height_ad" <?=($form->object->loaded() && $form->object->status != 'ELABORATING')? 'disabled' : '' ?> style="opacity: 0;width:280px;" ></select>
									<div class="clear"></div>
								</div>
							
							</td>
							</tr>
					</table>
					<?
					$form->render('date_publication');
					$form->render('title');
					$form->render('description_ad');
					$form->render('text_ad');
					$form->render('color');
					$form->render('type_ad');
					?>
					<div class="st-form-line" style="z-index: 200;">	
						<div class="titleh" style="z-index: 420;height:45px !important;">
							<h3 style="font-weight:normal;">Imagens-fonte</h3>
	                        <div class="shortcuts-icons" style="z-index: 410;">
								<span class="st-labeltext">Novas imagens</span>
								<div id="uniform-undefined" class="uploader" style="z-index: 690;">
									<div class="uploader" id="uniform-undefined">
										<input type="file" style="opacity: 0;" size="19" name="references[]" class="uniform " multiple>
									</div>
								</div>
	                        </div>
						</div>
						
						<div class="" id="example_wrapper">
							<table cellspacing="0" cellpadding="0" border="0" id="example" class="display data-table">
								<thead>
									<tr>
										<th class="ui-state-default" style="width: 40px;padding:0px;">Thumb</th>
										<th class="ui-state-default" style="width: 180px;padding:0px;">Autor</th>
										<th class="ui-state-default" style="width: 140px;padding:0px;">Momento de inclusão</th>
										<th class="ui-state-default" style="width: 40px;padding:0px;">Excluir</th>
									</tr>
								</thead>
								<tbody>
								<?
								$attachemnts = $form->object->attachments->order_by('id','desc')->find_all();
								if(count($attachemnts))
								{
									foreach($attachemnts as $attachemnt)
									{
										?>
										<tr class="gradeA odd" id="attachment" rel="<?=$attachemnt->pk()?>">
											<input type="hidden" name="attachments[]" value="<?=$attachemnt->pk()?>">
											<td align="center">
											<a href="<?=$attachemnt->path?>" target="_blank" border="0">
												<img src="<?=$attachemnt->path?>" width="40" height="40"/>
												</a>
											</td>
											<td><?=$attachemnt->author->username?></td>
											<td><?=date('d/m/Y H:i:s',strtotime($attachemnt->created))?></td>
											<td class="center"><a href="#" class="button-red remove_attachemnt" rel="<?=$attachemnt->pk()?>">excluir</a></td>
										</tr>
										<?
									}
								}
								else
								{
									?>
									<tr class="gradeA odd" id="attachment">
										<td colspan="4" align="center">Nenhuma imagem adicionada</td>
									</tr>
									<?
								}
								?>
								</tbody>
								<script>
								$(document).ready(function(){
									$('a.remove_attachemnt').bind('click', function(){
										$('tr#attachment[rel="'+$(this).attr('rel')+'"]').hide('slow', function(){
											$(this).remove();
										});
										return false;
									});
								})
								</script>
							</table>
						</div>
						<div class="clear" style="z-index: 190;"></div>
					</div>
				<?
				
				if($form->object->status == 'COMPLETED')
				{
					$form->render('moment_closed');
				}
				else if($form->object->status == 'CANCELLED')
				{
					?>
					<div class="st-form-line">	
				    	<span class="st-labeltext">Cancelamento</span>
				    	<table>
				    		<tr>
				    			<td>
							        <div>Usuário: <i><?=$form->object->canceller->username?></i></div>
							        <div>Momento: <i><?=date('d/m/Y H:i:s', strtotime($form->object->moment_cancelled))?></i></div>
							        <div>Motivo: <i><?=$form->object->reason_cancel ? $form->object->reason_cancel : '[não informado]'?></i></div>
				    			</td>
				    		</tr>
				    	</table>	
					</div>
					<?
				}

				if(in_array($form->object->status, array('ELABORATING','STUDIO','VALIDATING','WITH_DESIGNER','DEVELOPING','')))
				{
					?>
					<div style="padding:20px;">
						<input type="submit" name="button-submit" id="button" value="Salvar" class="st-button">
						<?
						if(Auth::instance()->get_user()->type == 'SELLER' && $form->object->status != '')
						{
							?>
							<input type="button" name="ask-for-cancel-order" id="button" value="Cancelar Ordem" class="button-red">
							
							<div class="st-form-line" id="ask-for-cancel-order"  style="display:none;background-color:#f1f1f1 !important;margin-top:10px;">	
						    	<span>Informe o motivo do cancelamento</span>	
						        <textarea name="reason-cancel-order" class="st-forminput tips-right disabled" id="text_ad" style="width:510px;" rows="3" cols="47"></textarea> 
			        		    <div class="clear" style="z-index: 520;"></div>
			        		    <br>
			        		    <input type="submit" name="button-cancel-order" id="button" value="Confirmar cancelamento da ordem" class="button-red">
			        		    * Esta ação não poderá ser desfeita
							</div>
							<script>
							$(function(){
								$('input[name="ask-for-cancel-order"]').bind('click', function(){
									$('div#ask-for-cancel-order').toggle('slidup');
								})
							})
							</script>
							<?
						}
						?>
					</div>
					<?
				}
				?>
				
				
			</div>

		</div>
		
		
		<?
		include 'admin_order_form_amendments.php';
		?>

	</form>
	
	<?
	if(isset($extra_sections))
	{
		foreach($extra_sections as $section)
		{
			echo $section;
		}
	}
	?>

</div>
<!-- END CONTENT -->