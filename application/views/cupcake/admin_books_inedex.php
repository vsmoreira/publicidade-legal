<!-- start page title -->
<div class="page-title">
	<div class="in">
		<div class="titlebar">
			<h2><?=strtoupper($page_title)?></h2>
			<p><?=$page_subtitle?></p>	
		</div>
		<div class="shortcuts-icons">
		
			<a class="icon-button" href="<?=$url?>/" style="margin: -3px;"><img width="18" height="18" alt="icon" src="/media/cupcake/img/icons/button/create.png"><span>Adicionar novo caderno</span></a>
		
			<?=isset($page_button_refresh) ? '<a href="#" class="shortcut tips" original-title="Refresh"><img width="25" height="25" alt="icon" src="/media/cupcake/img/icons/shortcut/refresh.png"></a>' : '' ?>
			<?=isset($page_button_dashboard) ? '<a href="#" class="shortcut tips" original-title="Dashboard"><img width="25" height="25" alt="icon" src="/media/cupcake/img/icons/shortcut/dashboard.png"></a>' : '' ?>
			<?=isset($page_button_addnew) ? '<a href="#" class="shortcut tips" original-title="Add New"><img width="25" height="25" alt="icon" src="/media/cupcake/img/icons/shortcut/plus.png"></a>' : '' ?>
			<?=isset($page_button_search) ? '<a href="#" class="shortcut tips" original-title="Search"><img width="25" height="25" alt="icon" src="/media/cupcake/img/icons/shortcut/search.png"></a>' : '' ?>
			<?=isset($page_button_help) ? '<a href="#" class="shortcut tips" original-title="Refresh"><img width="25" height="25" alt="icon" src="/media/cupcake/img/icons/shortcut/question.png"></a>' : '' ?>
		</div>
		<div class="clear"></div>
	</div>
</div>
<!-- end page title -->
	
<!-- START CONTENT -->
<div class="content">

	<? include "_result_message.php" ?>
	
	<!-- START TABLE -->
	<div class="simplebox grid740">
	
	<?=isset($page_list_title) ? "<div class='titleh'><h3>{$page_list_title}</h3></div>" : '' ?>	
	
	<div class="dataTables_wrapper" id="example_wrapper">
	
		<div class="fg-toolbar ui-toolbar ui-widget-header ui-corner-tl ui-corner-tr ui-helper-clearfix">
			<div id="example_length" class="dataTables_length"></div>
			<div class="dataTables_filter" id="example_filter">
				<form method='get'>
					<label>Buscar: <input type="text" name="search" value="<?=isset($_GET['search'])?$_GET['search']:''?>"></label>
				</form>
			</div>
		</div>
		
		<table cellspacing="0" cellpadding="0" border="0" id="example" class="display data-table">
			<?
			if($list->count() > 0)
			{
				$list_order_field = isset($list_order_field) ? $list_order_field : null;
				$list_order_drctn = isset($list_order_drctn) ? $list_order_drctn : null;
				
				if(isset($labels))
				{
					?>
					<thead>
						<tr>
							<th class="header ui-state-default" rowspan="1" colspan="1"><div class="DataTables_sort_wrapper"><?=$labels['description']?><span class="DataTables_sort_icon css_right ui-icon <?=$list_order_field=='description' ? 'ui-icon-triangle-1-n' : 'ui-icon ui-icon-carat-2-n-s'?>"></span></div></th>
							<th class="header ui-state-default" rowspan="1" colspan="1"><div class="DataTables_sort_wrapper"><?=$labels['max_height']?><span class="DataTables_sort_icon css_right ui-icon <?=$list_order_field=='max_height' ? 'ui-icon-triangle-1-n' : 'ui-icon ui-icon-carat-2-n-s'?>"></span></div></th>
					</thead>
					<?
				}
				?>
				<tbody>
				<?
				$la = $list->as_array();
				for($i=$pagination->page_from-1; $i<$pagination->page_to; $i++)
				{
					$item = $la[$i];
					$eo = (!isset($eo)||$eo=='even')?'odd':'even';
					?>
					<tr class="gradeA <?=$eo?>">
						<td class="<?=$list_order_field=='description' ? 'sorting_1':''?>"><a href='<?="{$url}/{$item->id}"?>'><?=$item->description?></a></td>
						<td class="<?=$list_order_field=='max_height' ? 'sorting_1':''?>"><a href='<?="{$url}/{$item->id}"?>'><?=$item->max_height?></a></td>
					</tr>
					<?
				}
				?>
				</tbody>
				<?
			}
			else
			{
				?>
				<tr class="gradeA"><td colspan="6" align="center"><?=_('No record found')?></td></tr>
				<?
			}
			?>
		</table>
		
		<?=$pagination->render_html()?>
		
	</div>
		
	<br clear="all"/>
	
</div>
<!-- END TABLE -->

</div>
<!-- END CONTENT -->

