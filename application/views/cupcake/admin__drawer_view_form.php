	<?=$page_title?>
	
	<!-- START CONTENT -->
    <div class="content">
    <?
    	if(isset($params['form']) && isset($params['form']['formsets']))
    	{
    		
    		if(isset($params['form']['message']))
    		{
    			$boxclass = '';
    			
    			switch($params['form']['message']['type']){
    				case 'info': $boxclass = 'informationbox'; break;
    				case 'error': $boxclass = 'errorbox'; break;
    				case 'success': $boxclass = 'succesbox'; break;
    				case 'warning': $boxclass = 'warningbox'; break;
    			}
    			?>
                <div id="frminfobox" class="albox <?=$boxclass?>">
                	<?=isset($params['form']['message']['title']) ? "<b>{$params['form']['message']['title']}: </b>":''?>
                	<?=$params['form']['message']['message']?>
                	<a original-title="close" href="#" class="close tips">close</a>
                </div>
				<script type="text/javascript" charset="utf-8">
					$(document).ready(function(){
						$("#frminfobox .close").click(function(){
							$("#frminfobox").slideToggle()
						}); 
					});
				</script>
	    		<?
    		}
    		
    		if(isset($params['form']['formsets']))
    		{
			?>
			<form action="" method="post" name="form2" id="form2" enctype="multipart/form-data">
			<?
	    		foreach($params['form']['formsets'] as $formset)
	    		{
		    		$obj = $formset['object'];
		    		$lbs = $obj->labels();
    				?>
	                <!-- START SIMPLE FORM -->
	            	<div class="simplebox grid740">
	                	<div class="titleh">
	                    	<h3><?=$formset['title']?></h3>
	                        <div class="shortcuts-icons">
	                        	<?=isset($formset['help']) ? "<a href='#' rel='{$formset['help']}' class='shortcut tips' original-title='".__('Help')."'><img width='25' height='25' alt='icon' src='/media/cupcake/img/icons/shortcut/question.png'></a>" : '' ?>
	                        </div>
	                    </div>
	                    <div class="body">
			    			<?
				    			$render_html_form = new Render_html_form();
	
					    		$render_html_form->render_all($obj, $formset['fields']);
			    			?>
						</div>
					</div>
					<!-- END SIMPLE FORM -->
    				<?
	    		}
    			?>
                <div class="button-box">
               	  <input type="submit" class="st-button" value="Submit" id="button" name="button">
               	  <?
               	  if(isset($params['form']['delete_button']) && $params['form']['delete_button']===true){
               	  	?>
               	  	<input type="submit" class="dl-button" value="<?=__('Delete')?>" id="delete-button" name="delete-button">
               	  	<script>
               	  	$(document).ready(function(){
	               	  	$("#delete-button").bind("click", function(){
	               	  		if(!confirm('<?=__('Are you sure you want to delete this record? \n(this action can not be undone and will be reported)')?>')){
		               	  		return false;
	               	  		}
	               	  	});
               	  	});
               	  	</script>
               	  	<?
               	  }
               	  ?>
                </div>
			</form>
			<?
			if(isset($params['page_footer']))
			{
				echo $params['page_footer'];
			}
    		}
    		
    	}
    ?>
    </div>
    <!-- END CONTENT -->

                    
                    	
                    
                    	
                    
	
	            
	            
	<?
	class Render_html_form {
		
		public function render_all($obj, $fields)
		{
			$lbs = $obj->labels();
			foreach ($fields as $field=>$fieldps)
			{
				if($fieldps)
				{
	    			$render_html_form = new Render_html_form();
	    			if($fieldps['type'] != 'tabset' && $fieldps['type'] != 'comboswap' && $fieldps['type'] != 'externalfield')
	    			{
		    			$disabled = isset($fieldps['disabled']) && $fieldps['disabled']===true ?'disabled':'';
		    			$disablec = isset($fieldps['disabled']) && $fieldps['disabled']===true ?'st-disable':'';
		    			$xtrclass = isset($fieldps['extraclass']) ? $fieldps['extraclass'] : '';
		    			$xtrclass = $xtrclass.' '.$disablec;
		    			
		    			$fieldlbl = isset($fieldps['label']) ? $fieldps['label'] : (isset($lbs[$field]) ? $lbs[$field] : $field);
		    			
		    			$fieldtip = isset($fieldps['tip']) ? $fieldps['tip'] : null;
		    			
		    			$fieldnme = isset($fieldps['namespace']) ? "{$fieldps['namespace']}[{$field}]" : $field;
		    			$field_id = $field;
						$fieldval = isset($fieldps['value']) ? $fieldps['value'] : (in_array($field, array_keys($obj->as_array())) ? $obj->$field : '') ;
						$field_er = isset($fieldps['error']) && !empty($fieldps['error']) ? "<span class='st-form-error'>* {$fieldps['error']}</span>" : null;
						$fieldvls = isset($fieldps['values']) ? $fieldps['values'] : array() ;
						
		    			echo $render_html_form->render_field($fieldps['type'], $fieldlbl, $fieldtip, $field_id, $fieldnme, $fieldval, $fieldvls, $field_er, $disabled, $xtrclass);
	    			}
	    			else if($fieldps['type'] == 'tabset')
	    			{
	    				echo $this->render_tabset($fieldps['tabset']);
	    			}
	    			else if($fieldps['type'] == 'comboswap')
	    			{
	    				echo $this->render_comboswap($field,$field,$fieldps['label'],$fieldps['selected_data'],$fieldps['available_data']);
	    			}
	    			else if($fieldps['type'] == 'externalfield')
	    			{
	    				echo $this->render_externalfield($obj, $fieldps['template'], $fieldps['parameters']);
	    			}
				}
			}
		}
		
		public function render_field($type, $label, $tip, $id, $name, $value, $values, $error, $disabled, $extraclass)
		{
			switch($type)
			{
				case 'datetime': return $this->render_datetime($name, $id, $label, $tip, $value, $error, $disabled, $extraclass);
				case 'textarea': return $this->render_textarea($name, $id, $label, $tip, $value, $error, $disabled, $extraclass);
				case 'checkbox': return $this->render_checkbox($name, $id, $label, $tip, $values, $error, $disabled, $extraclass);
				case 'radio': return $this->render_radio($name, $id, $label, $tip, $values, $error, $disabled, $extraclass);
				case 'select': return $this->render_select($name, $id, $label, $tip, $values, $error, $disabled, $extraclass);
				case 'hidden': break;
				case 'password': return $this->render_password($name, $id, $label, $tip, $value, $error, $disabled, $extraclass);
				case 'text': return $this->render_text($name, $id, $label, $tip, $value, $error, $disabled, $extraclass);
				case 'file': return $this->render_file($name, $id, $label, $tip, $value, $values, $error, $disabled, $extraclass);
				default: break;
			}
		}
		
		private function render_file($name, $id, $label, $tip, $value, $values, $error, $disabled, $extraclass)
		{
			$extraclass .= $error ? 'st-error-input' : '';
			?>
			<style>
			.list_file {margin: 0px 0 0 160px;padding: 0px;}
			.list_file .title {display: inline-block;width: 290px;font-weight:bold;}
			.list_file .size {display: inline-block;width: 70px;text-align:right;}
			.list_file .albox {margin: 5px 0;padding:16px 48px 12px;}
			.list_file .albox .icon {top: 12px;}
			.list_file .albox .close {background: url("/media/cupcake/img/icons/16x16/bin_closed.png") no-repeat scroll 0 14px transparent;}
			</style>
			<div class="st-form-line">	
				<span class="st-labeltext"><?=$label?></span>	
			    <div class="uploader" id="uniform-undefined">
			    	<input type="file" <?=$disabled?> class="uniform <?=$extraclass?>" size="19" style="opacity: 0;" id="<?=$id?>" name="<?=$name?>" ><span class="filename" style="-moz-user-select: none;"><?=__('No file selected')?></span><span class="action" style="-moz-user-select: none;"><?=__('Choose File')?></span>
			    </div>
			    <?=$error ? "<span class='st-form-error'>{$error}</span>" : ''?>
			    <?
			    if($values->count())
			    {
			    	echo '<ul class="list_file">';
			    	foreach($values as $contentmedia)
			    	{
						$rel = $contentmedia->type=='flash' ? 'prettyPhoto[flash]' : ($contentmedia->type=='video' ? 'prettyPhoto[movies]' : 'prettyPhoto');
						$href = $contentmedia->type=='flash' || $contentmedia->type=='video' ? '/'.$contentmedia->url.'?width=400&height=360' : '/'.$contentmedia->url;
			    		?>
						<li id="li-media-<?=$contentmedia->id?>">
	                        <div class="albox dialogbox">
	                        	<div class="icon"><img src="/media/cupcake/img/icons/22x22/<?=$contentmedia->type?>.png" alt="icon" height="22" width="22"></div>
	                        	<span class='title'><a title="" rel="<?=$rel?>" href="<?=$href?>"><?=$contentmedia->name?></a></span>
	                        	<span class='size'><?=$contentmedia->width?>x<?=$contentmedia->height?></span>
	                        	<span class='size'><? if($contentmedia->size < 100000) echo number_format($contentmedia->size / 1000,2,'.','').'kb'; else echo number_format($contentmedia->size / 1000000,2,'.','').'MB'?></span>
	                        	<a href="/admin/contents/deletemedia/<?=$contentmedia->id?>" class="close tips deleteContentMedia" rel="media-<?=$contentmedia->id?>"><img src="/media/cupcake/img/icons/16x16/bin_closed.png" alt="icon" height="16" width="16"></a>
	                        </div>
						</li>
			    		<?
			    	}
			    	echo '</ul>';
			    }
			    ?>
				<div class="clear"></div> 
				<script type="text/javascript" charset="utf-8">
					$(document).ready(function(){
						$(".uniform").uniform();
					    $("a[rel^='prettyPhoto']").prettyPhoto();
					    $("a.deleteContentMedia").bind('click',function(){
					    	var related = $(this).attr('rel');
					    	if(confirm("<?=__('Are you sure you want to delete this item? (this action can not be undone)')?>")){
								$.getJSON($(this).attr('href'), function(data) {
									if(data.success){
										$("#li-"+related).hide('slow');
									}else{
										alert(data.message)
									}
								});
					    	}
					    	return false;
					    });
					});
				</script>
			</div>
			<?
		}
		
		private function render_text($name, $id, $label, $tip, $value, $error, $disabled, $extraclass)
		{
			?>
	      	<div class="st-form-line">	
	        	<span class="st-labeltext"><?=$label?></span>	
	            <input type="text" <?=$disabled?> class="st-forminput <?=$extraclass?> <?=isset($error) && !empty($error) ? 'st-error-input' : ''?>" style="width:300px" id="<?=$id?>" name="<?=$name?>" value="<?=$value?>" >
	            <?=$error ? "<span class='st-form-error'>{$error}</span>" : ''?>
	            <?=($tip) ? "<div style='color: #87929A;font-size: 11px;line-height: 15px;margin-left:160px;'>{$tip}</div>":''?> 
	        	<div class="clear"></div>
	        </div>
			<?
		}
		
		private function render_password($name, $id, $label, $tip, $value, $error, $disabled, $extraclass)
		{
			?>
	      	<div class="st-form-line">	
	        	<span class="st-labeltext"><?=$label?></span>	
	            <input type="password" <?=$disabled?> class="st-forminput <?=$extraclass?> <?=isset($error) && !empty($error) ? 'st-error-input' : ''?>" style="width:300px" id="<?=$id?>" name="<?=$name?>" value="<?=$value?>" >
	            <?=$error ? "<span class='st-form-error'>{$error}</span>" : ''?>
	            <?=($tip) ? "<div style='color: #87929A;font-size: 11px;line-height: 15px;margin-left:160px;'>{$tip}</div>":''?> 
	        	<div class="clear"></div>
	        </div>
			<?
		}
		
		private function render_select($name, $id, $label, $tip, $values, $error, $disabled, $extraclass)
		{
			?>
	      	<div class="st-form-line">	
	        	<span class="st-labeltext"><?=$label?></span>
	        	
	        	<select class='uniform <?=$extraclass?>' name='<?=$name?>' id='<?=$id?>' <?=$disabled?>>
	        	<?
	        	foreach($values as $cb){
	        		$extraclass .= ' '.isset($error) && !empty($error) ? 'st-error-input' : '';
	        		$checked = $cb['checked'] ? ' SELECTED ' : ''; 
					echo "<option {$checked} value='{$cb['value']}'>{$cb['label']}</option>";
	        	}
	        	?>
	        	</select>
	        	
	            <?=$error ? "<span class='st-form-error'>{$error}</span>" : ''?>
	            <?=($tip) ? "<div style='color: #87929A;font-size: 11px;line-height: 15px;margin-left:160px;'>{$tip}</div>":''?> 
	        	<div class="clear"></div>
	        </div>
			<?
		}
		
		private function render_checkbox($name, $id, $label, $tip, $values, $error, $disabled, $extraclass)
		{
			?>
	      	<div class="st-form-line">	
	        	<span class="st-labeltext"><?=$label?></span>
	        	<?
	        	foreach($values as $cb){
	        		$extraclass .= ' '.isset($error) && !empty($error) ? 'st-error-input' : '';
	        		$checked = $cb['checked'] ? 'checked' : ''; 
	        		echo "<label class='margin-right20'><div class='checker' id='{$id}_{$cb['value']}'><span><input type='checkbox' class='uniform {$extraclass}' {$checked} {$disabled} name='{$name}[]' id='{$id}_{$cb['value']}' value='{$cb['value']}' style='opacity: 0;'></span></div>{$cb['label']}</label>";
	        	}
	        	?>
	        	
	            <?=$error ? "<span class='st-form-error'>{$error}</span>" : ''?>
	            <?=($tip) ? "<div style='color: #87929A;font-size: 11px;line-height: 15px;margin-left:160px;'>{$tip}</div>":''?> 
	        	<div class="clear"></div>
	        </div>
			<?
		}
		
		private function render_radio($name, $id, $label, $tip, $values, $error, $disabled, $extraclass)
		{
			?>
	      	<div class="st-form-line">	
	        	<span class="st-labeltext"><?=$label?></span>
	        	<?
	        	foreach($values as $cb){
	        		$extraclass .= ' '.isset($error) && !empty($error) ? 'st-error-input' : '';
	        		$checked = $cb['checked'] ? 'checked' : ''; 
	        		echo "<label class='margin-right20'><div class='radio' id='{$id}_{$cb['value']}'><span><input type='radio' class='uniform {$extraclass}' {$checked} {$disabled} name='{$name}' id='{$id}_{$cb['value']}' value='{$cb['value']}' style='opacity: 0;'></span></div>{$cb['label']}</label>";
	        	}
	        	?>
	        	
	            <?=$error ? "<span class='st-form-error'>{$error}</span>" : ''?>
	            <?=($tip) ? "<div style='color: #87929A;font-size: 11px;line-height: 15px;margin-left:160px;'>{$tip}</div>":''?> 
	        	<div class="clear"></div>
	        </div>
			<?
		}
		
		private function render_textarea($name, $id, $label, $tip, $value, $error, $disabled, $extraclass)
		{
			$extraclass .= isset($error) && !empty($error) ? 'st-error-input' : '';
			?>
			<div class="st-form-line">	
				<span class="st-labeltext"><?=$label?></span>	
				<textarea <?=$disabled?> cols="47" rows="3" style="width:300px" class="st-forminput <?=$extraclass?>" id="<?=$id?>" name="<?=$name?>"><?=$value?></textarea> 
				<?=$error ? "<span class='st-form-error'>{$error}</span>" : ''?>
				<?=($tip) ? "<div style='color: #87929A;font-size: 11px;line-height: 15px;margin-left:160px;'>{$tip}</div>":''?> 
				<div class="clear"></div>
			</div>
			<?
		}
		
		private function render_datetime($name, $id, $label, $tip, $value, $error, $disabled, $extraclass)
		{
			?>
	      	<div class="st-form-line">
	        	<span class="st-labeltext"><?=$label?></span>	
				<script type="text/javascript"> $(function() { $( "#<?=$id?>" ).datepicker(); }); </script>
	            <input type="text" <?=$disabled?> class="datepicker-input  <?=$disabled?> <?=$error ? 'st-error-input' : ''?>" style="width:180px; <?=$disabled?'background-color:#FAFAFA;color:#A5A5A5;':''?>" id="<?=$id?>" name="<?=$name?>" value="<?=$value?>" >
	            <?=$error ? "<span class='st-form-error'>{$error}</span>" : ''?>  
	        	<?=($tip) ? "<div style='color: #87929A;font-size: 11px;line-height: 15px;margin-left:160px;'>{$tip}</div>":''?>
	        	<div class="clear"></div>
	        </div>
			<?
		}
		
		private function render_tabset($tabset)
		{
			$title = $tabset['title'];
			$tabs = $tabset['tabs'];
			?>
	      	<div class="st-form-line">	
	        	<span class="st-labeltext"><?=$title?></span>	

				<div class="grid360 right" style="width: 538px;float:right;">

					<!-- start tabs codes -->
					<script type="text/javascript">$(function() {$( "#tabs" ).tabs();});</script>
					<div id="tabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all">
						<ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
						<? 
						foreach($tabs as $tab)
						{ 
							if($tab!=null){
								echo '<li class="ui-state-default ui-corner-top ' . (!isset($aksuy2) ? $aksuy2='ui-tabs-selected ui-state-active' : '') . '"><a href="#' . $tab['id'] . '">' . $tab['label'] . '</a></li>'; 
							}
						} 
						?>
						</ul>
						<?
						foreach($tabs as $tab)
						{
							if($tab!=null){
							?>
							<div id="<?=$tab['id']?>" class="ui-tabs-panel ui-widget-content ui-corner-bottom <?=!isset($akjsdh) ? $akjsdh='' : 'ui-tabs-hide'?>">
							<?
								if(isset($tab['fields']))
								{
									$this->render_all($tab['object'], $tab['fields']);
									
								}
								else
								{
									echo __('Undefined content');
								}
							?>
							</div>
							<?
							}
						}
						?>
					</div>
					<!-- end tabs codes -->
				</div>
				<br clear="all"/>
				
			</div>
			<?
		}
		
		private function render_comboswap($id, $name, $label, $selected_data, $available_data)
		{
			?>
			<div class="st-form-line">	
	        	<span class="st-labeltext"><?=$label?></span>	
	        	<span>
	        	<table border=1>
	        		<tr>
	        			<td><?=__('Selected')?></td>
	        			<td style="width:80px;text-align:center;"></td>
	        			<td><?=__('Avaliable')?></td>
	        		</tr>
	        		<tr>
	        			<td>
				            <select name="<?=$name?>[]" multiple id="pn_skills_<?=$id?>" style="width:225px; height:200px;" class="st-forminput st-disable" size="10">
				            <?
				            foreach($selected_data as $k=>$v){
				            	echo "<option value='{$k}'>{$v}</option>";
				            }
				            ?>
				            </select>
	        			</td>
	        			<td style="width:80px;text-align:center;">
							<a class="icon-button tips" href="#" original-title="Remove item"><img width="18" height="18" alt="icon" src="/media/cupcake/img/icons/button/arrow-right.png"></a>
							<br><br><br>
							<a class="icon-button tips" href="#" original-title="Add item"><img width="18" height="18" alt="icon" src="/media/cupcake/img/icons/button/arrow-left1.png"></a>
	        			</td>
	        			<td>
				            <select id="pn_skills1_<?=$id?>" style="width:225px; height:200px;" class="st-forminput st-disable" size="10">
				            <?
				            foreach($available_data as $k=>$v){
				            	echo "<option value='{$k}'>{$v}</option>";
				            }
				            ?>
				            </select>
	        			</td>
	        		</tr>
	        	</table>
	            
	            
	            </span>
	             
	        	<div class="clear"></div>
	        </div>
	        <script>
				$().ready(function() {
					$("#pn_skills_<?=$id?>").change(function (e) {
						$("#pn_skills_<?=$id?> option:selected").remove().appendTo("#pn_skills1_<?=$id?>");
					});
					
					$("#pn_skills1_<?=$id?>").change(function (e) {
						$("#pn_skills1_<?=$id?> option:selected").remove().appendTo("#pn_skills_<?=$id?>");
					});
					
					$("#form2").bind('submit',function(){
						$("option", $("select[name='<?=$name?>[]']")).attr('selected',true);
					});
				});
	        </script>
			<?
		}
		
		private function render_externalfield($obj, $template, $parameters)
		{
			$parameters['obj'] = $obj;
			echo View::factory( $template , $parameters );
		}
	
	}
	?>            
	            
	            
	            
