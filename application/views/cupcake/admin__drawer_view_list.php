		<?=$page_title?>

		<!-- START CONTENT -->
		<div class="content" style="z-index: 690;">
		<form id="dflt_table">

			<div class="simplebox grid740" style="z-index: 410;">

				<?
				if($params['show_table_header']){
					?>
					<div class="titleh" style="z-index: 400;">
						<h3><?=$params['table_header_title']?></h3>
						<div class="shortcuts-icons" style="z-index: 390;">
							<?
								if($params['table_header_button_plus_url']){
									echo "<a href='{$params['table_header_button_plus_url']}' class='shortcut tips'><img width='25' height='25' alt='icon' src='/media/cupcake/img/icons/shortcut/plus.png'></a>";
								}
								if($params['table_header_button_question_content']){
									echo "<a href='#' class='shortcut tips' rel='{$params['table_header_button_question_content']}'><img width='25' height='25' alt='icon' src='/media/cupcake/img/icons/shortcut/question.png'></a>";
								}
								if($params['show_table_header_button_refresh']){
									echo "<a href='#' class='shortcut tips' onClick='javascript:location.reload(true);'><img width='25' height='25' alt='icon' src='/media/cupcake/img/icons/shortcut/refresh.png'></a>";
								}
							?>
						</div>
					</div>
					<?
				}
				?>

				<!-- End Data Tables Initialisation code -->
				<div class="dataTables_wrapper" id="example_wrapper">
					<div class="fg-toolbar ui-toolbar ui-widget-header ui-corner-tl ui-corner-tr ui-helper-clearfix">
					<?
					if($params['addbutton'])
					{
						?>
						<div id="example_length" class="dataTables_length">
							<a class="icon-button" href="<?=$params['addbutton']['url']?>" style="display:inline-block;"><img width="18" height="18" alt="icon" src="/media/cupcake/img/icons/button/create.png"><span><?=$params['addbutton']['text']?></span></a>
						</div>
						<?
					}
					?>
					<div class="dataTables_filter" id="example_filter" <?=$params['addbutton']?"style='margin-top: 5px;'":""?>><label><?=__('Search')?>: <input name="q" type="text" <?=isset($_GET['q'])?"value='{$_GET['q']}'":''?>></label></div>
				</div>
				
				<?
				$labels = $list->labels();
				$list = $list->find_all();
				
				if($list->count() > 0){
					?>
					<table cellspacing="0" cellpadding="0" border="0" id="example" class="display data-table">
						<thead>
							<tr>
							<?
							foreach($params['fields'] as $field){
								$label = isset($labels[$field]) ? $labels[$field] : $field;
								?>
								<th class="header ui-state-default" rowspan="1" colspan="1"><div class="DataTables_sort_wrapper"><?=$label?><span class="DataTables_sort_icon css_right ui-icon ui-icon-triangle-1-n"></span></div></th>
								<?
							}
							if(isset($params['controls'])){
								?>
								<th class="header ui-state-default" rowspan="1" colspan="1" style="width: 88px;"><div class="DataTables_sort_wrapper"><?=__('Controls')?></div></th>
								<?
							}
							?>
							</tr>
						</thead>
						<tbody>
						<?
						$eo=1;
						
						$it_p_page = $params['items_per_page'];

						$curr_page = ceil($list->count() / $it_p_page) < $params['pg'] ? ceil($list->count() / $it_p_page) : $params['pg'];
						$list_from = ($curr_page*$it_p_page) - $it_p_page + 1;
						$list_till = ($list_from + $it_p_page) <= $list->count() ? ($list_from + $it_p_page)-1 : $list->count();
						
						$listas_array = $list->as_array();
						for($i=$list_from-1; $i<$list_till; $i++){
							echo "<tr class='gradeA ".($eo++%2==1?'even':'odd')."' >";

							$item = $listas_array[$i];
							$orderby = isset($params['order_by']) ? $params['order_by'] : array();
							$centerf = isset($params['center_fields']) ? $params['center_fields'] : array();
							foreach($params['fields'] as $field){
								$sorting = in_array($field, array_keys($orderby)) ? "sorting_1" : "";
								$xcenter = in_array($field, $centerf) ? "center" : "";
								echo "<td class='{$sorting} {$xcenter}'>{$item->$field}</td>";
							}
							
							$html_controls = array();
							if(isset($params['controls'])){
								foreach($params['controls'] as $ctrlname=>$control){
									array_push($html_controls, sprintf("<a href='{$control['url']}'>{$ctrlname}</a>", $item->$control['key']));
								}
								echo '<td class="center">' . implode(' &dot; ',$html_controls) . '</td>';
							}
							
							echo '</tr>';
						}
						?>
						</tbody>
					</table>
					<?
					
					//-->PAGINATION
					
					if($curr_page > 1){
						$first = '<a class="first fg-button ui-button ui-state-default" rel="1">'. __('First') .'</a>';
						$prev_ = '<a class="previous fg-button ui-button ui-state-default" rel="'.($curr_page-1).'">'. __('Previous') .'</a>';
					}else{
						$first = '<span class="first ui-corner-tl ui-corner-bl fg-button ui-button ui-state-default ui-state-disabled">'. __('First') .'</span>';
						$prev_ = '<span class="previous fg-button ui-button ui-state-default ui-state-disabled">'. __('Previous') .'</span>';
					}
					if($curr_page < ceil($list->count() / $it_p_page)){
						$last_ = '<a class="last fg-button ui-button ui-state-default" rel="'.(ceil($list->count() / $it_p_page)).'">'. __('Last') .'</a>';
						$next_ = '<a class="next fg-button ui-button ui-state-default" rel="'.($curr_page+1).'">'. __('Next') .'</a>';
					}else{
						$last_ = '<span class="last ui-corner-tl ui-corner-bl fg-button ui-button ui-state-default ui-state-disabled">'. __('Last') .'</span>';
						$next_ = '<span class="next fg-button ui-button ui-state-default ui-state-disabled">'. __('Next') .'</span>';
					}
					
					?>
					<div class="fg-toolbar ui-toolbar ui-widget-header ui-corner-bl ui-corner-br ui-helper-clearfix">
					
					
						<div style="width:350px;display:inline-block;">
							<label>
								<?=__('Showing until')?> 
								<select name="pp" size="1" style="padding:3px;">
									<option value="10" <?=$params['items_per_page']==10 ? 'selected="selected"' : ''?>>10</option>
									<option value="25" <?=$params['items_per_page']==25 ? 'selected="selected"' : ''?>>25</option>
									<option value="50" <?=$params['items_per_page']==50 ? 'selected="selected"' : ''?>>50</option>
									<option value="100" <?=$params['items_per_page']==100 ? 'selected="selected"' : ''?>>100</option>
								</select> 
								<?=__('entries')." - ".__('from')." {$list_from} ".__('to')." {$list_till} ".__('of')." {$list->count()} ".__('entries')?>
								<script> $(document).ready(function(){ $('select', $('#dflt_table')).bind('change',function(){ $('#dflt_table').submit(); }); }); </script>
							</label>
						</div>
						
						<?
						if(ceil($list->count() / $it_p_page) > 1){
						?>
						<div class="dataTables_paginate fg-buttonset ui-buttonset fg-buttonset-multi ui-buttonset-multi paging_full_numbers" id="paginate">
							<?=$first?>
							<?=$prev_?>
							<span>
								<?
								$pgntfrom = ($curr_page-2) > 0 ? $curr_page-2 : 1;
								$pgntto   = ($curr_page+2) <= ceil($list->count() / $it_p_page) ? $curr_page+2 : ceil($list->count() / $it_p_page); 
								for($i=$pgntfrom; $i<=$pgntto; $i++){
									if(($i)==$curr_page){
										echo "<span class='fg-button ui-button ui-state-default ui-state-disabled' rel='{$i}'>".($i)."</span> ";
									}else{
										echo "<a class='fg-button ui-button ui-state-default' rel='{$i}'>".($i)."</a> ";
									}
								}
								?>
							</span>
							<?=$next_?>
							<?=$last_?>
						</div>
						<input type='hidden' name='p' id='p' <?=isset($_GET['p'])?"value='{$_GET['p']}'":''?>/>
						<?
						}
						?>
					</div>
					<script>
						$(document).ready(function(){ $('a', $('#paginate')).bind('click',function(){ $('#p','#dflt_table').val($(this).attr('rel')); $('#dflt_table').submit(); }); });
					</script>
					<?
					
					//-->END OF PAGINATION
					
					
					
					
				}else{
					?>
						<div class="albox errorbox" style="z-index: 230;">
							<?=__('<b>Error :</b> no items registered for these searched parameters.')?>
                        	<a class="close tips" href="#" original-title="close"><?=__('close')?></a>
                        </div>
					<?
				}
							
				?>
				</div>
			</div>
			
			
		</form>
		</div>
		<!-- END CONTENT -->
