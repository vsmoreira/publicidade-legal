<?
$page = array('title'=>'vende uai','subtitle'=>'eles vão impor!',
			'button' =>
				array(
					'refresh'=>'#',
					'dashboard'=>'#',
					'add_new'=>'#',
					'search'=>'#',
					'help'=>'#',
					)
				);

if(isset($page))
{
	?>
	<!-- start page title -->
	<div style="z-index: 740;" class="page-title">
		<div style="z-index: 730;" class="in">
			<div style="z-index: 720;" class="titlebar">
				<h2><?=$page['title']?></h2>
				<?=isset($page['subtitle']) ? "<p>{$page['subtitle']}</p>" : ''?>
			</div>
			<div style="z-index: 710;" class="shortcuts-icons">
				<?=isset($page['button']['refresh'])   ? "<a original-title='"._('Refresh')."' class='shortcut tips' href='{$page['button']['refresh']}'><img src='/media/cupcake/img/icons/shortcut/refresh.png' alt='icon' height='25' width='25'></a>" : ''?>
				<?=isset($page['button']['dashboard']) ? "<a original-title='"._('Dashboard')."' class='shortcut tips' href='{$page['button']['dashboard']}'><img src='/media/cupcake/img/icons/shortcut/dashboard.png' alt='icon' height='25' width='25'></a>" : ''?>
				<?=isset($page['button']['add_new'])   ? "<a original-title='"._('Add New')."' class='shortcut tips' href='{$page['button']['add_new']}'><img src='/media/cupcake/img/icons/shortcut/plus.png' alt='icon' height='25' width='25'></a>" : ''?>
				<?=isset($page['button']['search'])    ? "<a original-title='"._('Search')."' class='shortcut tips' href='{$page['button']['search']}'><img src='/media/cupcake/img/icons/shortcut/search.png' alt='icon' height='25' width='25'></a>" : ''?>
				<?=isset($page['button']['help'])      ? "<a original-title='"._('Refresh')."' class='shortcut tips' href='{$page['button']['help']}'><img src='/media/cupcake/img/icons/shortcut/question.png' alt='icon' height='25' width='25'></a>" : ''?>
			</div>
			<div style="z-index: 700;" class="clear"></div>
		</div>
	</div>
	<!-- end page title -->
	<?
}
?>

<!-- START CONTENT -->
<div style="z-index: 690;" class="content">
	<? include "_result_message.php" ?>
	<?
	if(isset($help))
	{
		?>
		<div style="z-index: 680;" class="simple-tips">
			<h2><?=$help['title']?></h2>
			<ul>
				<?=$help['message']?>
			</ul>
			<a original-title="<?=_('close')?>" href="#" class="close tips"><?=_('close')?></a>
		</div>
		<?
	}

	if(isset($result))
	{
		switch($result['type'])
		{
			case 'warn':   $rs_class='warningbox';     $rs_tip=_('Warning'); break;
			case 'succes': $rs_class='succesbox';      $rs_tip=_('Succes'); break;
			case 'info':   $rs_class='informationbox'; $rs_tip=_('Information'); break;
			case 'error':  $rs_class='errorbox';       $rs_tip=_('Error'); break;
		}
		?>
		<div style="z-index: 260;" class="albox <?=$rs_class?>">
			<b><?=$rs_tip?>:</b> <?=$result['message']?>
			<a original-title="<?=_('close')?>" href="#" class="close tips"><?=_('close')?></a>
		</div>
		<?
	}

	if(isset($note))
	{
		?>
		<div style="z-index: 670;" class="albox dialogbox">
			<div style="z-index: 660;" class="icon">
				<img src="/media/cupcake/img/icons/16x16/note.png" alt="icon" height="16" width="16">
			</div>
			<?=__('<b>Hey there</b>, You have <b>:total</b> new notes.  <a href=":url">click here</a>',array(':total'=>0, ':url'=>'#'))?>
			<a original-title="<?=_('close')?>" href="#" class="close tips"><?=_('close')?></a>
		</div>
		<?
	}
	?>
	
	
	<!-- START TABLE -->
	<div style="z-index: 410;" class="simplebox grid740">
	
	<?
	
	$table = array('title'=>'xxx', 'urlnew'=>'', 'search'=>'search', 

				'list'=> ORM::factory('EpaperEdition')->find_all(), 
				'fields'=>array('epaper_id','publish_date','status','hash','name')

//				'fields'=>array('Year','Name','Profile','Message','CSS grade')
//				            'id' => array(array('numeric'),),
//            'epaper_id' => array(array('not_empty'),array('numeric'),),
//            'publish_date' => array(array('not_empty'),array('date'),),
//            'status' => array(array('not_empty'),array('in_array', array(':value', array_keys($this->_status))),),
//            'hash' => array(array('not_empty'),array('min_length', array(':value', 10)),array('max_length', array(':value', 64)),),
//			'name' => array(array('not_empty'),array('min_length', array(':value', 3)),array('max_length', array(':value', 32)),array('regex', array(':value', '/^[-\pL\pN_.]++$/uD')),),
//				
				
			);
	
	if( isset($table['title']) || isset($table['urlnew']) )
	{
		?>
		<div style="z-index: 400;" class="titleh">
			<?
				echo isset($table['title']) ? "<h3>{$table['title']}</h3>" : '';
				echo isset($table['urlnew']) ? "<div style='z-index: 390;' class='shortcuts-icons'><a original-title='"._('Add New Item')."' class='shortcut tips' href='{$table['urlnew']}'><img src='/media/cupcake/img/icons/shortcut/plus.png' alt='icon' height='25' width='25'></a></div>" : '';
			?>
		</div>
		<?
	}
	?>
	
	<!-- Start Data Tables Initialisation code -->
	<script type="text/javascript" charset="utf-8">
		$(document).ready(function() {
			oTable = $('#example').dataTable({
				"bJQueryUI": true,
				"sPaginationType": "full_numbers"
			});
		} );
	</script>
	<!-- End Data Tables Initialisation code -->
	
	
	<div id="example_wrapper" class="dataTables_wrapper">
	
		<div class="fg-toolbar ui-toolbar ui-widget-header ui-corner-tl ui-corner-tr ui-helper-clearfix">
			<div class="dataTables_length" id="example_length">
				<label>
				<?
				$select = '<select size="1" name="example_length"><option selected="selected" value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select>';
				echo __('Show :select entries', array(':select'=>$select))
				?>
				</label>
			</div>
			<div id="example_filter" class="dataTables_filter">
				<label><?=_('Search')?>: <input type="text" value="<?=isset($table['search']) ? $table['search'] : ''?>"></label>
			</div>
		</div>
		
		<table class="display data-table" id="example" border="0" cellpadding="0" cellspacing="0">
			<thead>
				<tr>
				<?
				foreach($table['fields'] as $field)
				{
					echo '<th colspan="1" rowspan="1" class="header ui-state-default">';
					echo '<div class="DataTables_sort_wrapper">'.$field.'<span class="DataTables_sort_icon css_right ui-icon '.($eworu= (!isset($eworu)) ? 'ui-icon-triangle-1-n' : 'ui-icon-carat-2-n-s').'"></span></div>';
					echo '</th>';
				}
				?>
				</tr>
			</thead>
			<tbody>
				<?
				foreach($table['list'] as $item)
				{
					$cloumn = 0;
					echo "<tr class='gradeA ".($eo = (!isset($eo)||$eo!='even'?'even':'odd'))."'>";
					foreach($table['fields'] as $field)
					{
						echo '<td class="'.(($cloumn++ == 0) ? 'sorting_1' : '').'">' . $item->$field . '</td>';
					}
					echo "</tr>";
				}
				?>
			</tbody>
		</table>
		
		<div class="fg-toolbar ui-toolbar ui-widget-header ui-corner-bl ui-corner-br ui-helper-clearfix">
			<div id="example_info" class="dataTables_info"><?=__('Showing :ini to :end of :tot entries', array(':ini'=>1, ':end'=>10, ':tot'=>44))?></div>
				<div id="example_paginate" class="dataTables_paginate fg-buttonset ui-buttonset fg-buttonset-multi ui-buttonset-multi paging_full_numbers">
					<a id="example_first" class="first ui-corner-tl ui-corner-bl fg-button ui-button ui-state-default ui-state-disabled"><?=_('First')?></a>
					<a id="example_previous" class="previous fg-button ui-button ui-state-default ui-state-disabled"><?=_('Previous')?></a>
					<span>
						<a class="fg-button ui-button ui-state-default ui-state-disabled">1</a>
						<a class="fg-button ui-button ui-state-default">2</a>
						<a class="fg-button ui-button ui-state-default">3</a>
						<a class="fg-button ui-button ui-state-default">4</a>
						<a class="fg-button ui-button ui-state-default">5</a>
					</span>
					<a id="example_next" class="next fg-button ui-button ui-state-default"><?=_('Next')?></a>
					<a id="example_last" class="last ui-corner-tr ui-corner-br fg-button ui-button ui-state-default"><?=_('Last')?></a>
				</div>
			</div>
		</div>
		
	</div>
	<!-- END TABLE -->

</div>
<!-- END CONTENT -->

