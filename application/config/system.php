<?php defined('SYSPATH') or die('No direct script access.');

return array
(
	'admin_theme' => 'cupcake',
	'cache_factor'     => (Kohana::$environment!=Kohana::DEVELOPMENT)?1:0,
	'uploads_folder' => DOCROOT . 'uploads',
);