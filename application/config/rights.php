<?php defined('SYSPATH') or die('No direct script access.');

return array
(
	'login'                => array('value'=>'login', 'label'=>'Pode logar no sistema OCA'),

	'RIGHT_USER_READ'   => array('value'=>'RIGHT_USER_READ',   'label'=>'Pode ler usuários'),
	'RIGHT_USER_EDIT'   => array('value'=>'RIGHT_USER_EDIT',   'label'=>'Pode editar usuários'),
	'RIGHT_USER_DELETE' => array('value'=>'RIGHT_USER_DELETE', 'label'=>'Pode deletar usuários'),
		
	'RIGHT_ROLE_READ'   => array('value'=>'RIGHT_ROLE_READ',   'label'=>'Pode ler papéis'),
	'RIGHT_ROLE_EDIT'   => array('value'=>'RIGHT_ROLE_EDIT',   'label'=>'Pode editar papéis'),
	'RIGHT_ROLE_DELETE' => array('value'=>'RIGHT_ROLE_DELETE', 'label'=>'Pode deletar papéis'),

	'RIGHT_CLIENT_READ'   => array('value'=>'RIGHT_CLIENT_READ',   'label'=>'Pode ler clientes'),
	'RIGHT_CLIENT_EDIT'   => array('value'=>'RIGHT_CLIENT_EDIT',   'label'=>'Pode editar clientes'),
	'RIGHT_CLIENT_DELETE' => array('value'=>'RIGHT_CLIENT_DELETE', 'label'=>'Pode deletar clientes'),
	
	'RIGHT_BOOK_READ'   => array('value'=>'RIGHT_BOOK_READ',   'label'=>'Pode ler cadernos'),
	'RIGHT_BOOK_EDIT'   => array('value'=>'RIGHT_BOOK_EDIT',   'label'=>'Pode editar cadernos'),
	'RIGHT_BOOK_DELETE' => array('value'=>'RIGHT_BOOK_DELETE', 'label'=>'Pode deletar cadernos'),
	
	'RIGHT_ORDER_READ'   => array('value'=>'RIGHT_ORDER_READ',   'label'=>'Pode ler OCA'),
	'RIGHT_ORDER_EDIT'   => array('value'=>'RIGHT_ORDER_EDIT',   'label'=>'Pode editar OCA'),
	'RIGHT_ORDER_DELETE' => array('value'=>'RIGHT_ORDER_DELETE', 'label'=>'Pode deletar OCA'),
	
	'RIGHT_DASHBOARD_VIEW' => array('value'=>'RIGHT_DASHBOARD_VIEW', 'label'=>'Pode visualisar o sistema OCA'),
	'RIGHT_DASHBOARD_VIEW' => array('value'=>'RIGHT_DASHBOARD_VIEW', 'label'=>'Pode visualisar o sistema OCA'),
);