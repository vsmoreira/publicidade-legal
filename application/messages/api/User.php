<?php defined('SYSPATH') or die('No direct access allowed.');

return array(
    'id.numeric' => __(':field must be numeric'),
    'username.not_empty' => __(':field must not be empty'),
    'username.min_length' => __(':field must be at least :param2 characters long'),
    'username.max_length' => __(':field must not exceed :param2 characters long'),
    'username.regex' => __(':field does not match the required format'),
    'username.username_unavailable' => __('This username is already taken'),
    'email.not_empty' => __(':field must not be empty'),
    'email.min_length' => __(':field must be at least :param2 characters long'),
    'email.max_length' => __(':field must not exceed :param2 characters long'),
    'email.email' => __(':field must be an email address'),
    'email.email_unavailable' => __('This email is already taken'),
    'password.not_empty' => __(':field must not be empty'),
    'password.min_length' => __(':field must be at least :param2 characters long'),
    'password.max_length' => __(':field must not exceed :param2 characters long'),
    'registration.not_empty' => __(':field must not be empty'), 
    'registration.date' => __(':field must be a date'),
    'last_login.date' => __(':field must be a date'),
    'logins.numeric' => __(':field must be numeric'),
);