<?php defined('SYSPATH') OR die('No direct script access.');

return array(
	'username.username_unavailable' => _('username unavailable'),
);
