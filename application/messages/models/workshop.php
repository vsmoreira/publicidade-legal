<?php defined('SYSPATH') OR die('No direct script access.');

return array(

	'n_teatcher_contract_id'=>array(
		'with_n_teatcher_contract_id_filled_n_class_start_time_is_required' => 'ao preencher o professor, é necessário informar a hora de início',
		'with_n_teatcher_contract_id_filled_n_class_end_time_is_required' => 'ao preencher o professor, é necessário informar a hora de término',
	),
	'n_class_start_time'=>array(
		'with_n_class_start_time_filled_n_teatcher_contract_id_is_required' => 'a hora de início só é aceita se informado o professor deste turno',
		'with_n_class_start_time_filled_n_class_end_time_is_required' => 'a hora de início só é aceita se informada a de término',
	),
	'n_class_end_time'=>array(
		'with_n_class_end_time_filled_n_teatcher_contract_id_is_required' => 'a hora de término só é aceita se informado o professor deste turno',
		'with_n_class_end_time_filled_n_class_start_time_is_required' => 'a hora de término só é aceita se informada a de início',
	),

	
	'm_teatcher_contract_id'=>array(
		'with_m_teatcher_contract_id_filled_m_class_start_time_is_required' => 'ao preencher o professor, é necessário informar a hora de início',
		'with_m_teatcher_contract_id_filled_m_class_end_time_is_required' => 'ao preencher o professor, é necessário informar a hora de término',
	),
	'm_class_start_time'=>array(
		'with_m_class_start_time_filled_m_teatcher_contract_id_is_required' => 'a hora de início só é aceita se informado o professor deste turno',
		'with_m_class_start_time_filled_m_class_end_time_is_required' => 'a hora de início só é aceita se informada a de término',
	),
	'm_class_end_time'=>array(
		'with_m_class_end_time_filled_m_teatcher_contract_id_is_required' => 'a hora de término só é aceita se informado o professor deste turno',
		'with_m_class_end_time_filled_m_class_start_time_is_required' => 'a hora de término só é aceita se informada a de início',
	),

	
	'a_teatcher_contract_id'=>array(
		'with_a_teatcher_contract_id_filled_a_class_start_time_is_required' => 'ao preencher o professor, é necessário informar a hora de início',
		'with_a_teatcher_contract_id_filled_a_class_end_time_is_required' => 'ao preencher o professor, é necessário informar a hora de término',
	),
	'a_class_start_time'=>array(
		'with_a_class_start_time_filled_a_teatcher_contract_id_is_required' => 'a hora de início só é aceita se informado o professor deste turno',
		'with_a_class_start_time_filled_a_class_end_time_is_required' => 'a hora de início só é aceita se informada a de término',
	),
	'a_class_end_time'=>array(
		'with_a_class_end_time_filled_a_teatcher_contract_id_is_required' => 'a hora de término só é aceita se informado o professor deste turno',
		'with_a_class_end_time_filled_a_class_start_time_is_required' => 'a hora de término só é aceita se informada a de início',
	),
);
