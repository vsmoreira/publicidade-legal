<?php defined('SYSPATH') or die('No direct access allowed.');
 
class Model_User extends Model_Base {

    protected $_table_name = 'user';
    
	protected $_has_many = array(
		#'user_tokens' => array('model' => 'User_Token'),
		'roles' => array('model' => 'Role', 'through' => 'user_roles'),
	);
	
	protected $_has_one = array('profile'=> array( 'model' => 'userprofile', 'foreign_key' => 'user_id', ),);
	
    public $_status = array(
    	'ACTIVE' => 'Active',
    	'DELETED' => 'Deleted',
    	);
    
    public $_types = array(
    		'DESIGNER' => 'Designer',
    		'SELLER' => 'Seller',
    		'PREPRESS' => 'Prepress',
    );
    
    public $_areas = array(
    		'EDITORA_ODIA' => 'Editora O dia',
    		'COMERCIAL' => 'Comercial',
    );
    
    public function rules()
    {
        return array(
            'id' => array(array('numeric'),),
            'status' => array(array('in_array', array(':value', array_keys($this->_status))),),
        	'type' => array(array('in_array', array(':value', array_keys($this->_types))),),
        	'area' => array(array('in_array', array(':value', array_keys($this->_areas))),),
           	'username' => array(array('not_empty'),array('min_length', array(':value', 3)),array('max_length', array(':value', 32)),array('regex', array(':value', '/^[-\pL\pN_.]++$/uD')),array(array($this, 'username_unavailable')),),
            'email' => array(array('not_empty'),array('min_length', array(':value', 8)),array('max_length', array(':value', 64)),array('email'),array(array($this, 'email_unavailable')),),
            'avatar' => array(array('max_length', array(':value', 256))),
            'password' => array(array('not_empty'),array('min_length', array(':value', 10)),array('max_length', array(':value', 64)),),
            'registration' => array(array('not_empty'),array('date'),),
            'last_login' => array(array('date'),),
            'logins' => array(array('numeric'),),
            'recover_hash' => array(array('max_length', array(':value', 256))),
            'name' => array(array('not_empty'),array('min_length', array(':value', 3)),array('max_length', array(':value', 64)),),
        );
    }
 
    public function labels()
    {
        return array(
            'id' => __('ID'),
            'username' => __('Username'),
            'email' => __('Email'),
            'avatar' => __('Avatar'),
            'status' => __('Status'),
            'password' => __('Password'),
            'registration' => __('Registration'),
            'last_login' => __('Last Login'),
            'logins' => __('Logins'),
        	'type' => __('Type'),
        	'area' => __('Area'),
            'recover_hash' => __('Código de recuperação'),
            'name' => __('Nome'),
        );
    }
 
    public function tips()
    {
        return array(
            'username' => __('exclusive name in system'),
            'email' => __('email of this user'),
            'status' => __('situation of this user'),
            'password' => __('secret password'),
            'registration' => __('registration moment'),
            'last_login' => __('last Login moment'),
            'logins' => __('Number of logins'),
            'recover_hash' => __('Código de recuperação'),
            'name' => __('Nome'),
        );
    }
    
    public function filters()
    {
        return array(
            'username' => array(array('trim'),),
            'email' => array(array('trim'),),
            'password' => array(array(array($this, 'crypt_pass')),),
        );
    }
    
    public function username_unavailable($username)
    {
    	if($this->loaded())
    	{
    		return ! ORM::factory('User')->where('username','=',$username)->where('id','<>',$this->id)->find()->loaded();
    	}
    	else
    	{
    		return ! ORM::factory('User')->where('username','=',$username)->find()->loaded();
    	}
    }
    
    public function email_unavailable($email)
    {
    	if($this->loaded())
    	{
    		return ! ORM::factory('User')->where('email','=',$email)->where('id','<>',$this->id)->find()->loaded();
    	}
    	else
    	{
	        return ! ORM::factory('User')->where('email','=',$email)->find()->loaded();
    	}
    }
	
	public static function crypt_pass($password)
	{
//		$security_hash = 'Y!Sbj/z9vR#xsXfT$E_-';
//		
//		if (!empty($password))
//			return sha1( $password . $security_hash );
//		else
//			throw new Oca_Exception('Trying to hash an empty password');
			
		if ( ! Kohana::$config->load('auth.hash_key'))
			throw new Kohana_Exception('A valid hash key must be set in your auth config.');

		return hash_hmac(Kohana::$config->load('auth.hash_method'), $password, Kohana::$config->load('auth.hash_key'));
	}  
	  
    public function register($post)
    {
		if($this->loaded())	
		{
			throw new Oca_Exception('Trying to register an already registered user');
		}
		if(empty($post))	
		{
			throw new Oca_Exception('Empty post!');
		}
		
		$extra = Validation::factory($post)
            ->rule('password_confirmation', 'not_empty')
            ->rule('password_confirmation', 'matches', array(':validation', ':field', 'password'));
            
        if($extra->check())
        {
			$post['registration'] = date('Y-m-d H:i:s');
			
			$this->values($post);
        }
		
		return $this->save($extra);
    }
    
    public function edit($post)
    {
		if( ! $this->loaded())	
		{
			throw new Oca_Exception('Trying to load user');
		}
		
		if(empty($post))	
		{
			throw new Oca_Exception('Empty post!');
		}
		
		$this->values($post);
		
		return $this->save();
    }
    
    
    public function change_email($new_email)
    {
 		if( ! $this->loaded())	
		{
			throw new Oca_Exception('Trying to access an unloaded user');
		}
		
		$this->email = $new_email;
		
		return $this->save();
    }
    
    public function change_username($new_username)
    {
 		if( ! $this->loaded())	
		{
			throw new Oca_Exception('Trying to access an unloaded user');
		}
		
		$this->username = $new_username;
		
		return $this->save();
    }
    
    public function change_password($password, $password_confirmation)
    {
 		if( ! $this->loaded())	
		{
			throw new Oca_Exception('Trying to access an unloaded user');
		}
		
		$extra = Validation::factory(array(
				'password'=>$password,
				'password_confirmation'=>$password_confirmation,
			))
            ->rule('password_confirmation', 'not_empty')
            ->rule('password_confirmation', 'matches', array(':validation', ':field', 'password'));
		
        if($extra->check())
        {
			$this->password = $password;
			
			return $this->save($extra);
        }
		
    }
	public function delete()
	{
 		if( ! $this->loaded())	
		{
			throw new Oca_Exception('Trying to access an unloaded user');
		}
		
		// Virtual delete the object
		$user = ORM::factory('User')->where('status','<>','DELETED')
				->where($this->_primary_key, '=', $this->pk())
				->find($this->_db);
				
		if($user->loaded())
		{
			$user->status = 'DELETED';
			return $user->save();
		}
		else
		{
			throw new Oca_Exception('This user does not exists');
		}
	}
	
    public function update_roles_list($new_list)
    {
    	if($this->loaded())
    	{
	    	$insert_values = array();
	    	foreach($new_list as $new_role)
	    	{
				array_push($insert_values,"({$this->pk()},'{$new_role}')");
	    	}
	    	DB::query(Database::DELETE,'DELETE FROM user_roles WHERE `user_id`='.$this->pk())->execute();
	    	if($insert_values)
	    	{
		    	DB::query(Database::INSERT,'INSERT INTO user_roles (`user_id`,`role_id`) VALUES ' . implode(',',$insert_values))->execute();
	    	}
	    	return $new_list;
    	}
    }
	
	
	
	
	
	
	

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
//	
//
//	/**
//	 * Filters to run when data is set in this model. The password filter
//	 * automatically hashes the password when it's set in the model.
//	 *
//	 * @return array Filters
//	 */
//	public function filters()
//	{
//		return array(
//			'password' => array(
//				array(array(Auth::instance(), 'hash'))
//			)
//		);
//	}
//
//	/**
//	 * Labels for fields in this model
//	 *
//	 * @return array Labels
//	 */
//	public function labels()
//	{
//		return array(
//			'username'         => 'username',
//			'email'            => 'email address',
//			'password'         => 'password',
//		);
//	}

	/**
	 * Complete the login for a user by incrementing the logins and saving login timestamp
	 *
	 * @return void
	 */
	public function complete_login()
	{
		if ($this->_loaded)
		{
				// Update the number of logins
				$this->logins = (is_numeric($this->logins) ? $this->logins : 0) + 1; //new Database_Expression('logins + 1');
	
				// Set the last login date
				$this->last_login = date('Y-m-d H:i:s',time());
	
				// Save the user
				$this->update();
		}
	}
	
//	/**
//	 * Tests if a unique key value exists in the database.
//	 *
//	 * @param   mixed    the value to test
//	 * @param   string   field name
//	 * @return  boolean
//	 */
//	public function unique_key_exists($value, $field = NULL)
//	{
//		if ($field === NULL)
//		{
//			// Automatically determine field by looking at the value
//			$field = $this->unique_key($value);
//		}
//
//		return (bool) DB::select(array(DB::expr('COUNT(*)'), 'total_count'))
//			->from($this->_table_name)
//			->where($field, '=', $value)
//			->where($this->_primary_key, '!=', $this->pk())
//			->execute($this->_db)
//			->get('total_count');
//	}

	/**
	 * Allows a model use both email and username as unique identifiers for login
	 *
	 * @param   string  unique value
	 * @return  string  field name
	 */
	public function unique_key($value)
	{
		return Valid::email($value) ? 'email' : 'username';
	}
	
	public function add_token($token)
	{
		$userToken = ORM::factory('UserToken');
		$userToken->user_id = $this->pk();
		$userToken->token = $token;
		$userToken->ip = $_SERVER['REMOTE_ADDR'];
		$userToken->os = '-';
		$userToken->device_type = '-';
		$userToken->device_id = '-';
		$userToken->lang = '-';
		$userToken->lat = NULL;
		$userToken->lng = NULL;
		$userToken->created = date('Y-m-d H:i:s');
		$userToken->expires = date('Y-m-d H:i:s', strtotime(' +1 day'));
		$userToken->save();
		
		return $userToken;
	}
	
	public static function is_registered($email)
	{
		$user = ORM::factory('User')->where('email','=',$email)->or_where('username','=',$email)->find();
		return $user->loaded();
	}
	
	/*
	 * This method can be used for receive any image
	 * $arr_file -> a posted key expected to exist in var $_FILE
	 * $flg_delete -> (1|0) flag used to delete an image already loaded
	 * $just_validate -> if TRUE, his method will return result of validation, if false method will retur result of storage
	 */
	public function receive_avatar($key_file, $key_delete, $just_validate=FALSE, $final_width=200, $final_height=200)
	{
		$_exts = array(
                    'image/png'=>'png', 
				    'image/jpeg'=>'jpg', 
				    'image/jpg'=>'jpg', 
				    'image/gif'=>'gif'
				    );
				    	
		$_errs = array(
    				0 => "There was a problem with your upload.", //no error; possible file attack!
    				1 => "The file you are trying to upload is too big.", //uploaded file exceeds the upload_max_filesize directive in php.ini
    				2 => "The file you are trying to upload is too big.", //uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the html form
    				3 => "The file you are trying upload was only partially uploaded.",  //uploaded file was only partially uploaded
    				4 => "You must select an image for upload.",  //no file was uploaded
    				5 => "There was a problem with your upload.",  //a default error, just in case!  :)
    				);
		
		$_file        = Arr::get($_FILES,$key_file);
		$_delk        = Arr::get($_POST,$key_delete);
		$_uploads_dir = DOCROOT;
		$_todays_dir  = '/media/uploads/' . date('Y/m/d/');
		$_final_name  = $key_file . '_' . $this->pk() . '.' . Arr::get($_exts,$_file['type']);
		
		//Validating...
		if($_file && $_file['error']!=4)
		{
			list($width, $height, $type, $attr) = getimagesize($_file['tmp_name']);
			
			
			if($_file['error'] != 0)
			{
				throw new Oca_Exception($_errs[$_file['error']]);
			}
			if( ! in_array($_file['type'], array_keys($_exts)))
			{
				throw new Oca_Exception(_('Invalid file type'));
			}
			/*
			if($width<$final_width || $height<$final_height)
			{
				throw new Oca_Exception(vsprintf(_('The max %s accepted size is %s'), array($key_file, "{$final_width}x{$final_height}pixels")));
			}
 			*/
			if( ! is_dir($_uploads_dir.$_todays_dir))
			{
				if ( ! mkdir($_uploads_dir.$_todays_dir, 0700, true)) {
				    throw new Oca_Exception(_('Failed to create folders...'));
				}
			}
			if($just_validate)
			{
				return TRUE;
			}
		}
		
		//Starting work on image..
		if($_delk == 1)
		{
			if( ! empty($this->$key_file))
			{
				if(is_file($_uploads_dir . $this->$key_file))
				{
					$rsunlink = unlink($_uploads_dir . $this->$key_file);
				}
		    	$this->$key_file = NULL;
		    	return $this->save();
			}
		}
		
		if($_file && $_file['error']!=4)
		{
			if ( ! move_uploaded_file($_file['tmp_name'], $_uploads_dir.$_todays_dir.$_final_name))
			{
			    throw new Oca_Exception(_('Possible file upload attack!'));
			}
			
			require_once(APPPATH . 'vendor/php-image-magician/php_image_magician.php');
		    $magicianObj = new imageLib($_uploads_dir.$_todays_dir.$_final_name);
		    $magicianObj -> resizeImage($final_width, $final_height, 'crop');
		    $magicianObj -> saveImage($_uploads_dir.$_todays_dir.$_final_name, 100);
		    
		    if( ! $errrs = $magicianObj->getErrors())
		    {
		    	$this->$key_file = $_todays_dir . $_final_name;
		    	return $this->save();
		    }
		    else
		    {
		    	throw new Oca_Exception($errrs[0]);
		    }
		}
		else
		{
			return TRUE;
		}
	}
	

//	/**
//	 * Password validation for plain passwords.
//	 *
//	 * @param array $values
//	 * @return Validation
//	 */
//	public static function get_password_validation($values)
//	{
//		return Validation::factory($values)
//			->rule('password', 'min_length', array(':value', 8))
//			->rule('password_confirm', 'matches', array(':validation', ':field', 'password'));
//	}
//
//	/**
//	 * Create a new user
//	 *
//	 * Example usage:
//	 * ~~~
//	 * $user = ORM::factory('User')->create_user($_POST, array(
//	 *	'username',
//	 *	'password',
//	 *	'email',
//	 * );
//	 * ~~~
//	 *
//	 * @param array $values
//	 * @param array $expected
//	 * @throws ORM_Validation_Exception
//	 */
//	public function create_user($values, $expected)
//	{
//		// Validation for passwords
//		$extra_validation = Model_User::get_password_validation($values)
//			->rule('password', 'not_empty');
//
//		return $this->values($values, $expected)->create($extra_validation);
//	}
//
//	/**
//	 * Update an existing user
//	 *
//	 * [!!] We make the assumption that if a user does not supply a password, that they do not wish to update their password.
//	 *
//	 * Example usage:
//	 * ~~~
//	 * $user = ORM::factory('User')
//	 *	->where('username', '=', 'kiall')
//	 *	->find()
//	 *	->update_user($_POST, array(
//	 *		'username',
//	 *		'password',
//	 *		'email',
//	 *	);
//	 * ~~~
//	 *
//	 * @param array $values
//	 * @param array $expected
//	 * @throws ORM_Validation_Exception
//	 */
//	public function update_user($values, $expected = NULL)
//	{
//		if (empty($values['password']))
//		{
//			unset($values['password'], $values['password_confirm']);
//		}
//
//		// Validation for passwords
//		$extra_validation = Model_User::get_password_validation($values);
//
//		return $this->values($values, $expected)->update($extra_validation);
//	}
//







	public function send_confirmation_mail($newpasswd)
	{
		$email_sent = FALSE;
		
		if($this->loaded())
		{
			$origin_host = 'http://'.$_SERVER['HTTP_HOST'].'/';
			
			$confirm_url = $origin_host . Route::get('default')->uri(array('directory'=>'admin'));
			
			$mail_content =
				'		Voc&ecirc; acaba de ser cadastrado no sistema OCA da EJESA' .
				'		<p>Seu cadastro está pronto para ser utilizado, conforme as informações abaixo:</p>' .
				'		<p>URL: <a href="'.$confirm_url.'" target="_blank">'.$confirm_url.'</a></p>' .
				'		<p>Usuário para login: <b>'.$this->email.'</b></p>' .
				'		<p>Senha: <b>'.$newpasswd.'</b></p>' .
				'		Seja muito bem vindo ao <a style="color:#D6852A" href="'.$origin_host.'">OCA</a>.';
			
			$email = new Util_Email($this->email, $this->email,'[OCA] Cadastro', Util_Email::default_body($mail_content));
			
			$email_sent = $email->send();
		}
		
		return $email_sent;
	}

	private function send_recover_mail()
	{
		$email_sent = FALSE;
		
		if($this->loaded())
		{
			$origin_host = 'http://'.$_SERVER['HTTP_HOST'].'/';
			
			$confirm_url = $origin_host . Route::get('default')->uri(array('directory'=>'admin','controller'=>'login','action'=>'recuperar_senha','id'=>$this->recover_hash));
			
			$mail_content =
				'		Voc&ecirc; recebeu este emial porque solicitou a recuperação da senha do sistema OCA' .
				'		<p>Para recadastrar sua senha você deve acessar a url abaixo em um prazo máximo de 24 horas:</p>' .
				'		<p>URL: <a href="'.$confirm_url.'" target="_blank">'.$confirm_url.'</a></p>' .
				'		Obrigado por utilizar o <a style="color:#D6852A" href="'.$origin_host.'">OCA</a>.';
			
			$email = new Util_Email($this->email, $this->email,'[OCA] Recuperação de senha', Util_Email::default_body($mail_content));
			
			$email_sent = $email->send();
		}
		
		return $email_sent;
	}
	
	/**
	 * 1 - verifica se o usuario tá carregado
	 * 2 - gerar uma chave hasheada desfazível com id e data limite de alteracao
	 * 3 - gravar tal chave no banco
	 * 4 - gerar url de recuperarção que receba tal chave como id
	 * 5 - enviar email para o usuario com tal chave
	 */
	public function start_password_recover_process()
	{
		if( ! $this->loaded())
		{
			throw new Oca_Exception('Erro ao tentar acessar um usuário inexistente');
		}
		
		$plain_txt = $this->pk().'|'.date('c',strtotime('+1 day', strtotime(date('c'))));
		
		#$this->recover_hash = $this->encrypt_decrypt('encrypt', $plain_txt);
		$this->recover_hash = Util_System::encrypt_decrypt('encrypt', $plain_txt);
		
		if($this->save())
		{
			return $this->send_recover_mail();
		}
		
		throw new Oca_Exception('Erro desconhecido ao tentar iniciar o processo de recuperação de senha');
	}


	/**
	 * Receives de recovery hash sent to user email, and if it is okay return matching user
	 * @return Model_User user for change password
	 */
	public static function password_recover_process_continue($hash)
	{
		$recover_arr = explode('|',Util_System::encrypt_decrypt('decrypt', $hash));
		
		if(count($recover_arr) == 2 && is_numeric($recover_arr[0]))
		{
			if(strtotime(date('c')) > strtotime($recover_arr[1]))
			{
				throw new Oca_Exception('Período limite para recuperação de senha expirado. Por favor solicite nova recuperação de senha');
			}

			$user = ORM::factory('User', intval($recover_arr[0]));
			
			if($user->loaded() && $user->recover_hash==$hash)
			{
				return $user;
			}
		}
		
		throw new Oca_Exception('Código de recuperação inválido');
	}

	public function password_recover_process_finish($post)
	{
        $validation = Validation::factory($post)
            ->rule('password', 'not_empty')
            ->rule('password', 'min_length', array(':value', 6))
            ->rule('passworc', 'matches', array(':validation', ':field', 'password'))
            ;
 
        if ($validation->check())
        {
            $this->password = Arr::get($post,'password');
            $this->recover_hash = NULL;
            return $this->save();
        }
		
		throw new Oca_Exception('Erro desconhecido');
	}
	
}