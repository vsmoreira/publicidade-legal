<?php defined('SYSPATH') or die('No direct access allowed.');
 
class Model_Book extends Model_Base {

    protected $_table_name = 'book';
    
    protected $_has_many = array(
    	'formats' => array('model'=>'BookFormat','foreign_key'=>'book_id')
    );
    
    public static $_closing_time = array(
    		'00:00'=>'00:00',
    		'01:00'=>'01:00',
    		'02:00'=>'02:00',
    		'03:00'=>'03:00',
    		'04:00'=>'04:00',
 			'05:00'=>'05:00',
    		'06:00'=>'06:00',
    		'07:00'=>'07:00',
    		'08:00'=>'08:00',
    		'09:00'=>'09:00',
    		'10:00'=>'10:00',
    		'11:00'=>'11:00',
    		'12:00'=>'12:00',
    		'13:00'=>'13:00',
    		'14:00'=>'14:00',
    		'15:00'=>'15:00',
    		'16:00'=>'16:00',
    		'17:00'=>'17:00',
    		'18:00'=>'18:00',
    		'19:00'=>'19:00',
    		'20:00'=>'20:00',
    		'21:00'=>'21:00',
    		'22:00'=>'22:00',
    		'23:00'=>'23:00'
    	);
	
    public function rules()
    {
        return array(
            'id' => array(array('digit')),
            'description' => array(array('not_empty')),
        	'max_height' => array(array('not_empty'),array('numeric')),
        	'sunday' => array(array('in_array', array(':value', array_keys(self::$_closing_time)))),
        	'monday' => array(array('in_array', array(':value', array_keys(self::$_closing_time)))),
        	'tuesday' => array(array('in_array', array(':value', array_keys(self::$_closing_time)))),
        	'wednesday' => array(array('in_array', array(':value', array_keys(self::$_closing_time)))),
        	'thursday' => array(array('in_array', array(':value', array_keys(self::$_closing_time)))),
        	'friday' => array(array('in_array', array(':value', array_keys(self::$_closing_time)))),
        	'saturday' => array(array('in_array', array(':value', array_keys(self::$_closing_time))))
        );
    }

    public function labels()
    {
        return array(
            'id' => _('ID'),
            'description' => _('Descri&ccedil;&atilde;o'),
            'max_height' => _('Altura Máxima'),
        	'sunday' => _('Domingo'),
        	'monday' => _('Segunda'),
        	'tuesday' => _('Terça'),
        	'wednesday' => _('Quarta'),
        	'thursday' => _('Quinta'),
        	'friday' => _('Sexta'),
        	'saturday' => _('Sábado')
        );
    } 
    
   	public static function get_all_as_assoc_array()
    {
    	$out = array();
    	$books = ORM::factory('Book')->order_by('description','asc')->find_all();
    	if(count($books)>0)
    	{
    		foreach($books as $book)
    		{
    			$out[$book->pk()] = $book->description;
    		}
    	}
    	return $out;
    }

    public function batch_delete_formats($ids_to_keep)
    {
    	array_push($ids_to_keep, -1);
    	
    	$qry = "delete from book_format where book_id={$this->id} and id not in (".implode(',',$ids_to_keep).")";

    	$res = DB::query(Database::DELETE, $qry)->execute();
    	
    	return $res;
    }
 
}