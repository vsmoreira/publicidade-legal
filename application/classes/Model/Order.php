<?php defined('SYSPATH') or die('No direct access allowed.');
 
class Model_Order extends Model_Base {

    protected $_table_name = 'order';
	
	protected $_belongs_to = array(
		'book' => array('model' => 'Book', 'foreign_key' => 'book_id'),
		'canceller' => array('model' => 'User', 'foreign_key' => 'cancel_user_id'),
		'seller' => array('model' => 'User', 'foreign_key' => 'user_seller_id'),
		'prepress' => array('model' => 'User', 'foreign_key' => 'user_prepress_id'),
		'designer' => array('model' => 'User', 'foreign_key' => 'user_designer_id'),
		'client' => array('model' => 'Client', 'foreign_key' => 'client_id'),
	);
	
	protected $_has_many = array(
		'attachments' => array('model' => 'Attachment', 'foreign_key' => 'order_id'),
		'treatments' => array('model' => 'Treatment', 'foreign_key' => 'order_id'),
	);
    
    public static $_destination = array(
    		'CLASSIFICADOS' => 'Classificados',
    		'NOTICIARIO' => 'Noticiario',
    );
    
    public static $_colors = array(
    		'COLORFUL' => 'Colorido',
    		'BW' => 'Preto e Branco'
    );
    
    public static $_types_ad = array(
    		'INVOICE' => 'Somente Faturar',
    		'PAGE' => 'Paginar'
    );
    
    public static $_status = array(
    		'ELABORATING' => 'Em Elaboracao',
    		'STUDIO' => 'Em Estudio de Arte',
    		'VALIDATING' => 'Aguardando validação',
    		'WITH_DESIGNER' => 'Com o Designer',
    		'DEVELOPING' => 'Em Desenvolvimento',
    		'COMPLETED' => 'Concluida',
    		'CANCELLED' => 'Cancelada',
    ); 
    
    public function rules()
    {
        return array(
            'id' => array(array('digit'),),
        	'user_seller_id' => array(array('not_empty'), array('digit')),
        	'user_designer_id' => array(),
        	'user_prepress_id' => array(),
        	'client_id' => array(array('not_empty'), array('digit')),
        	'book_id' => array(array('not_empty'), array('digit')),
        	'format_id' => array(array('not_empty'), array('digit')),
        	'destination' => array(array('in_array', array(':value', array_keys(self::$_destination))),),
        	'title' => array(array('not_empty')),
        	'description_ad' => array(array('not_empty')),
        	'text_ad' => array(array('not_empty')),
        	'color' => array(array('not_empty'),array('in_array', array(':value', array_keys(self::$_colors))),),
        	'type_ad' => array(array('not_empty'),array('in_array', array(':value', array_keys(self::$_types_ad))),),
        	'height_ad' => array(array('digit'),array('not_empty')),
        	'date_publication' => array(array('not_empty'),array('date'),),
        	'status' => array(array('in_array', array(':value', array_keys(self::$_status))),),
        	'reason_cancel' => array(array('min_length', array(':value',10))),
        	'cancel_user_id' => array(array('digit'),),
			'moment_creation' => array(array('date')),
        	'moment_taken_designer' => array(array('date')),
        	'moment_taken_prepressor' => array(array('date')),
        	'moment_closed' => array(array('date')),
        	'attachment_id' => array(),
        );
    }

    public function labels()
    {
        return array(
            'id' => _('OCA'),
            'user_seller_id' => _('Emissor'),
            'user_designer_id' => _('Designer'),
            'user_prepress_id' => _('Pr&eacute;-impress&atilde;o'),
        	'client_id' => _('Cliente'),
        	'book_id' => _('Caderno'),
        	'format_id' => _('Formato'),
        	'destination' => _('Destino'),
        	'title' => _('Titulo'),
        	'description_ad' => _('Descri&ccedil;&atilde;o do An&uacute;ncio'),
        	'text_ad' => _('Texto do An&uacute;ncio'),
        	'color' => _('Cor'),
        	'type_ad' => _('Tipo An&uacute;ncio'),
        	'height_ad' => _('Altura do Anuncio'),
        	'date_publication' => _('Publica&ccedil;&atilde;o'),
        	'status' => _('Status'),
        	'moment_creation' => _('Criação da ordem'),
        	'moment_taken_designer' => _('Data recebida pelo designer'),
        	'moment_taken_prepressor' => _('Momento da assunção'),
        	'moment_closed' => _('Conclus&atilde;o'),
        	'attachment_id' => _('Anexo'),
        );
    }
    
   	public static function get_all_as_assoc_array()
    {
    	$out = array();
    	$orders = ORM::factory('Order')->order_by('id','asc')->find_all();
    	if(count($orders)>0)
    	{
    		foreach($orders as $order)
    		{
    			$out[$order->pk()] = $order->name;
    		}
    	}
    	return $out;
    } 

	public function save(Validation $validation = NULL)
	{
		if(Auth::instance()->get_user()->type == 'SELLER')
		{
			$result = parent::save($validation);
		}
		else
		{
			throw new Oca_Exception('Apenas vendedores podem criar e editar ordens');
		}
	}
	
	public function send_studio()
	{
		if( ! $this->loaded())
		{
			throw new Oca_Exception('Trying to access an unloaded object');
		}
		if(Auth::instance()->get_user()->type == 'SELLER')
		{
			$desinger_logged_id = Auth::instance()->get_user()->id;
		 	$sucess = DB::query(Database::UPDATE, "update `order` set `status`='STUDIO', `moment_sent_studio`='" . date('Y-m-d H:i:s') . "' where `id`={$this->pk()};")->execute();
			
			$origin_host = 'http://'.$_SERVER['HTTP_HOST'].'/';
			$oca_url = $origin_host . Route::get('default')->uri(array('directory'=>'admin','controller'=>'Order', 'action'=>'edit', 'id'=>$this->pk()));

			$users = ORM::factory('User')->where('type', '=', 'DESIGNER')->find_all();
			
			foreach ($users as $user)
			{	
				$mail_content =
				'		Sistema OCA EJESA' .
				'		<p>Olá! '. $user->name .', há uma nova OCA a ser assumida:</p>' .
				'		<p>URL: <a href="'.$oca_url.'" target="_blank">'.$oca_url.'</a></p>';
				
				$email = new Util_Email($user->email, $user->email,'[OCA] Nova OCA criada', Util_Email::default_body($mail_content));
				$email->send();
			}
			
			return $sucess;
		}
		else
		{
			throw new Oca_Exception('Apenas vendedores podem enviar ordens ao estúdio');
		}
	}
	
	public function own_order($force=FALSE)
	{
		if( ! $this->loaded())
		{
			throw new Oca_Exception('Tentando acessar um objeto não carregado.');
		}
		if(Auth::instance()->get_user()->type == 'DESIGNER')
		{
			$desinger_logged_id = Auth::instance()->get_user()->id;
			return DB::query(Database::UPDATE, "update `order` set `user_designer_id`={$desinger_logged_id}, `status`='WITH_DESIGNER', `moment_taken_designer`='" . date('Y-m-d H:i:s') . "' where `id`={$this->pk()};")->execute();
		}
		elseif($force==TRUE)
		{
			return DB::query(Database::UPDATE, "update `order` set `status`='WITH_DESIGNER' where `id`={$this->pk()};")->execute();
		}
		else
		{
			throw new Oca_Exception('Apenas designers podem assumir ordens');
		}
	}
	
	public function start_development()
	{
		if( ! $this->loaded())
		{
			throw new Oca_Exception('Tentando acessar um objeto não carregado.');
		}
		if(Auth::instance()->get_user()->type == 'DESIGNER')
		{
			$desinger_logged_id = Auth::instance()->get_user()->id;
			return DB::query(Database::UPDATE, "update `order` set `status`='DEVELOPING', `moment_last_development`='" . date('Y-m-d H:i:s') . "' where `id`={$this->pk()};")->execute();
		}
		else
		{
			throw new Oca_Exception('Apenas designers podem desenvolver ordens');
		}
	}
	
	public function wait_validation()
	{
		if( ! $this->loaded())
		{
			throw new Oca_Exception('Tentando acessar um objeto não carregado.');
		}
		if(Auth::instance()->get_user()->type == 'DESIGNER')
		{
			$desinger_logged_id = Auth::instance()->get_user()->id;
			$sucess = DB::query(Database::UPDATE, "update `order` set `status`='VALIDATING', `moment_last_development`='" . date('Y-m-d H:i:s') . "' where `id`={$this->pk()};")->execute();
			
			$origin_host = 'http://'.$_SERVER['HTTP_HOST'].'/';
			$oca_url = $origin_host . Route::get('default')->uri(array('directory'=>'admin','controller'=>'Order', 'action'=>'edit', 'id'=>$this->pk()));
			
			$user = ORM::factory('User',$this->user_seller_id);

			$mail_content =
			'		Sistema OCA EJESA' .
			'		<p>Olá! '. $user->name .', a OCA '. $this->pk() .' está pronta para verificação e aprovação.</p>' .
			'		<p>URL: <a href="'.$oca_url.'" target="_blank">'.$oca_url.'</a></p>';
		
			$email = new Util_Email($user->email, $user->email,'[OCA] Emenda OCA esperando aprovação', Util_Email::default_body($mail_content));
			$email->send();

			return $sucess;
		}
		else
		{
			throw new Oca_Exception('Apenas designers podem desenvolver ordens');
		}
	}
	
	public function change_status_to_completed()
	{
		if( ! $this->loaded())
		{
			throw new Oca_Exception('Tentando acessar um objeto não carregado.');
		}
		if(Auth::instance()->get_user()->type == 'SELLER')
		{
			$desinger_logged_id = Auth::instance()->get_user()->id;
			$sucess = DB::query(Database::UPDATE, "update `order` set `status`='COMPLETED', `moment_closed`='" . date('Y-m-d H:i:s') . "' where `id`={$this->pk()};")->execute();
			
			$origin_host = 'http://'.$_SERVER['HTTP_HOST'].'/';
			$oca_url = $origin_host . Route::get('default')->uri(array('directory'=>'admin','controller'=>'Order', 'action'=>'edit', 'id'=>$this->pk()));
			
			$users = ORM::factory('User')->where('type', '=', 'PREPRESS')->find_all();
				
			foreach ($users as $user)
			{
				$mail_content =
				'		Sistema OCA EJESA' .
				'		<p>Olá! '. $user->name .', há uma nova OCA a ser impressa:</p>' .
				'		<p>URL: <a href="'.$oca_url.'" target="_blank">'.$oca_url.'</a></p>';
			
				$email = new Util_Email($user->email, $user->email,'[OCA] Nova OCA para pré-impressão', Util_Email::default_body($mail_content));
				$email->send();
			}
				
			return $sucess;
		}
		else
		{
			throw new Oca_Exception('Apenas vendedores podem fechar ordens');
		}
	}
	
	public function change_status_to_cancelled($reason_cancel_order)
	{
		if( ! $this->loaded())
		{
			throw new Oca_Exception('Tentando acessar um objeto não carregado.');
		}
		if(Auth::instance()->get_user()->type == 'SELLER')
		{
			$desinger_logged_id = Auth::instance()->get_user()->id;
			
			foreach($this->treatments->find_all() as $treatment)
			{
				try
				{
					$treatment->reject('Rejeitado automaticamente por cancelamento da ordem');
				}
				catch(Exception $e)
				{
				}
			}
			
			$sucess = DB::query(Database::UPDATE, "update `order` set `cancel_user_id`={$desinger_logged_id}, `status`='CANCELLED', `reason_cancel`='{$reason_cancel_order}', `moment_cancelled`='" . date('Y-m-d H:i:s') . "' where `id`={$this->pk()};")->execute();

			if($this->user_designer_id != NULL && $this->user_designer_id != '' && !empty($this->user_designer_id))
			{
				$origin_host = 'http://'.$_SERVER['HTTP_HOST'].'/';
				$oca_url = $origin_host . Route::get('default')->uri(array('directory'=>'admin','controller'=>'Order', 'action'=>'edit', 'id'=>$this->pk()));
				
				$user = ORM::factory('User',$this->user_designer_id);
	
				$mail_content =
				'		Sistema OCA EJESA' .
				'		<p>Olá! '. $user->name .', a OCA '. $this->pk() .' foi cancelada, veja mais detalhes na URL abaixo:</p>' .
				'		<p>URL: <a href="'.$oca_url.'" target="_blank">'.$oca_url.'</a></p>';
			
				$email = new Util_Email($user->email, $user->email,'[OCA] Cancelamento OCA', Util_Email::default_body($mail_content));
				$email->send();
			}

			return $sucess;
			
		}
		else
		{
			throw new Oca_Exception('Apenas vendedores podem fechar ordens');
		}
	}
	
	public function own_preprint()
	{
		if( ! $this->loaded())
		{
			throw new Oca_Exception('Tentando acessar um objeto não carregado.');
		}
		if(Auth::instance()->get_user()->type == 'PREPRESS')
		{
			if( ! $this->user_prepress_id)
			{
				$prepress_logged_id = Auth::instance()->get_user()->id;
				return DB::query(Database::UPDATE, "update `order` set `user_prepress_id`={$prepress_logged_id}, `status`='COMPLETED', `moment_taken_prepressor`='" . date('Y-m-d H:i:s') . "' where `id`={$this->pk()};")->execute();
			}
			else
			{
				return TRUE;
			}
		}
		else
		{
			throw new Oca_Exception('Apenas pré-impressores podem assumir ordens para pré-impressão');
		}
	}
	
}