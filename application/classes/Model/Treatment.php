<?php defined('SYSPATH') or die('No direct access allowed.');
 
class Model_Treatment extends Model_Base {

    protected $_table_name = 'treatment';
    
    public $_belongs_to = array
    (
    	'order' => array('model'=>'Order', 'foreign_key'=>'order_id'),
    	'designer' => array('model'=>'User', 'foreign_key'=>'user_designer_id'),
    	'seller' => array('model'=>'User', 'foreign_key'=>'user_seller_id'),
    );
    
    public $_status = array(
    		'WAITING' => 'Em Espera',
    		'ACCEPTED' => 'Aceito',
    		'DENIED' => 'Recusado',
    ); 
    
    public function rules()
    {
        return array(
            'id' => array(array('digit'),),
        	'order_id' => array(array('not_empty'), array('digit')),
        	'user_seller_id' => array(array('digit'), array(array($this, 'validate_seller')),),
        	'user_designer_id' => array(array('not_empty'), array('digit'), array(array($this, 'validate_designer')),),
        	'attachment_image_path' => array(array('not_empty'), array('max_length', array(':value',512))),
        	'attachment_raw_path' => array(array('max_length', array(':value',512))),
        	'status' => array(array('in_array', array(':value', array_keys($this->_status))),),
        	'reason_denial' => array(),
        	'date_create' => array(array('not_empty'),array('date')),
        	'date_response' => array(array('date')),
        	'observation' => array(),
        );
    }

    public function labels()
    {
        return array(
            'id' => _('ID'),
        	'status' => _('Status'),
        	'date_create' => _('Data Cria&ccidil;&atilde;o'),
        	'date_response' => _('Data Entrega'),
        	'reason_denial' => _('Motivo Recusa'),
        	'observation' => _('Observa&ccidil;&atilde;o')
        );
    }
    
    public function validate_seller($user_id)
    {	
		return ORM::factory('User')->where('id','=',$user_id)->where('type','=','SELLER')->find()->loaded();
    }
    
    public function validate_designer($user_id)
    {	
		return ORM::factory('User')->where('id','=',$user_id)->where('type','=','DESIGNER')->find()->loaded();
    }

   	public static function get_all_as_assoc_array()
    {
    	$out = array();
    	$treatments = ORM::factory('Order')->order_by('id','asc')->find_all();
    	if(count($treatments)>0)
    	{
    		foreach($treatments as $order)
    		{
    			$out[$treatment->pk()] = $treatment->name;
    		}
    	}
    	return $out;
    }
    
    public static function add_from_post($order)
    {
		if($order->loaded())
		{

//			if(Auth::instance()->get_user()->type == 'DESIGNER')
//			{
				$arr_low_and_hihg_final_paths = self::receive_amendment_files();
				
				$desiger_comments = isset($_POST['amendment']) && isset($_POST['amendment']['designer_comment']) ? $_POST['amendment']['designer_comment'] : '';
				
				$treatment = ORM::factory('Treatment');
				$treatment->order_id = $order->pk();
				$treatment->user_seller_id = NULL;
				$treatment->user_designer_id = Auth::instance()->get_user()->id;
				$treatment->attachment_image_path = $arr_low_and_hihg_final_paths['low'];
				$treatment->attachment_raw_path = $arr_low_and_hihg_final_paths['high'];
				$treatment->status = 'WAITING';
				$treatment->reason_denial = NULL;
				$treatment->date_create = date('Y-m-d H:i:s');
				$treatment->date_response = NULL;
				$treatment->observation = $desiger_comments;
				
				return $treatment->save();
//			}
//			else
//			{
//				throw new Oca_Exception('Apenas designers podem adicionar emendas');
//			}
		}
		return FALSE;
    } 
 
 	private static function receive_amendment_files()
	{
		if($_FILES && isset($_FILES['amendment']))
		{
			$key_file='finalarts';
			$_uploads_dir = DOCROOT;
			$_todays_dir  = 'media/uploads/'.$key_file.'/' . date('Y/m/d/');
			
			if( ! is_dir($_uploads_dir.$_todays_dir))
			{
				if ( ! mkdir($_uploads_dir.$_todays_dir, 0700, true)) {
				    throw new Oca_Exception(_('Failed to create folders...'));
				}
			}

			$_file_low = NULL;
			$_file_low_exts = array('image/png'=>'png','image/jpeg'=>'jpg','image/jpg'=>'jpg','image/gif'=>'gif');
			$_file_high = NULL;
			$_file_high_exts = array('image/tiff'=>'tif');
			
			if(isset($_FILES['amendment']['name']['low']) && $_FILES['amendment']['error']['low']!=4)
			{
				$_file_low = array
					(
						'name'=>$_FILES['amendment']['name']['low'],
						'type'=>$_FILES['amendment']['type']['low'],
						'tmp_name'=>$_FILES['amendment']['tmp_name']['low'],
						'error'=>$_FILES['amendment']['error']['low'],
						'size'=>$_FILES['amendment']['size']['low'],
					);
			}
			else
			{
				throw new Oca_Exception('O envio do arquivo de baixa é obrigatório');
			}
			if(isset($_FILES['amendment']['name']['high']) && $_FILES['amendment']['error']['high']!=4)
			{
				$_file_high = array
					(
						'name'=>$_FILES['amendment']['name']['high'],
						'type'=>$_FILES['amendment']['type']['high'],
						'tmp_name'=>$_FILES['amendment']['tmp_name']['high'],
						'error'=>$_FILES['amendment']['error']['high'],
						'size'=>$_FILES['amendment']['size']['high'],
					);
			}
			else
			{
				throw new Oca_Exception('O envio do arquivo de alta é obrigatório');
			}
			try
			{
				Util_FileUploader::validade_file($_file_low, $_file_low_exts);
			}
			catch(Exception $e)
			{
				throw new Oca_Exception('O arquivo de baixa enviado é inválido: ' . $e->getmessage());
			}
			try
			{
				Util_FileUploader::validade_file($_file_high, $_file_high_exts);
			}
			catch(Exception $e)
			{
				throw new Oca_Exception('O arquivo de alta enviado é inválido: ' . $e->getmessage());
			}
			
			$_final_low_name  = $key_file . '_' . crc32($_file_low['name']) . '.' . Arr::get($_file_low_exts,$_file_low['type']);
			$_final_high_name  = $key_file . '_' . crc32($_file_high['name']) . '.' . Arr::get($_file_high_exts,$_file_high['type']);

			if ( ! move_uploaded_file($_file_low['tmp_name'], $_uploads_dir.$_todays_dir.$_final_low_name))
			{
			    throw new Oca_Exception(_('Possible file upload attack!'));
			}
			if ( ! move_uploaded_file($_file_high['tmp_name'], $_uploads_dir.$_todays_dir.$_final_high_name))
			{
			    throw new Oca_Exception(_('Possible file upload attack!'));
			}
			
			return array
			(
				'low' => $_todays_dir.$_final_low_name,
				'high' => $_todays_dir.$_final_high_name,
			);
		}
		else
		{
			throw new Oca_Exception('O envio dos dois arquivos de emenda é obrigatório');
		}
	}
	
	public function accept()
	{
		$user = Auth::instance()->get_user();
		
		if( ! $this->loaded())
		{
			throw new Oca_Exception ('Erro ao tentar remover uma emenda não carregada');
		}
		if($user->type != 'SELLER')
		{
			throw new Oca_Exception('Apenas vendedores podem aprovar ordens');
		}
		if($this->status != 'WAITING')
		{
			throw new Oca_Exception('Sé é possível aprovar ordens que não foram analisadas');
		}
		
		$this->status = 'ACCEPTED';
		$this->user_seller_id = $user->id;
		$this->date_response = date('Y-m-d H:i:s');
		
		if($this->save())
		{
			$this->order->change_status_to_completed();
			return TRUE;
		}
		
		throw new Oca_Exception('Não foi possível aprovar a ordem');
	}
	
	public function reject($reason_denial)
	{
		$user = Auth::instance()->get_user();
		
		if( ! $this->loaded())
		{
			throw new Oca_Exception ('Erro ao tentar remover uma emenda não carregada');
		}
		if($user->type != 'SELLER')
		{
			throw new Oca_Exception('Apenas vendedores podem aprovar ordens');
		}
		if($this->status != 'WAITING')
		{
			throw new Oca_Exception('Sé é possível aprovar ordens que não foram analisadas');
		}
		
		$this->reason_denial = $reason_denial;
		$this->user_seller_id = $user->id;
		$this->date_response = date('Y-m-d H:i:s');
		$this->attachment_raw_path = '';
		$this->status = 'DENIED';
		
		if($this->save())
		{
			$this->order->own_order(TRUE);
			
			if(is_file(DOCROOT.$this->attachment_raw_path)) unlink(DOCROOT.$this->attachment_raw_path);
			
			$origin_host = 'http://'.$_SERVER['HTTP_HOST'].'/';
			$oca_url = $origin_host . Route::get('default')->uri(array('directory'=>'admin','controller'=>'Order', 'action'=>'edit', 'id'=>$this->order_id));
			
			$user_designer = ORM::factory('User',$this->user_designer_id);
			
			$mail_content =
			'		Sistema OCA EJESA' .
			'		<p>Olá! '. $user_designer->name .', a Emenda da OCA '. $this->order_id .' foi rejeitada, veja mais detalhes na URL abaixo:</p>' .
			'		<p>URL: <a href="'.$oca_url.'" target="_blank">'.$oca_url.'</a></p>';
				
			$email = new Util_Email($user_designer->email, $user_designer ->email,'[OCA] Rejeição de emenda', Util_Email::default_body($mail_content));
			$email->send();
			
			return TRUE;
		}
		
		throw new Oca_Exception('Não foi possível aprovar a ordem');
	}
	
	

	
	public function delete()
	{
		$user = Auth::instance()->get_user();
		
		if( ! $this->loaded())
		{
			throw new Oca_Exception ('Erro ao tentar remover uma emenda não carregada');
		}
		if($user->type != 'DESIGNER' || $user->pk() != $this->user_designer_id)
		{
			throw new Oca_Exception('Apenas o desginer que criou a emenda pode remove-la');
		}
		if($this->status != 'WAITING')
		{
			throw new Oca_Exception('Sé é possível remover emendas que não foram analisadas');
		}
		
		$order = $this->order;
        $attachment_raw_path = $this->attachment_raw_path;
        $attachment_image_path = $this->attachment_image_path;
		
		if($result = parent::delete())
		{
			$order->start_development();
			
			if(is_file(DOCROOT.$attachment_raw_path)) unlink(DOCROOT.$attachment_raw_path);
			if(is_file(DOCROOT.$attachment_image_path)) unlink(DOCROOT.$attachment_image_path);
			
			return TRUE;
		}
		
		throw new Oca_Exception('Não foi possível remover a emenda');
	}
	
}