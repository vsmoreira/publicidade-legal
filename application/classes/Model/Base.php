<?php defined('SYSPATH') or die('No direct access allowed.');
 
class Model_Base extends ORM
{
    public function get_url_edit($controller=null, $action=null)
    {
    	if( $this->loaded() )
    	{
    		$action=$action==null?'edit':$action;
    		$controller=$controller==null?$this->object_name():$controller;
	    	return URL::site(Route::get('default')->uri(array('directory'=>'admin', 'controller'=>$controller, 'action'=>$action, 'id'=>$this->pk())));
    	}
    	throw new HTTP_Exception_500('Trying to get edit url of an unloaded object');
    }


}