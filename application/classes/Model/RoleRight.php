<?php defined('SYSPATH') or die('No direct access allowed.');
 
class Model_RoleRight extends Model_Base {

    protected $_table_name = 'role_rights';
 
    public function rules()
    {
        return array(
            'role_id' => array(array('not_empty'),array('numeric'),),
            'right' => array(array('not_empty'),array('max_length', array(':value', 32)),array('regex', array(':value', '/^[-\pL\pN_.]++$/uD')),),
        );
    }
 
    public function labels()
    {
        return array(
            'role_id' => __('Role'),
            'right' => __('Right'),
        );
    }
}