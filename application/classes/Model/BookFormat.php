<?php defined('SYSPATH') or die('No direct access allowed.');
 
class Model_BookFormat extends Model_Base {

    protected $_table_name = 'book_format';
	
    public function rules()
    {
        return array(
            'id' => array(array('digit'),),
        	'book_id' => array(array('not_empty'), array('digit')),
        	'name' => array(array('not_empty'),array('max_length', array(':value',16))),
            'width' => array(array('not_empty'),array('numeric'))
        );
    }

    public function labels()
    {
        return array(
            'id' => _('ID'),
            'book_id' => _('Book'),
            'name' => _('Name'),
            'width' => _('Width'),
        );
    } 
    
 
}