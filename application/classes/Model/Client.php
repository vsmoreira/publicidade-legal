<?php defined('SYSPATH') or die('No direct access allowed.');
 
class Model_Client extends Model_Base {

    protected $_table_name = 'client';
    	

    public function rules()
    {
        return array(
            'id' => array(array('digit'),),
            'corporate_name' => array(array('not_empty'),array('min_length', array(':value', 7)),array('max_length', array(':value', 128))),
        	'fantasy_name' => array(array('min_length', array(':value', 5)),array('max_length', array(':value', 128))),
        	'cpf_cnpj' => array(array('not_empty'),array('min_length', array(':value', 11)),array('max_length', array(':value', 20))),
        	'address' => array(array('max_length', array(':value',256))),
        	'address_neighborhood' => array(array('max_length', array(':value',64))),
        	'address_city' => array(array('max_length', array(':value',64))),
        	'address_state' => array(array('exact_length', array(':value',2))),
        	'address_zip' => array(array('max_length', array(':value',10))),
        	'phone_primary' => array(array('min_length', array(':value', 8)),array('max_length', array(':value', 16))),
        	'phone_secondary' => array(array('min_length', array(':value', 8)),array('max_length', array(':value', 16))),
        	'email' => array(array('min_length', array(':value', 8)),array('max_length', array(':value', 64)),array('email')),
        );
    }

    public function labels()
    {
        return array(
            'id' => _('ID'),
            'corporate_name' => _('Razao Social'),
        	'fantasy_name' => _('Nome Fantasia'),
        	'cpf_cnpj' => _('CPF/CNPJ'),
        	'address' => _('Endereço'),
        	'address_neighborhood' => _('Bairro'),
        	'address_city' => _('Cidade'),
        	'address_state' => _('Estado'),
        	'address_zip' => _('CEP'),
        	'phone_primary' => _('Telefone 1'),
        	'phone_secondary' => _('Telefone 2'),
        	'email' => _('Email'),
        );
    }
    
   	public static function get_all_as_assoc_array()
    {
    	$out = array();
    	$clients = ORM::factory('Client')->order_by('corporate_name','asc')->find_all();
    	if(count($clients)>0)
    	{
    		foreach($clients as $client)
    		{
    			$out[$client->pk()] = $client->name;
    		}
    	}
    	return $out;
    } 
    
}