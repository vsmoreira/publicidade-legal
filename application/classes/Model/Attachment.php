<?php defined('SYSPATH') or die('No direct access allowed.');

class Model_Attachment extends Model_Base
{
    protected $_table_name = 'attachment';

	protected $_belongs_to = array(
		'author' => array('model' => 'User', 'foreign_key' => 'author_user_id'),
	);
	
    public function rules()
    {
        return array(
            'id' => array(array('digit'),),
        	'path' => array(array('not_empty')),            
        	'name' => array(array('not_empty')),            
            'author_user_id' => array(array('not_empty'),array('digit'),),
            'created' => array(array('date'),),
        );
    }

    public function labels()
    {
        return array(
            'id' => _('ID'),
            'path' => _('Path'),
            'name' => _('name'),
            'author_user_id' => _('author_user_id'),
            'created' => _('created'),
        );
    }
    
    public static function update_attached_files($order, $key_file='references',$key_delete='attachments', $final_width=200, $final_height=200)
    {
		$_uploads_dir = DOCROOT;
		$_todays_dir  = '/media/uploads/refereces/' . date('Y/m/d/');
    	$_exts = array('image/png'=>'png','image/jpeg'=>'jpg','image/jpg'=>'jpg','image/gif'=>'gif');

    	if($order->loaded())
    	{
			if( ! is_dir($_uploads_dir.$_todays_dir))
			{
				if ( ! mkdir($_uploads_dir.$_todays_dir, 0700, true)) {
				    throw new Oca_Exception(_('Failed to create folders...'));
				}
			}
	
			self::delete_removed_images($order->pk(),Arr::get($_POST,$key_delete,array()));
								    				
	    	if(isset($_FILES[$key_file]))
	    	{
	    		foreach($_FILES[$key_file]['name'] as $key=>$fname)
	    		{
	    			$_file = array(
	    				'name'=>$fname,
	    				'type'=>$_FILES[$key_file]['type'][$key],
	    				'tmp_name'=>$_FILES[$key_file]['tmp_name'][$key],
	    				'error'=>$_FILES[$key_file]['error'][$key],
	    				'size'=>$_FILES[$key_file]['size'][$key],
	    			);
	    			
	    			$single_name = explode('.',$fname);
	    			
					if(Util_FileUploader::validade_file($_file, $_exts))
					{
// 		    			$_final_name  = $key_file . '_' . $order->pk(). '_' . crc32($fname) . '.' . Arr::get($_exts,$_file['type']);
						$_final_name  = 'oca' . '_' . $order->pk(). '_' . $single_name[0] . '.' . Arr::get($_exts,$_file['type']);
						
						if ( ! move_uploaded_file($_file['tmp_name'], $_uploads_dir.$_todays_dir.$_final_name))
						{
						    throw new Oca_Exception(_('Possible file upload attack!'));
						}
						
						require_once(APPPATH . 'vendor/php-image-magician/php_image_magician.php');
					    $magicianObj = new imageLib($_uploads_dir.$_todays_dir.$_final_name);
// 					    $magicianObj -> resizeImage($final_width, $final_height, 'crop');
					    $magicianObj -> saveImage($_uploads_dir.$_todays_dir.$_final_name, 100);
					    
					    if( ! $errrs = $magicianObj->getErrors())
					    {
					    	$attachment = ORM::factory('Attachment');
					    	$attachment->order_id = $order->pk();
					    	$attachment->name = $_file['name'];
					    	$attachment->path = $_todays_dir.$_final_name;
					    	$attachment->author_user_id = Auth::instance()->get_user()->pk();
					    	$attachment->save();
					    }
					    else
					    {
					    	throw new Oca_Exception($errrs[0]);
					    }
					}
					
	    		}
	    	}
    	}
    	else
    	{
    		throw new Oca_Exception('Trying to acess an unloaded order');
    	}
    } 
    
    private static function delete_removed_images($order_id, $arr_images_to_keep)
    {
    	array_push($arr_images_to_keep, -1); #Adiciono um id invalido, para casos do array vir vazio, ou seja, quando nenhuma imagem sera mantida
		
    	$query_select = "SELECT id, path FROM attachment WHERE order_id={$order_id} AND id NOT IN (".implode(',',$arr_images_to_keep).")";
	    	
    	foreach(DB::query(Database::SELECT, $query_select)->execute() as $result)
    	{
   			if( ! empty($result['path']))
   			{
   				if(is_file(DOCROOT.$result['path']))
   				{
  					$rsunlink = unlink(DOCROOT.$result['path']);
   				}
   			}
    	}
	    	
    	$qrery = "DELETE FROM attachment WHERE order_id={$order_id} AND id NOT IN (".implode(',',$arr_images_to_keep).")";
    	$result = DB::query(Database::DELETE, $qrery)->execute();
	    	
    	return $result;
    }
    
//    private static function validade_file($_file, $_exts)
//    {
//		$_errs = array(
//    				0 => "There was a problem with your upload.", //no error; possible file attack!
//    				1 => "The file you are trying to upload is too big.", //uploaded file exceeds the upload_max_filesize directive in php.ini
//    				2 => "The file you are trying to upload is too big.", //uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the html form
//    				3 => "The file you are trying upload was only partially uploaded.",  //uploaded file was only partially uploaded
//    				4 => "You must select an image for upload.",  //no file was uploaded
//    				5 => "There was a problem with your upload.",  //a default error, just in case!  :)
//    				);
//    				
//		//Validating...
//		if($_file && $_file['error']!=4)
//		{
//			list($width, $height, $type, $attr) = getimagesize($_file['tmp_name']);
//			
//			if($_file['error'] != 0)
//			{
//				throw new Oca_Exception($_errs[$_file['error']]);
//			}
//			if( ! in_array($_file['type'], array_keys($_exts)))
//			{
//				throw new Oca_Exception(_('Invalid file type'));
//			}
//			/*
//			if($width<$final_width || $height<$final_height)
//			{
//				throw new Oca_Exception(vsprintf(_('The max %s accepted size is %s'), array($key_file, "{$final_width}x{$final_height}pixels")));
//			}
// 			*/
// 			return TRUE;
//		}
//		return FALSE;
//    }
// 
}