<? defined('SYSPATH') OR die('No direct access allowed.');

class Util_FileUploader
{
    public static function validade_file($_file, $_exts)
    {
		$_errs = array(
    				0 => "There was a problem with your upload.", //no error; possible file attack!
    				1 => "The file you are trying to upload is too big.", //uploaded file exceeds the upload_max_filesize directive in php.ini
    				2 => "The file you are trying to upload is too big.", //uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the html form
    				3 => "The file you are trying upload was only partially uploaded.",  //uploaded file was only partially uploaded
    				4 => "You must select an image for upload.",  //no file was uploaded
    				5 => "There was a problem with your upload.",  //a default error, just in case!  :)
    				);
    				
		//Validating...
		if($_file && $_file['error']!=4)
		{
			list($width, $height, $type, $attr) = getimagesize($_file['tmp_name']);
			
			if($_file['error'] != 0)
			{
				throw new Oca_Exception($_errs[$_file['error']]);
			}
			if( ! in_array($_file['type'], array_keys($_exts)))
			{
				throw new Oca_Exception(_('Invalid file type'));
			}
			/*
			if($width<$final_width || $height<$final_height)
			{
				throw new Oca_Exception(vsprintf(_('The max %s accepted size is %s'), array($key_file, "{$final_width}x{$final_height}pixels")));
			}
 			*/
 			return TRUE;
		}
		return FALSE;
    }
}