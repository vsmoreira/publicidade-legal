<?php defined('SYSPATH') or die('No direct script access.');

class Util_Email {
	
	var $from_email;
	var $from_name;
	var $subject;
	var $body;
	
	function __construct($from_email, $from_name, $subject, $body)
	{
	    $this->from_email = $from_email;
	    $this->from_name = $from_name;
	    $this->subject = $subject;
	    $this->body = $body;
	}
	
	public function send()
	{
        require_once(APPPATH.'vendor/PHPMailer_5.2.1/class.phpmailer.php');
       
        $mail = new PHPMailer(true);
        $mail->IsSMTP(); // telling the class to use SMTP
        $mail->SMTPAuth = true; // enable SMTP authentication
        $mail->IsHTML(true); // Define que o e-mail serï¿½ enviado como HTML
        $mail->CharSet = 'UTF-8'; //'iso-8859-1'; // Charset da mensagem (opcional)

        $mail->SetFrom(Kohana::$config->load('email.from_mail'), Kohana::$config->load('email.from_name'));
        $mail->AddAddress($this->from_email);
        $mail->Subject = $this->subject;
        $mail->Body = $this->body;
        
        $mail->SMTPSecure = Kohana::$config->load('email.smtp_type');
        $mail->Host = Kohana::$config->load('email.smtp_host');
        $mail->Port = Kohana::$config->load('email.smtp_port');
        $mail->Username = Kohana::$config->load('email.smtp_user');
        $mail->Password = Kohana::$config->load('email.smtp_pass');
       
        $email_sent = $mail->Send();
        
        $mail->ClearAllRecipients();
        $mail->ClearAttachments();
        
		return $email_sent;
	}
	
	public static function default_body($mail_content, $origin_host=NULL)
	{
		if(is_null($origin_host))
		{
			$origin_host = 'http://'.$_SERVER['HTTP_HOST'].'/';
		}
		
		return 
			'<div style="background-color:#fff;width:630px;">' .
			'	<img src="'.$origin_host.'media/img/mainheader.png">' .
			'	<div style="padding:20px;font-size:16px;">' .
					$mail_content .
			'	</div>' .
			'	<br clear="all"/>' .
			'</div>';
	}

  function attach_file()
  {
    if( file_exists( $this->anexo ) && is_file( $this->anexo ) )
    {
        $fp = fopen( $this->anexo, 'rb' );
        $anexo = fread( $fp, filesize( $this->anexo ) );
        $anexo = base64_encode( $anexo );
        fclose( $fp );
        $anexo = chunk_split( $anexo );

        $pathinfo = pathinfo( $this->anexo );
        $mime = new mimetype;
        $tipo = $mime->getType( $this->anexo );
        $nome_arquivo = $pathinfo['basename'];
        $boundary = "XYZ-" . date("dmYis") . "-ZYX";

        $mens = "--$boundary\n";
        $mens .= "Content-Transfer-Encoding: 8bits\n";
        $mens .= "Content-Type: text/html; charset=\"iso-8859-1\"\n\n"; //plain
        $mens .= "$this->corpo\n";
        $mens .= "--$boundary\n";
        $mens .= "Content-Type: ".$tipo."\n";
        $mens .= "Content-Disposition: attachment; filename=\"".$nome_arquivo."\"\n";
        $mens .= "Content-Transfer-Encoding: base64\n\n";
        $mens .= "$anexo\n";
        $mens .= "--$boundary--\r\n";

        $this->corpo = $mens; 

        $this->headers  = "MIME-Version: 1.0\n";
        $this->headers .= "From: $this->remetente <$this->default_sender_mail>\r\n";
        $this->headers .= "Return-Path:  $this->default_sender_name <$this->default_sender_mail>\n";
        $this->headers .= "Content-type: multipart/mixed; boundary=\"$boundary\"\r\n";
        $this->headers .= "$boundary\n";

      }
  }


}
