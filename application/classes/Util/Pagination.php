<? defined('SYSPATH') OR die('No direct access allowed.');

class Util_Pagination {

	public static function factory($total, $limit = null, $page = null, $adjacents = 2)
	{
		$limit = $limit!=null ? $limit : 25;
		 
		$page = $page==null ? Arr::get($_GET, 'p', 0) < 1 ? 1 : Arr::get($_GET, 'p', 0) : $page;
		
		return new Util_Pagination($total, $limit, $page, $adjacents);
	}

	public $total;

	public $limit;

	public $page;

	public $adjacents;
	
	public $page_itens;
	public $page_from;
	public $page_to;

	public function __construct($total, $limit, $page, $adjacents)
	{
		$this->total = (int) $total;
		$this->limit = (int) $limit;
		$this->page = (int) $page;
		$this->adjacents = (int) $adjacents;

		if ($this->page < 1)
		{
			$this->page = 1;
		}
		$this->render();
	}

	public function render()
	{
		$last = ceil($this->total / $this->limit);

//		if ($last < 2 or $this->page > $last)
//		{
//			return FALSE;
//		}

		$arr = array(
			'current' => $this->page,
			'limit' => $this->limit,
			'total' => $this->total,
			'pages' => $last
		);

		if ($this->page > 1)
		{
			$arr['previous'] = $this->page - 1;
		}

		if ($this->page < $last)
		{
			$arr['next'] = $this->page + 1;
		}

		$r1 = ($this->page - 1) * $this->limit + 1;

		$r2 = $r1 + $this->limit - 1;

		$r2 = ($this->total < $r2) ? $this->total : $r2;

		$arr['displaying'] = array(
			'items' => $r2 - $r1 + 1,
			'from' => $r1,
			'to' => $r2
		);
		
		$this->page_itens = $r2 - $r1 + 1;
		$this->page_from = $r1;
		$this->page_to = $r2;
		

		if ($last < 5 + ($this->adjacents * 2))
		{
			for ($counter = 1; $counter <= $last; $counter++)
			{
				$arr['links'][] = $this->_numbers($counter);
			}
		}
		elseif ($last >= 5 + ($this->adjacents * 2))
		{
			if ($this->page < 1 + ($this->adjacents * 2))
			{
				$arr['last'] = TRUE;

				for ($counter = 1; $counter < 2 + ($this->adjacents * 2); $counter++)
				{
					$arr['links'][] = $this->_numbers($counter);
				}
			}
			elseif ($last - ($this->adjacents * 2) >= $this->page AND $this->page > ($this->adjacents * 2))
			{
				$arr['first'] = TRUE;
				$arr['last'] = TRUE;

				for ($counter = $this->page - $this->adjacents; $counter <= $this->page + $this->adjacents; $counter++)
				{
					$arr['links'][] = $this->_numbers($counter);
				}
			}
			else
			{
				$arr['first'] = TRUE;

				for ($counter = $last - ($this->adjacents * 2); $counter <= $last; $counter++)
				{
					$arr['links'][] = $this->_numbers($counter);
				}
			}
		}

		return $arr;
	}

	public function render_html(){
		$par = array();
		foreach($_GET as $gk=>$gv){
			if($gk!='p'){
				array_push($par, "$gk=$gv");
			}
		}
		$par = implode('&',$par);
		$pgn = $this->render();
		
		if($this->total)
		{
    	?>
		<div class="fg-toolbar ui-toolbar ui-widget-header ui-corner-bl ui-corner-br ui-helper-clearfix">
			<div class="dataTables_info" id="example_info">Exibindo <?=$this->page_from?> a <?=$this->page_to?> de <?=$this->total?> registros</div>
			<div class="dataTables_paginate fg-buttonset ui-buttonset fg-buttonset-multi ui-buttonset-multi" id="example_paginate">
				<?=$pgn['current']>1 ? "<a class='last ui-corner-tr ui-corner-br fg-button ui-button ui-state-default' id='example_last' href='?p=1&$par'>Primeira</a>" : "<a class='previous fg-button ui-button ui-state-default ui-state-disabled' id='example_previous'>Primeira</a>";?>
				<?= isset($pgn['previous']) ? "<a class='last ui-corner-tr ui-corner-br fg-button ui-button ui-state-default' id='example_last' href='?p={$pgn['previous']}&$par'>Anterior</a>" : "<a class='previous fg-button ui-button ui-state-default ui-state-disabled' id='example_previous'>Anterior</a>";?>
				<?
				if( isset($pgn['links']) )
				{
					?>
					<span>
					<?
					foreach($pgn['links'] as $k=>$v)
					{
						?>
						<a class="fg-button ui-button ui-state-default <?=(isset($v['active'])&&$v['active']==1?"ui-state-disabled":"")?>"  href='?p=<?=$v['number'].'&'.$par?>'><?=$v['number']?></a>
						<?
					}
					?>
					</span>
					<?
				}
				?>
				<?=isset($pgn['next']) ? "<a class='last ui-corner-tr ui-corner-br fg-button ui-button ui-state-default' id='example_last' href='?p={$pgn['next']}&$par'>Próxima</a>" : "<a class='previous fg-button ui-button ui-state-default ui-state-disabled' id='example_previous'>Próxima</a>";?>
				<?=$pgn['current']<$pgn['pages'] ? "<a class='last ui-corner-tr ui-corner-br fg-button ui-button ui-state-default' id='example_last' href='?p=".$pgn['pages']."&$par'>Última</a>" : "<a class='previous fg-button ui-button ui-state-default ui-state-disabled' id='example_previous'>Última</a>";?>
			</div>
		</div>
		<?
		}
	}
	
	
	protected function _numbers($num)
	{
		if ($num == $this->page)
		{
			return array('active' => TRUE, 'number' => $num);
		}
		else
		{
			return array('number' => $num);
		}
	}

} // End Pagination
