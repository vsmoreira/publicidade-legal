<? defined('SYSPATH') OR die('No direct access allowed.');

class Util_Cupcakeform
{
	public $labels;
	public $object;
	public $errors;
	public $objtips;
	public $config;
	public $message;
	public $buttons;

	public static function factory($model_object, $model_errors, $form_message, $model_configs)
	{
		return new Util_Cupcakeform($model_object, $model_errors, $form_message, $model_configs);
	}

	public function __construct($model_object, $model_errors, $form_message, $model_configs)
	{
		$this->object = $model_object;
		$this->labels = $model_object->labels();
		$this->errors = $model_errors;
		$this->message = $form_message;
		$this->config = is_array($model_configs) ? $model_configs : array();
		$this->objtips = method_exists ( $model_object , 'tips' ) ? $model_object->tips() : array();
		
		$this->buttons = array();
		$this->add_button('<input type="submit" class="st-button" value="Salvar" id="button" name="button-submit">', 'aways');
		$this->add_button('<input type="submit" class="button-red" value="Deletar" id="button" name="button-delete">', 'loaded');
	}
	
	public function render_all()
	{
		foreach(array_keys($this->config) as $k)
		{
			$this->render($k);
		}
	}
	
	public function render($field, $input_type=null)
	{
		$field_config = (array_key_exists($field,$this->config)) ? $this->config[$field] : array();
		
		if($input_type==null) {
			$input_type = isset($field_config['type']) ? $this->config[$field]['type'] : 'text';
		}
		
		switch(isset($field_config['show']) ? $field_config['show'] : 'aways') {
			case 'loaded': $showing = $this->object->loaded(); break;
			case 'never': $showing = false; break;
			case 'aways': $showing = true; break;
		}
		
		if($showing)
		{
			switch($input_type)
			{
				case 'html': $this->_render_input_hmtl($field,$field_config); break;
				case 'text': $this->_render_input_text($field,$field_config); break;
				case 'swap': $this->_render_input_swap($field,$field_config); break;
				case 'password': $this->_render_input_text($field,$field_config,'password'); break;
				case 'select': $this->_render_input_select($field,$field_config); break;
				case 'radio': $this->_render_input_radio($field,$field_config); break;
				case 'checkbox': $this->_render_input_checkbox($field,$field_config); break;
				case 'file': $this->_render_input_file($field,$field_config); break;
				case 'image': $this->_render_input_image($field,$field_config); break;
				case 'toggle': $this->_render_input_toggle($field,$field_config); break;
				case 'textarea': $this->_render_input_textarea($field,$field_config); break;
				case 'editor': $this->_render_input_editor($field,$field_config); break;
				case 'date': $this->_render_input_date($field,$field_config); break;
			}
		}
	}
	
	private function _render_input_hmtl($field,$field_config)
	{
		?>
		<div class="st-form-line">	
	    	<span class="st-labeltext"><?=$this->labels[$field]?></span>	
	        <?=Arr::get($field_config, 'value')?>
		    <div class="clear"></div>
		</div>
		<?
	}
	
	private function _render_input_text($field,$field_config,$input_type='text')
	{
		$tip = array_key_exists($field, $this->objtips) ? "original-title='{$this->objtips[$field]}'" : '';
		$style = Arr::get($field_config,'style','width:280px;');
		$disable = isset($field_config['disabled'])&&$field_config['disabled']===true?'disabled':'';
		$readonly = isset($field_config['readonly'])&&$field_config['readonly']===true?'readonly':'';
		$classes = array('st-forminput','tips-right', $disable);
		$errrmsg = null;
		
		if(is_array($this->errors) && array_key_exists($field, $this->errors)){
			array_push($classes, 'st-error-input');
			$errrmsg = " <span class='st-form-error'>* {$this->errors[$field]}</span>";
		}
		?>
		<div class="st-form-line">	
	    	<span class="st-labeltext"><?=$this->labels[$field]?></span>	
	        <input type="<?=$input_type?>" <?=$disable?> id="<?=$field?>" name="<?=$field?>" value="<?=$this->object->$field?>" style="<?=$style?>" class="<?=implode(' ',$classes)?>" <?=$tip?>> <?=$errrmsg?>
		    <div class="clear"></div>
		</div>
		<?
	}
	
	private function _render_input_swap($field,$field_config)
	{
		$tip = array_key_exists($field, $this->objtips) ? "original-title='{$this->objtips[$field]}'" : '';
		$disable = isset($field_config['disabled'])&&$field_config['disabled']===true?'disabled':'';
		$classes = array('st-forminput','tips-right', $disable);
		$errrmsg = null;
		
		if(is_array($this->errors) && array_key_exists($field, $this->errors)){
			array_push($classes, 'st-error-input');
			$errrmsg = " <span class='st-form-error'>* {$this->errors[$field]}</span>";
		}
		?>
		<style>
		.multiple-swap select {width:280px;height:112px;}
		.multiple-swap select option {padding:5px 0px;}
		.multiple-swap td.target {width:280px;padding-bottom:10px;}
		.multiple-swap td.control {width:120px;padding-bottom:10px;text-align:center;}
		.multiple-swap td.source {width:280px;padding-bottom:10px;}
		</style>
		
		<div class="st-form-line" style="z-index: 750;">	
		<table class="multiple-swap">
			<tr>
				<td class="target"><span class="st-labeltext"><?=$this->config[$field]['target_label']?></span></td>
				<td class="control">&nbsp;</td>
				<td class="source"><span class="st-labeltext"><?=$this->config[$field]['origin_label']?></span></td>
			</tr>
			<tr>
				<td>
					<select class="tips-right" name="<?=$field?>[]" id="<?=$field?>_target" multiple <?=$disable?> <?=$tip?>>
					<?
					foreach($this->config[$field]['target_options'] as $item){
						$item = $this->config[$field]['origin_options'][$item];
						?>
						<option value='<?=$item['value']?>'><?=$item['label']?></option>
						<?
					}
					?>
					</select>
				</td>
				<td class="control">
					<div><a href="#" id="<?=$field?>_button_1" class="button-gray"> < </a></div>
					<br clear="all">
					<div><a href="#" id="<?=$field?>_button_2" class="button-gray"> > </a></div>
				</td>
				<td>
					<select class="tips-right" name="<?=$field?>_origin" id="<?=$field?>_origin" multiple <?=$disable?> <?=$tip?>>
					<?
					foreach($this->config[$field]['origin_options'] as $item)
					{
						if( ! in_array($item['value'], $this->config[$field]['target_options']))
						{
							?>
							<option value='<?=$item['value']?>'><?=$item['label']?></option>
							<?
						}
					}
					?>
					</select>
				</td>
			</tr>
			<script>
			$(document).ready(function(){

				var swapSelectOptions = function (selectFrom,selectTo,swapAll) { 
				    if (typeof(selectFrom) == "string") {
				        availableitems = document.getElementById(selectFrom); 
				    }
				    if (typeof(selectTo) == "string") { 
				        selecteditems= document.getElementById(selectTo); 
				    } 
				    for (var i = 0; i < availableitems.length; i++) { 
				        if (availableitems.options[i].selected || swapAll) { 
				            selecteditems.options[selecteditems.options.length] = new Option(availableitems.options[i].text);
				            selecteditems.options[selecteditems.options.length-1].value = availableitems.options[i].value;
				            availableitems.options[i].selected = false; availableitems.options[i] = null; i--;
				        }
				    }
				}
				
				$("#<?=$field?>_target").bind('dblclick', function(){
					swapSelectOptions("<?=$field?>_target","<?=$field?>_origin",false);
				});

				$("#<?=$field?>_origin").bind('dblclick', function(){
					swapSelectOptions("<?=$field?>_origin","<?=$field?>_target",false);
				});
			    
				$("#<?=$field?>_button_1").bind('click', function(){
					swapSelectOptions("<?=$field?>_origin","<?=$field?>_target",false);
					return false;
				});
				$("#<?=$field?>_button_2").bind('click', function(){
					swapSelectOptions("<?=$field?>_target","<?=$field?>_origin",false);
					return false;
				});

				$("#<?=$field?>_target").parents("form").submit(function(){
					$("option", "#<?=$field?>_target").attr('selected', 'selected');
				});
			});
			</script>
		</table>
		
		<?=$errrmsg?>
		
		<div class="clear"></div>
		
		</div>
		<?
	}
	
	private function _render_input_select($field,$field_config)
	{
//		$mandatory = FALSE;
//		$rules = $this->object->rules();
//		foreach($rules[$field] as $rule)
//		{
//			if($rule[0]=='not_empty')
//			{
//				$mandatory = TRUE;
//				break;
//			}
//		}
		
		$tip = array_key_exists($field, $this->objtips) ? "original-title='{$this->objtips[$field]}'" : '';
		$style = Arr::get($field_config,'style','width:280px;');
		$disable = isset($field_config['disabled'])&&$field_config['disabled']===true?'disabled':'';
		$readonly = isset($field_config['readonly'])&&$field_config['readonly']===true?'readonly=true':'';
		$classes = array('st-forminput','uniform','tips-right', $disable);
		$errrmsg = null;
		
		if(is_array($this->errors) && array_key_exists($field, $this->errors)){
			array_push($classes, 'st-error-input');
			$errrmsg = " <span class='st-form-error'>* {$this->errors[$field]}</span>";
		}
		?>
		<div class="st-form-line">	
	    	<span class="st-labeltext"><?=$this->labels[$field]?></span>	
	    	<?
	    	if(array_key_exists($field,$this->config) && array_key_exists('options',$this->config[$field]))
	    	{
	    		?>
			    <select class="<?=implode(' ',$classes)?>" <?=$disable?> <?=$readonly?> id="<?=$field?>" name="<?=$field?>" style="opacity: 0;<?=$style?>" <?=$tip?>>
			    <option <?=!($this->object->$field)?'selected':''?>></option>
	    		<?
	    		foreach($this->config[$field]['options'] as $value=>$optlabel)
	    		{
	    			$selected = $this->object->$field==$value ? ' selected ' : '';
	    			echo "<option $selected value='{$value}'>{$optlabel}</option>";
	    		}
	    		?>
				</select>
	    		<?
	    	}
	    	else
	    	{
	    		echo _('There is no option avaliable');
	    	}
	    	?>
        	<?=$errrmsg?>
        	
		
		    <div class="clear"></div>
		</div>
		<?
	}
	
	private function _render_input_radio($field,$field_config)
	{
		$tip = array_key_exists($field, $this->objtips) ? "original-title='{$this->objtips[$field]}'" : '';
		$disable = isset($field_config['disabled'])&&$field_config['disabled']===true?'disabled':'';
		$errrmsg = (is_array($this->errors) && array_key_exists($field, $this->errors)) ? " <span class='st-form-error'>* {$this->errors[$field]}</span>" : null;

		?>
		<div class="st-form-line">	
	    	<span class="st-labeltext"><?=$this->labels[$field]?></span>
	    	<?
	    	if(array_key_exists($field,$this->config) && array_key_exists('options',$this->config[$field]))
	    	{
	    		foreach($this->config[$field]['options'] as $value=>$optlabel)
	    		{
	    			?>
		        	<label class="margin-right10 tips-right" <?=$tip?>><div class="radio" id="uniform-undefined"><span><div class="radio"><span><input type="radio" <?=$disable?> class="uniform <?=(Arr::get($field_config['disabled'],false)?'disabled':'')?>" id="<?=$field?>" name="<?=$field?>" value="<?=$value?>" style="opacity: 0;" <?=$this->object->$field==$value ? 'checked' : ''?> <?=$tip?>></span></div></span></div> <?=$optlabel?></label> 
	    			<?
	    		}
	    	}
	    	else
	    	{
	    		echo _('There is no option avaliable');
	    	}
	    	?>
        	<?=$errrmsg?>
		    <div class="clear"></div>
		</div>
		<?
	}
	
	private function _render_input_checkbox($field,$field_config)
	{
		?>
		<div class="st-form-line">	
        	<span class="st-labeltext">Language</span>	
            <label class="margin-right10"><div class="checker" id="uniform-checkbox1"><span><div class="checker" id="uniform-checkbox1"><span><input type="checkbox" class="uniform" id="checkbox1" name="checkbox" style="opacity: 0;"></span></div></span></div> Checkbox</label>
      		<div class="clear"></div> 
        </div>
		<?
	}
	
	private function _render_input_file($field,$field_config)
	{
		?>
		<div class="st-form-line">	
			<span class="st-labeltext">Upload Your File</span>	
		    <div class="uploader" id="uniform-undefined"><div class="uploader" id="uniform-undefined"><input type="file" class="uniform" size="19" style="opacity: 0;"><span class="filename" style="-moz-user-select: none;">No file selected</span><span class="action" style="-moz-user-select: none;">Choose File</span></div><span class="filename" style="-moz-user-select: none;">No file selected</span><span class="action" style="-moz-user-select: none;">Choose File</span></div>
			<div class="clear"></div> 
		</div>
		<?
	}
	
	private function _render_input_date($field,$field_config)
	{
		$tip = array_key_exists($field, $this->objtips) ? "original-title='{$this->objtips[$field]}'" : '';
		$disable = isset($field_config['disabled'])&&$field_config['disabled']===true?'disabled':'';
		$classes = array('datepicker-input', 'tips-right', $disable);
		$errrmsg = null;
		
		if(is_array($this->errors) && array_key_exists($field, $this->errors)){
			array_push($classes, 'st-error-input');
			$errrmsg = " <span class='st-form-error'>* {$this->errors[$field]}</span>";
		}
		?>
		<div class="st-form-line">	
	    	<span class="st-labeltext"><?=$this->labels[$field]?></span>	

			<script type="text/javascript">
				$(function() { $( "#s<?=$field?>" ).datepicker({ dateFormat:'dd/mm/yy'}); });
			</script>
			<input type="text" id="s<?=$field?>" <?=$disable?> name="<?=$field?>" value="<?=($this->object->$field != null)?date('d/m/Y',strtotime($this->object->$field)):''?>" class="<?=implode(' ',$classes)?>" style="width:180px;" <?=$tip?>> <?=$errrmsg?>

		    <div class="clear"></div>
		</div>
		<?
	}
	
	private function _render_input_image($field,$field_config)
	{
		$tip = array_key_exists($field, $this->objtips) ? "original-title='{$this->objtips[$field]}'" : '';
		$disable = isset($field_config['disabled'])&&$field_config['disabled']===true?'disabled':'';
		$classes = array('uniform', $disable);
		$errrmsg = null;
		
		if(is_array($this->errors) && array_key_exists($field, $this->errors)){
			array_push($classes, 'st-error-input');
			$errrmsg = " <span class='st-form-error'>* {$this->errors[$field]}</span>";
		}
		
		$we_have_an_image = !empty($this->object->$field);
		?>
		<div class="st-form-line" style="z-index: 500;">	
            <table border="1" style="width:100%;">
             <tr valign="middle">
              <td style="width:150px;" rowspan="2"><span class="st-labeltext"><?=Arr::get($this->labels,$field,$field)?></span></td>
              <? if($we_have_an_image){ ?>
              <td style="width:60px;text-align:center;" rowspan="2"><img src="<?=$this->object->$field?>" width="60" height="60"> </td>
              <? } ?>
              <td>
              	<div class="uploader" id="uniform-undefined">
              		<input type="file" <?=$tip?> class="<?=implode(' ',$classes)?>" size="19" name="<?=$field?>" <?=$disable?> style="opacity: 0;">
              		<span class="filename" style="-moz-user-select: none;">Nenhum aquivo selecionado</span>
              		<span class="action" style="-moz-user-select: none;">Selecione</span>
          		</div>
              	<?=$errrmsg?>
              </td>
             </tr>
             <? if($we_have_an_image){ ?>
	             <tr>
	             	<td>
	             		<input type="checkbox" value="1" id="<?=$field?>_delete" <?=$disable?> name="<?=$field?>_delete">
						<label for="<?=$field?>_delete">check to delete this image on next save</label>
					</td>
				</tr>
             <? } ?>
            </table>
      		<div class="clear" style="z-index: 490;"></div> 
        </div>
		<?
	}
	
	private function _render_input_toggle($field,$field_config)
	{
		$tip = array_key_exists($field, $this->objtips) ? "original-title='{$this->objtips[$field]}'" : '';
		$disable = isset($field_config['disabled'])&&$field_config['disabled']===true?'disabled':'';
		$classes = array('st-forminput','tips-right', $disable);
		$errrmsg = null;
		
		if(is_array($this->errors) && array_key_exists($field, $this->errors)){
			array_push($classes, 'st-error-input');
			$errrmsg = " <span class='st-form-error'>* {$this->errors[$field]}</span>";
		}
		?>
		<div class="st-form-line">	
	    	<span class="st-labeltext"><?=$this->labels[$field]?></span>	
	    	<?
	    	if(array_key_exists($field,$this->config) && array_key_exists('options',$this->config[$field]))
	    	{
	    		if(count($this->config[$field]['options']) == 2)
	    		{
	    		?>
	    			<p class="field switch" <?=$tip?>>
		        	<label class="cb-enable <?="{$this->object->$field}"==="{$this->config[$field]['options'][0]['value']}" ? ' selected ' : ''?>" id="<?=$field?>" <?=$disable?> for="radio_<?=$field?>_<?=$this->config[$field]['options'][0]['value']?>">
		        		<input type='radio' id="radio_<?=$field?>_<?=$this->config[$field]['options'][0]['value']?>" name="<?=$field?>" value="<?=$this->config[$field]['options'][0]['value']?>" <?=$disable?> style="display:none;" <?="{$this->object->$field}"==="{$this->config[$field]['options'][0]['value']}" ? ' checked ' : ''?>>
		        		<span><?=$this->config[$field]['options'][0]['label']?></span>
		        	</label>
		        	<label class="cb-disable <?="{$this->object->$field}"==="{$this->config[$field]['options'][1]['value']}" ? ' selected ' : ''?>" id="<?=$field?>" <?=$disable?> for="radio_<?=$field?>_<?=$this->config[$field]['options'][1]['value']?>">
		        		<input type='radio' id="radio_<?=$field?>_<?=$this->config[$field]['options'][1]['value']?>" name="<?=$field?>" value="<?=$this->config[$field]['options'][1]['value']?>" <?=$disable?> style="display:none;" <?="{$this->object->$field}"==="{$this->config[$field]['options'][1]['value']}" ? ' checked ' : ''?>>
		        		<span><?=$this->config[$field]['options'][1]['label']?></span>
		        	</label>
					</p>
				<?
	    		}
	    		else
	    		{
	    			throw new Oca_Exception(_('Toggle option may have two options'));
	    		}
	    	}
	    	else
	    	{
	    		echo _('There is no option avaliable');
	    	}
	    	?>
	    	<?=$errrmsg?>
		    <div class="clear"></div>
		</div>
		<?
	}
	
	private function _render_input_textarea($field,$field_config)
	{
		$tip = array_key_exists($field, $this->objtips) ? "original-title='{$this->objtips[$field]}'" : '';
		$style = Arr::get($field_config,'style','width:510px;');
		$disable = isset($field_config['disabled'])&&$field_config['disabled']===true?'disabled':'';
		$classes = array('st-forminput','tips-right', $disable);
		$errrmsg = null;
		
		if(is_array($this->errors) && array_key_exists($field, $this->errors)){
			array_push($classes, 'st-error-input');
			$errrmsg = " <span class='st-form-error'>* {$this->errors[$field]}</span>";
		}
		?>
		<div class="st-form-line">	
	    	<span class="st-labeltext"><?=$this->labels[$field]?></span>	
	        <textarea <?=$disable?> cols="47" rows="3" style="<?=$style?>" id="<?=$field?>" class="<?=implode(' ',$classes)?>" name="<?=$field?>"  <?=$tip?>><?=$this->object->$field?></textarea> 
	        <?=$errrmsg?>
		    <div class="clear"></div>
		</div>
		<?
	}

	
	private function _render_input_editor($field,$field_config)
	{
		?>
		<div style="width: 712px;" class="wysiwyg"><ul class="panel" role="menu"><li><a href="javascript:;" tabindex="-1" role="menuitem" class="bold" title="Bold">bold</a></li><li><a href="javascript:;" tabindex="-1" role="menuitem" class="italic" title="Italic">italic</a></li><li><a href="javascript:;" tabindex="-1" role="menuitem" class="strikeThrough" title="Strike-through">strikeThrough</a></li><li><a href="javascript:;" tabindex="-1" role="menuitem" class="underline" title="Underline">underline</a></li><li class="separator" role="separator"></li><li><a href="javascript:;" tabindex="-1" role="menuitem" class="justifyLeft" title="Justify Left">justifyLeft</a></li><li><a href="javascript:;" tabindex="-1" role="menuitem" class="justifyCenter" title="Justify Center">justifyCenter</a></li><li><a href="javascript:;" tabindex="-1" role="menuitem" class="justifyRight" title="Justify Right">justifyRight</a></li><li><a href="javascript:;" tabindex="-1" role="menuitem" class="justifyFull" title="Justify Full">justifyFull</a></li><li class="separator" role="separator"></li><li><a href="javascript:;" tabindex="-1" role="menuitem" class="indent" title="Indent">indent</a></li><li><a href="javascript:;" tabindex="-1" role="menuitem" class="outdent" title="Outdent">outdent</a></li><li><a href="javascript:;" tabindex="-1" role="menuitem" class="subscript" title="Subscript">subscript</a></li><li><a href="javascript:;" tabindex="-1" role="menuitem" class="superscript" title="Superscript">superscript</a></li><li class="separator" role="separator"></li><li><a href="javascript:;" tabindex="-1" role="menuitem" class="undo" title="Undo">undo</a></li><li><a href="javascript:;" tabindex="-1" role="menuitem" class="redo" title="Redo">redo</a></li><li class="separator" role="separator"></li><li><a href="javascript:;" tabindex="-1" role="menuitem" class="insertOrderedList" title="Insert Ordered List">insertOrderedList</a></li><li><a href="javascript:;" tabindex="-1" role="menuitem" class="insertUnorderedList" title="Insert Unordered List">insertUnorderedList</a></li><li><a href="javascript:;" tabindex="-1" role="menuitem" class="insertHorizontalRule" title="Insert Horizontal Rule">insertHorizontalRule</a></li><li class="separator" role="separator"></li><li><a href="javascript:;" tabindex="-1" role="menuitem" class="createLink" title="Create link">createLink</a></li><li><a href="javascript:;" tabindex="-1" role="menuitem" class="insertImage" title="Insert image">insertImage</a></li><li class="separator" role="separator"></li><li><a href="javascript:;" tabindex="-1" role="menuitem" class="h1" title="Header 1">h1</a></li><li><a href="javascript:;" tabindex="-1" role="menuitem" class="h2" title="Header 2">h2</a></li><li><a href="javascript:;" tabindex="-1" role="menuitem" class="h3" title="Header 3">h3</a></li><li class="separator" role="separator"></li><li><a href="javascript:;" tabindex="-1" role="menuitem" class="removeFormat" title="Remove formatting">removeFormat</a></li></ul><div style="clear: both; z-index: 420;"><!-- --></div><iframe frameborder="0" src="javascript:false;" style="min-height: 81px; width: 704px;" id="wysiwygIFrame" tabindex="0"></iframe></div>
		<?
	}

	public function add_button($html, $showing='aways')
	{
		if($showing=='aways' || ($showing='loaded' && $this->object->loaded())){
			array_push($this->buttons, $html);
		}
	}
	
	public function render_buttons()
	{
		?>
		<div class="button-box">
		<?
		foreach($this->buttons as $item)
		{
			echo $item;
		}
		?>
        </div>
		<?
	}

}