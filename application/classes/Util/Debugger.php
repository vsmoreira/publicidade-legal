<? defined('SYSPATH') OR die('No direct access allowed.');

class Util_Debugger
{
    public static function show($msg)
    {
    	echo "<pre style='background-color:#fff'>\n\n\n";
    	print_r($msg);
    	echo "\n\n\n</pre>\n\n\n";
    }
}