<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Book extends Controller_Admin_Base {
	
	public static function rights()
	{
		return array(
			'url' => URL::site(Route::get('default')->uri(array('directory'=>'admin', 'controller'=>'Book', 'action'=>'list'))),
			'icon' => '/media/cupcake/img/icons/sidemenu/content.png',
			'label' => 'Caderno',
			'order' => '100700',
			'controller' => 'Book',
			'requires'=>Kohana::$config->load('rights.RIGHT_BOOK_READ.value'),
		);
	}

	public static function profile()
	{
		return array(
			'page_title' => _('caderno'),
			'page_subtitle' => _('Aqui você pode administrar todos os cadernos.'),
			'page_back_label' => _('Voltar para listagem'),
			'titleh_form' => _('Formulário de caderno'),
			'titleday_form' => _('Dia e Hora do Fechamento'),
			'page_url_list' => URL::site(Route::get('default')->uri(array('directory'=>'admin', 'controller'=>'Book', 'action'=>'list'))),
			'page_url_item' => URL::site(Route::get('default')->uri(array('directory'=>'admin', 'controller'=>'Book', 'action'=>'edit'))),
		);
	}

	public function action_list()
	{
		$this->check_permissions(Kohana::$config->load('rights.RIGHT_BOOK_READ.value'));
		
		$lbls = ORM::factory('Book')->labels();
		$list = ORM::factory('Book')->order_by($lof = 'id', $lod = 'desc');
		$lurl = URL::site(Route::get('default')->uri(array('directory'=>'admin', 'controller'=>'Book', 'action'=>'edit')));
		
		if($resmsg = Session::instance()->get('result_message', null))
		{
			Session::instance()->delete('result_message');
		}

		if(isset($_GET['search']))
		{
			$s = htmlentities($_GET['search']);
			$list = $list->where('description','like',"%{$s}%")->or_where('description','like',"%{$s}%");
		}
		
		$list = $list->order_by('id','desc')->find_all();
		
		$prms = array_merge($this->profile(), array('list'=>$list, 'labels'=>$lbls, 'url'=>$lurl, 'list_order_field'=>$lof, 'list_order_drctn'=>$lod, 'pagination'=>Util_Pagination::factory($list->count()), 'message'=>$resmsg));

		$this->_response_render(null, View::factory(Kohana::$config->load('system.admin_theme') . '/admin_books_inedex', $prms)->render());

	}

	public function action_edit()
	{
		
		$this->check_permissions(Kohana::$config->load('rights.RIGHT_BOOK_READ.value'), Kohana::$config->load('rights.RIGHT_BOOK_EDIT.value'));
		
		$errros = null;
		
		$objc = ORM::factory('Book', $this->request->param('id'));
		
		if( (! $objc->loaded() && $this->request->param('id')))
		{
			throw new HTTP_Exception_404('Not found');
		}

		if($resmsg = Session::instance()->get('result_message', null))
		{
			Session::instance()->delete('result_message');
		}
		
		if($_POST)
		{
			try
			{
				if(isset($_POST['button-delete']))
				{
					return $this->action_delete();
				}
				
				$objc->values(Arr::extract($_POST, array('description','max_height','sunday','monday','tuesday','wednesday','thursday','friday','saturday')));
				
				if($objc->save())
				{
					/** lidando com a lista de formatos **/
					$ids_to_keep = array();
					if(isset($_POST['formats']) && isset($_POST['formats']['id']))
					{
						foreach($_POST['formats']['id'] as $key=>$format_id)
						{
							$format = ORM::factory('BookFormat', $format_id);
							$format->name = $_POST['formats']['name'][$key];
							$format->width = $_POST['formats']['width'][$key];
							if($format->loaded() && $format->book_id != $objc->pk())
							{
								throw new Oca_Exception('O formato de id='.$format_id.' nao pertence ao caderno atual');
							}
							else
							{
								$format->book_id = $objc->pk();
							}
							$format->save();
					
							array_push($ids_to_keep, $format->pk());
						}
					}
					$objc->batch_delete_formats($ids_to_keep);
					/** /lidando com a lista de formatos **/
					
					Session::instance()->set('result_message', array('success'=>true, 'message'=>_('Caderno atualizado com sucesso.')));
					$this->redirect( URL::site(Route::get( 'default' )->uri(array('directory'=>'admin','controller'=>'Book','action'=>'edit','id'=>"{$objc->pk()}"))) );
				}
			}
			catch(ORM_Validation_Exception $e)
			{
				$errros = $e->errors('models');
				$resmsg = array('success'=>false, 'message'=>_('Erros em ') . implode(', ', array_values(Arr::extract($objc->labels(), array_keys($errros)))));
			}
		}
		
		$form = Util_Cupcakeform::factory($objc, $errros, $resmsg, array(
				'description'	=> array('type'=>'text', 'show'=>'aways', 'disabled'=>false),
				'max_height'	=> array('type'=>'text', 'show'=>'aways', 'disabled'=>false),
				'sunday'		=> array('type'=>'select',	 'show'=>'aways',	'disabled'=>false, 'options'=>Model_Book::$_closing_time),
				'monday'		=> array('type'=>'select',	 'show'=>'aways',	'disabled'=>false, 'options'=>Model_Book::$_closing_time),
				'tuesday'		=> array('type'=>'select',	 'show'=>'aways',	'disabled'=>false, 'options'=>Model_Book::$_closing_time),
				'wednesday'		=> array('type'=>'select',	 'show'=>'aways',	'disabled'=>false, 'options'=>Model_Book::$_closing_time),
				'thursday'		=> array('type'=>'select',	 'show'=>'aways',	'disabled'=>false, 'options'=>Model_Book::$_closing_time),
				'friday'		=> array('type'=>'select',	 'show'=>'aways',	'disabled'=>false, 'options'=>Model_Book::$_closing_time),
				'saturday'		=> array('type'=>'select',	 'show'=>'aways',	'disabled'=>false, 'options'=>Model_Book::$_closing_time)
		));
					
		$this->_response_render(null, View::factory(Kohana::$config->load('system.admin_theme') . '/admin_book_form', array_merge($this->profile(), array('form'=>$form)))->render());
	}

	public function action_delete()
	{
		$this->check_permissions(Kohana::$config->load('rights.RIGHT_BOOK_DELETE.value'));

		$objc = ORM::factory('Book', $this->request->param('id'));
		
		if($objc->loaded())
		{
			try
			{
				$objc->delete();
				
				Session::instance()->set('result_message', array('success'=>true, 'message'=>_('Caderno excluido com sucesso.')));
			}
			catch(Exception $e)
			{
				$errros = $e->errors('models');
				Session::instance()->set('result_message', array('success'=>false, 'message'=>$e->getmessage() .': '. implode(', ', array_values(Arr::extract($objc->labels(), array_keys($errros))))));
				$this->redirect( URL::site(Route::get( 'default' )->uri(array('directory'=>'admin','controller'=>'Book','action'=>'edit','id'=>"{$objc->pk()}"))) );
			}
		}

		$this->redirect( URL::site(Route::get( 'default' )->uri(array('directory'=>'admin','controller'=>'Book','action'=>'list'))) );
	}

} 
