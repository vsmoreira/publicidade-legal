<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Client extends Controller_Admin_Base {
	
	public static function rights()
	{
		return array(
			'url' => URL::site(Route::get('default')->uri(array('directory'=>'admin', 'controller'=>'Client', 'action'=>'list'))),
			'icon' => '/media/cupcake/img/icons/sidemenu/content.png',
			'label' => 'Clientes',
			'order' => '100600',
			'controller' => 'Client',
			'requires'=>Kohana::$config->load('rights.RIGHT_CLIENT_READ.value'),
		);
	}

	public static function profile()
	{
		return array(
			'page_title' => _('clientes'),
			'page_subtitle' => _('Aqui você pode administrar todos os clientes.'),
			'page_back_label' => _('Voltar para listagem'),
			'titleh_form' => _('Formulário de cliente'),
			'page_url_list' => URL::site(Route::get('default')->uri(array('directory'=>'admin', 'controller'=>'Client', 'action'=>'list'))),
			'page_url_item' => URL::site(Route::get('default')->uri(array('directory'=>'admin', 'controller'=>'Client', 'action'=>'edit'))),
		);
	}

	public function action_list()
	{
		$this->check_permissions(Kohana::$config->load('rights.RIGHT_CLIENT_READ.value'));
		
		$lbls = ORM::factory('Client')->labels();
		$list = ORM::factory('Client')->order_by($lof = 'id', $lod = 'desc');
		$lurl = URL::site(Route::get('default')->uri(array('directory'=>'admin', 'controller'=>'Client', 'action'=>'edit')));
		
		if($resmsg = Session::instance()->get('result_message', null))
		{
			Session::instance()->delete('result_message');
		}

		if(isset($_GET['search']))
		{
			$s = htmlentities($_GET['search']);
			$list = $list->where('corporate_name','like',"%{$s}%")->or_where('email','like',"%{$s}%");
		}
		
		$list = $list->order_by('id','desc')->find_all();
		
		$prms = array_merge($this->profile(), array('list'=>$list, 'labels'=>$lbls, 'url'=>$lurl, 'list_order_field'=>$lof, 'list_order_drctn'=>$lod, 'pagination'=>Util_Pagination::factory($list->count()), 'message'=>$resmsg));

		$this->_response_render(null, View::factory(Kohana::$config->load('system.admin_theme') . '/admin_clients_inedex', $prms)->render());

	}

	public function action_edit()
	{
		
		$this->check_permissions(Kohana::$config->load('rights.RIGHT_CLIENT_READ.value'), Kohana::$config->load('rights.RIGHT_CLIENT_EDIT.value'));
		
		$errros = null;
		
		$objc = ORM::factory('Client', $this->request->param('id'));
		
		if( (! $objc->loaded() && $this->request->param('id')))
		{
			throw new HTTP_Exception_404('Not found');
		}

		if($resmsg = Session::instance()->get('result_message', null))
		{
			Session::instance()->delete('result_message');
		}
		
		if($_POST)
		{
			try
			{
				
				if(isset($_POST['button-delete']))
				{
					return $this->action_delete();
				}
				
				if(! $objc->loaded())
				{
					$objc->values(Arr::extract($_POST, array('corporate_name','fantasy_name','cpf_cnpj','address','address_neighborhood','address_city','address_state','address_zip','phone_primary','phone_secondary','email')));
					Session::instance()->set('result_message', array('success'=>true, 'message'=>_('Cliente adicinado com sucesso')));					
				}
				else{
					Session::instance()->set('result_message', array('success'=>true, 'message'=>_('Cliente atualizado com sucesso')));
				}
				
				$objc->save();
				$this->redirect( URL::site(Route::get( 'default' )->uri(array('directory'=>'admin','controller'=>'Client','action'=>'edit','id'=>"{$objc->pk()}"))) );
			}
			catch(ORM_Validation_Exception $e)
			{
				$errros = $e->errors('models');
				$resmsg = array('success'=>false, 'message'=>_('Erros em ') . implode(', ', array_values(Arr::extract($objc->labels(), array_keys($errros)))));
			}
		}
		
		$form = Util_Cupcakeform::factory($objc, $errros, $resmsg, array(
					'corporate_name'		=> array('type'=>'text', 'show'=>'aways', 'disabled'=>false),
					'fantasy_name'	 		=> array('type'=>'text', 'show'=>'aways', 'disabled'=>false),
					'cpf_cnpj'				=> array('type'=>'text', 'show'=>'aways', 'disabled'=>false),
					'address'				=> array('type'=>'text', 'show'=>'aways', 'disabled'=>false),
					'address_neighborhood'	=> array('type'=>'text', 'show'=>'aways', 'disabled'=>false),
					'address_city'			=> array('type'=>'text', 'show'=>'aways', 'disabled'=>false),
					'address_state'			=> array('type'=>'text', 'show'=>'aways', 'disabled'=>false),
					'address_zip'			=> array('type'=>'text', 'show'=>'aways', 'disabled'=>false),
					'phone_primary'			=> array('type'=>'text', 'show'=>'aways', 'disabled'=>false),
					'phone_secondary'		=> array('type'=>'text', 'show'=>'aways', 'disabled'=>false),
					'email'					=> array('type'=>'text', 'show'=>'aways', 'disabled'=>false),
					));
					
		$this->_response_render(null, View::factory(Kohana::$config->load('system.admin_theme') . '/admin_default_form', array_merge($this->profile(), array('form'=>$form)))->render());
	}

	public function action_delete()
	{
		$this->check_permissions(Kohana::$config->load('rights.RIGHT_CLIENT_DELETE.value'));

		$objc = ORM::factory('Client', $this->request->param('id'));
		
		if($objc->loaded())
		{
			try
			{
				$objc->delete();
				
				Session::instance()->set('result_message', array('success'=>true, 'message'=>_('Cliente excluido com sucesso.')));
			}
			catch(Exception $e)
			{
				$errros = $e->errors('models');
				Session::instance()->set('result_message', array('success'=>false, 'message'=>$e->getmessage() .': '. implode(', ', array_values(Arr::extract($objc->labels(), array_keys($errros))))));
				$this->redirect( URL::site(Route::get( 'default' )->uri(array('directory'=>'admin','controller'=>'client','action'=>'edit','id'=>"{$objc->pk()}"))) );
			}
		}

		$this->redirect( URL::site(Route::get( 'default' )->uri(array('directory'=>'admin','controller'=>'client','action'=>'list'))) );
	}

} 
