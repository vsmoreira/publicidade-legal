<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Order extends Controller_Admin_Base {
	
	public static function rights()
	{
		return array(
			'url' => URL::site(Route::get('default')->uri(array('directory'=>'admin', 'controller'=>'Order', 'action'=>'list'))),
			'icon' => '/media/cupcake/img/icons/sidemenu/content.png',
			'label' => 'OCA',
			'order' => '100500',
			'controller' => 'Order',
			'requires'=>Kohana::$config->load('rights.RIGHT_ORDER_READ.value'),
		);
	}

	public static function profile()
	{
		return array(
			'page_title' => _('OCA'),
			'page_subtitle' => _('Aqui você pode administrar todas as OCAS.'),
			'page_back_label' => _('Voltar para listagem'),
			'page_url_list' => URL::site(Route::get('default')->uri(array('directory'=>'admin', 'controller'=>'Order', 'action'=>'list'))),
			'page_url_item' => URL::site(Route::get('default')->uri(array('directory'=>'admin', 'controller'=>'Order', 'action'=>'edit'))),
		);
	}
	
	public function action_list()
	{
		$this->check_permissions(Kohana::$config->load('rights.RIGHT_ORDER_READ.value'));
		$order = ORM::factory('Order');
		$status = Model_Order::$_status;
		$list = $order->order_by($lof = 'id', $lod = 'desc');
		$lurl = URL::site(Route::get('default')->uri(array('directory'=>'admin', 'controller'=>'Order', 'action'=>'edit')));
		if($resmsg = Session::instance()->get('result_message', null))
		{
			Session::instance()->delete('result_message');
		}
		
		if(isset($_GET['search']))
		{
			$s = htmlentities($_GET['search']);
			
			$list = $list
					->where_open()
					->where('order.id','like',"%{$s}%")
					->or_where('title','like',"%{$s}%")
					->or_where('description_ad','like',"%{$s}%")
					->or_where('text_ad','like',"%{$s}%")
					->where_close()
					;
		}
		
		
		
		$advanced_seach_fields = array('user_seller_id','user_designer_id','client_id','book_id','format_id','status','destination','type_ad','date_publication');
		
		foreach(array_intersect(array_keys($_GET), $advanced_seach_fields) as $field)
		{
			if($field=='date_publication')
			{
				$value = implode('-',array_reverse(explode('/',$_GET[$field])));
			}
			else 
			{
				$value = $_GET[$field];
			}
			
			if($_GET[$field]) $list = $list->and_where($field,'=',$value);
		}
		
		$sources = array(
				'sellers'=>Controller_Admin_Order::get_object_list('User', 'name', array('type'=>'SELLER')),
				'designers'=>Controller_Admin_Order::get_object_list('User', 'name', array('type'=>'DESIGNER')),
				'clients'=>Controller_Admin_Order::get_object_list('Client', 'corporate_name'),
				'books'=>Controller_Admin_Order::get_object_list('Book', 'description'),
			);
		
		$list =	$list->find_all();
		
		$prms = array_merge($this->profile(), array('status'=>$status, 'list'=>$list, 'labels'=>$order->labels(), 'url'=>$lurl, 'list_order_field'=>$lof, 'list_order_drctn'=>$lod, 'pagination'=>Util_Pagination::factory($list->count(), 50), 'message'=>$resmsg, 'advanced_seach_fields'=>$advanced_seach_fields, 'sources'=>$sources));

		$this->_response_render(null, View::factory(Kohana::$config->load('system.admin_theme') . '/admin_orders_inedex', $prms)->render());
	}
	
	
	private static function get_object_list($object_name, $label_field, $wheres=array())
	{
		$out = array();
		
		$objects = ORM::factory($object_name)->order_by($label_field);
		
		foreach($wheres as $key=>$value)
		{
			$objects = $objects->where($key,'=',$value);
		}
		
		$objects = $objects->find_all();
		
		foreach($objects as $object)
		{
			$out[$object->pk()] = $object->$label_field;
		}
		
		return $out;
	}

	public function action_edit()
	{
		$this->check_permissions(Kohana::$config->load('rights.RIGHT_ORDER_READ.value'), Kohana::$config->load('rights.RIGHT_ORDER_EDIT.value'));
		
		$errros = null;
		
		$objc = ORM::factory('Order', $this->request->param('id'));
		
		if($resmsg = Session::instance()->get('result_message', null))
		{
			Session::instance()->delete('result_message');
		}
		
		
		
		
		//*********************************************************************************************
		
		
		
		
		if($_POST)
		{			
			try
			{
				if(isset($_POST['button-send-studio']))
				{
					$objc->send_studio();
					Session::instance()->set('result_message', array('success'=>true, 'message'=>_('Ordem enviada para o estúdio!')));
					$this->redirect( URL::site(Route::get( 'default' )->uri(array('directory'=>'admin','controller'=>'Order','action'=>'edit','id'=>"{$objc->pk()}"))) );
				}
				if(isset($_POST['button-own-order']))
				{
					$objc->own_order();
					Session::instance()->set('result_message', array('success'=>true, 'message'=>_('Ordem assumida!')));
					$this->redirect( URL::site(Route::get( 'default' )->uri(array('directory'=>'admin','controller'=>'Order','action'=>'edit','id'=>"{$objc->pk()}"))) );
				}
				if(isset($_POST['button-start-development']))
				{
					$objc->start_development();
					Session::instance()->set('result_message', array('success'=>true, 'message'=>_('Desenvolvimento iniciado!')));
					$this->redirect( URL::site(Route::get( 'default' )->uri(array('directory'=>'admin','controller'=>'Order','action'=>'edit','id'=>"{$objc->pk()}"))) );
				}
				if(isset($_POST['button-remove-treatment']))
				{
					ORM::factory('Treatment', Arr::get($_POST,'button-remove-treatment-id'))->delete();
					Session::instance()->set('result_message', array('success'=>true, 'message'=>_('Emenda removida com sucesso')));
					$this->redirect( URL::site(Route::get( 'default' )->uri(array('directory'=>'admin','controller'=>'Order','action'=>'edit','id'=>"{$objc->pk()}"))) );
				}
				if(isset($_POST['button-accept-treatment']))
				{
					ORM::factory('Treatment', Arr::get($_POST,'button-accrej-treatment-id'))->accept();
					Session::instance()->set('result_message', array('success'=>true, 'message'=>_('Ordem aprovada com sucesso')));
					$this->redirect( URL::site(Route::get( 'default' )->uri(array('directory'=>'admin','controller'=>'Order','action'=>'edit','id'=>"{$objc->pk()}"))) );
				}
				if(isset($_POST['button-reject-treatment']))
				{
					ORM::factory('Treatment', Arr::get($_POST,'button-accrej-treatment-id'))->reject(Arr::get($_POST,'button-reject-treatment-reason'));
					Session::instance()->set('result_message', array('success'=>true, 'message'=>_('Emenda rejeitada com sucesso')));
					$this->redirect( URL::site(Route::get( 'default' )->uri(array('directory'=>'admin','controller'=>'Order','action'=>'edit','id'=>"{$objc->pk()}"))) );
				}
				elseif(isset($_POST['button-cancel-order']))
				{
					if( ! Arr::get($_POST,'reason-cancel-order'))
					{
						throw new Oca_Exception('A razão para o cancelamento da ordem é obrigatória');
					}
					$objc->change_status_to_cancelled(Arr::get($_POST,'reason-cancel-order'));
					Session::instance()->set('result_message', array('success'=>true, 'message'=>_('Ordem cancelada com sucesso')));
					$this->redirect( URL::site(Route::get( 'default' )->uri(array('directory'=>'admin','controller'=>'Order','action'=>'edit','id'=>"{$objc->pk()}"))) );
				}
				elseif(isset($_POST['button-awn-preprint']))
				{
					$objc->own_preprint();
					Session::instance()->set('result_message', array('success'=>true, 'message'=>_('Pré-impressão assumida com sucesso')));
					$this->redirect( URL::site(Route::get( 'default' )->uri(array('directory'=>'admin','controller'=>'Order','action'=>'edit','id'=>"{$objc->pk()}"))) );
				}
				elseif(isset($_POST['button-delete']))
				{
					return $this->action_delete();
				}
				elseif(isset($_POST['button-submit-amendment']))
				{
					if($result = Model_Treatment::add_from_post($objc))
					{
						$objc->wait_validation();
					}
					
					Session::instance()->set('result_message', array('success'=>true, 'message'=>_('Emenda adicionada com sucesso')));
					$this->redirect( URL::site(Route::get( 'default' )->uri(array('directory'=>'admin','controller'=>'Order','action'=>'edit','id'=>"{$objc->pk()}"))) );
				}
				else
				{
					if( ! $objc->loaded())
					{
						$_POST['moment_creation'] = date('Y-m-d H:i:s');	
						$_POST['date_publication'] = implode('-',array_reverse(explode('/',$_POST['date_publication'])));	
						$_POST['user_seller_id'] = Auth::instance()->get_user()->id;
						$objc->values(Arr::extract($_POST, array('user_seller_id','client_id','destination','book_id','format_id','height_ad','title','description_ad','text_ad','color','type_ad','date_publication','status','moment_creation')));
						Session::instance()->set('result_message', array('success'=>true, 'message'=>_('OCA criada com sucesso.')));
					}
					else
					{
						if($objc->status == 'ELABORATING')
						{
							$_POST['date_publication'] = implode('-',array_reverse(explode('/',$_POST['date_publication'])));
							$objc->values(Arr::extract($_POST, array('destination','book_id','format_id','height_ad','title','description_ad','text_ad','color','type_ad','date_publication')));
						}
						else
						{
							$objc->values(Arr::extract($_POST, array('title','type_ad')));
						}
						Session::instance()->set('result_message', array('success'=>true, 'message'=>_('OCA atualizada com sucesso.')));
					}
					
					$objc->save();
					
					Model_Attachment::update_attached_files($objc);
					
					$this->redirect( URL::site(Route::get( 'default' )->uri(array('directory'=>'admin','controller'=>'Order','action'=>'edit','id'=>"{$objc->pk()}"))) );
				}
			}
			catch(ORM_Validation_Exception $e)
			{
				$errros = $e->errors('models');
				$resmsg = array('success'=>false, 'message'=>_('Erros em ') . implode(', ', array_values(Arr::extract($objc->labels(), array_keys($errros)))));
			}	
			catch(Oca_Exception $e)
			{
				$resmsg = array('success'=>false, 'message'=>$e->getmessage());
			}	
		}
		
		$books = array();
		$formats = array();
		$clients = array();
		$designers = array();
		
		foreach(ORM::factory('Client')->order_by('corporate_name','asc')->find_all() as $client) $clients[$client->pk()] = $client->corporate_name;
		
		foreach(ORM::factory('Book')->order_by('description','asc')->find_all() as $book) $books[$book->pk()] = $book->description;
		
		$user_seller_id = $objc->seller->loaded() ? array($objc->seller->pk()=>$objc->seller->username) : array();
		
		$can_edit_field = ($objc->status == 'ELABORATING') ? FALSE : $objc->loaded();
		
		$fields = array(
			'id'						=> array('type'=>'text',     'show'=>'loaded',	'disabled'=>true),
			'moment_creation'			=> array('type'=>'date',     'show'=>'loaded',	'disabled'=>$can_edit_field),
			'user_seller_id'			=> array('type'=>'select',   'show'=>'loaded',	'disabled'=>true, 'options'=>$user_seller_id),
			'user_designer_id'			=> array('type'=>'select',   'show'=>'loaded',	'disabled'=>true, 'options'=>NULL),
			'user_prepress_id'			=> array('type'=>'select',   'show'=>'loaded',	'disabled'=>true, 'options'=>NULL),
			'client_id'					=> array('type'=>'select',	 'show'=>'aways',	'disabled'=>$can_edit_field, 'options'=>$clients),
			'destination'				=> array('type'=>'select',   'show'=>'aways',	'disabled'=>$can_edit_field, 'options'=>Model_Order::$_destination),
			'book_id'					=> array('type'=>'select',	 'show'=>'aways',	'disabled'=>$can_edit_field, 'options'=>$books),
			'format_id'					=> array('type'=>'select',	 'show'=>'aways',	'disabled'=>$can_edit_field, 'options'=>$formats),
			'title'						=> array('type'=>'text',     'show'=>'aways',	'disabled'=>false),
			'description_ad'			=> array('type'=>'textarea', 'show'=>'aways',	'disabled'=>$can_edit_field),
			'text_ad'					=> array('type'=>'textarea', 'show'=>'aways',	'disabled'=>$can_edit_field),
			'color'						=> array('type'=>'radio',    'show'=>'aways',	'disabled'=>$can_edit_field, 'options'=>Model_Order::$_colors),
			'type_ad'					=> array('type'=>'radio',    'show'=>'aways',	'disabled'=>$can_edit_field, 'options'=>Model_Order::$_types_ad),
			'date_publication'			=> array('type'=>'date',     'show'=>'aways',	'disabled'=>$can_edit_field),
			'status'					=> array('type'=>'select',   'show'=>'aways',	'disabled'=>true, 'options'=>Model_Order::$_status),
			'moment_taken_designer'		=> array('type'=>'date',     'show'=>'loaded',	'disabled'=>$can_edit_field),
			'moment_taken_prepressor'	=> array('type'=>'date',     'show'=>'loaded',	'disabled'=>$can_edit_field),
			'moment_closed'				=> array('type'=>'date',     'show'=>'loaded',	'disabled'=>$can_edit_field),
		);
		
		$form = Util_Cupcakeform::factory($objc, $errros, $resmsg, $fields);
							
		$this->_response_render(null, View::factory(Kohana::$config->load('system.admin_theme') . '/admin_order_form', array_merge($this->profile(), array('form'=>$form)))->render());
	}

	public function action_book_formats()
	{
		$this->check_permissions(Kohana::$config->load('rights.RIGHT_ORDER_READ.value'), Kohana::$config->load('rights.RIGHT_ORDER_EDIT.value'));
		
		$book = ORM::factory('Book',$this->request->param('id'));
		$formats = ORM::factory('BookFormat')->where('book_id','=',$this->request->param('id'))->order_by('id','asc')->find_all();
		
		if(count($formats)>0)
		{
			$data = array();
			foreach($formats->as_array('id') as $key=>$format)
			{
				$data[$key] = "{$format->name} (larg. {$format->width}cm)";
			}
// 			$this->response->body(json_encode($data))->headers('content-type', 'application/json');
			$this->response->body(json_encode(array('book'=>$book->as_array(),'formats'=>$data)))->headers('content-type', 'application/json');
			
		}
		else
		{
			throw new Oca_Exception('Invalid book id');
		}
	}

	public function action_download_raw_image()
	{
		$this->check_permissions(Kohana::$config->load('rights.RIGHT_ORDER_READ.value'), Kohana::$config->load('rights.RIGHT_ORDER_EDIT.value'));
		
		$objc = ORM::factory('Order', $this->request->param('id'));
		
		$result = array('success'=>NULL,'message'=>NULL,'file'=>NULL);
		
		try
		{
			if($objc->loaded())
			{
				if($objc->own_preprint())
				{
					$treatments = $objc->treatments->where('status','=','ACCEPTED')->find();
					
					$result = array('success'=>TRUE,'message'=>NULL,'file'=>$treatments->attachment_raw_path);
				}
				else
				{
					$result = array('success'=>FALSE,'message'=>'Nao foi possivel atribuir a ordem ao seu usuario','file'=>NULL);
				}
			}
			else
			{
				$result = array('success'=>FALSE,'message'=>'Numero de OCA invalido ou inexistente','file'=>NULL);
			}
		}
		catch(Exception $e)
		{
			$result = array('success'=>FALSE,'message'=>$e->getmessage(),'file'=>NULL);
		}
		
		$this->response->body(json_encode($result))->headers('content-type', 'application/json');
	}
	
	
	
}