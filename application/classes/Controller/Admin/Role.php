<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Role extends Controller_Admin_Base {
	
	public static function rights()
	{
		return array(
			'url' => URL::site(Route::get('default')->uri(array('directory'=>'admin', 'controller'=>'Role', 'action'=>'list'))),
			'icon' => '/media/cupcake/img/icons/sidemenu/content.png',
			'label' => 'Papéis de usuários',
			'order' => '100900',
			'controller' => 'Role',
			'requires'=>Kohana::$config->load('rights.RIGHT_ROLE_READ.value'),
		);
	}

	public static function profile()
	{
		return array(
			'page_title' => _('PAPÉIS'),
			'page_subtitle' => _('Aqui você pode administrar todos os papéis.'),
			'page_back_label' => _('Voltar para listagem'),
			'titleh_form' => _('Formulário de papéis'),
			'page_url_list' => URL::site(Route::get('default')->uri(array('directory'=>'admin', 'controller'=>'Role', 'action'=>'list'))),
			'page_url_item' => URL::site(Route::get('default')->uri(array('directory'=>'admin', 'controller'=>'Role', 'action'=>'edit'))),
		);
	}

	public function action_list()
	{
		$this->check_permissions(Kohana::$config->load('rights.RIGHT_ROLE_READ.value'));

		$show = array('name','description');
		$lbls = ORM::factory('Role')->labels();
		$list = ORM::factory('Role')->order_by($lof = 'id', $lod = 'desc');
		$lurl = URL::site(Route::get('default')->uri(array('directory'=>'admin', 'controller'=>'Role', 'action'=>'edit')));
		
		if($resmsg = Session::instance()->get('result_message', null))
		{
			Session::instance()->delete('result_message');
		}

		if(isset($_GET['search']))
		{
			$s = htmlentities($_GET['search']);
			$list = $list->where('name','like',"%{$s}%")->or_where('description','like',"%{$s}%");
		}
		
		$fields_options = array(
//			'product_id' => $this->list_to_options(ORM::factory('Product')->find_all(), 'name')
		);
		
		$prms = array_merge($this->profile(), array('list'=>$list=$list->find_all(), 'fields'=>$show, 'fields_options'=>$fields_options, 'labels'=>$lbls, 'url'=>$lurl, 'list_order_field'=>$lof, 'list_order_drctn'=>$lod, 'pagination'=>Util_Pagination::factory($list->count()), 'message'=>$resmsg));

		$this->_response_render(null, View::factory(Kohana::$config->load('system.admin_theme') . '/admin_roles_list', $prms)->render());
	}

	public function action_edit()
	{
		$this->check_permissions(Kohana::$config->load('rights.RIGHT_ROLE_READ.value'), Kohana::$config->load('rights.RIGHT_ROLE_EDIT.value'));
		
		$errros = null;
		
		$objc = ORM::factory('Role', $this->request->param('id'));
		
		if( (! $objc->loaded() && $this->request->param('id')))
		{
			throw new HTTP_Exception_404('Not found');
		}

		if($resmsg = Session::instance()->get('result_message', null))
		{
			Session::instance()->delete('result_message');
		}
		
		if($_POST)
		{
			try
			{
				$rights_list = Arr::get($_POST,'rights_list', array());
				
				if(isset($_POST['button-delete']))
				{
					return $this->action_delete();
				}
				
				if(! $objc->loaded())
				{
					Session::instance()->set('result_message', array('success'=>true, 'message'=>_('Regra adicinada com sucesso')));
					$objc->values(Arr::extract($_POST, array('name','description')));
					$objc->save();					
				}
				else{
					Session::instance()->set('result_message', array('success'=>true, 'message'=>_('Regra atualizada com sucesso')));
				}
				
				$rights_list = $objc->update_rights_list($rights_list);
				$this->redirect( URL::site(Route::get( 'default' )->uri(array('directory'=>'admin','controller'=>'role','action'=>'edit','id'=>"{$objc->pk()}"))) );
			}
			catch(ORM_Validation_Exception $e)
			{
				$errros = $e->errors('models');
				$resmsg = array('success'=>false, 'message'=>_('Erros em ') . implode(', ', array_values(Arr::extract($objc->labels(), array_keys($errros)))));
			}
		}

		if($objc->loaded())
		{
			
		$rights_list = $objc->get_rights_list();
		
		$form = Util_Cupcakeform::factory($objc, $errros, $resmsg, array(
					'name'        => array('type'=>'text', 'show'=>'aways',  'disabled'=>false),
					'description' => array('type'=>'text', 'show'=>'aways',  'disabled'=>false),
					'rights_list' => array('type'=>'swap', 'show'=>'loaded', 'disabled'=>false, '' .
											'target_label'=>_('Permissões selecionadas'), 'target_options'=>$rights_list, 
											'origin_label'=>_('Permissões disponíveis'), 'origin_options'=>Kohana::$config->load('rights')->as_array() ),
					));
					
		}
		else
		{
			
		$form = Util_Cupcakeform::factory($objc, $errros, $resmsg, array(
					'name'        => array('type'=>'text', 'show'=>'aways',  'disabled'=>false),
					'description' => array('type'=>'text', 'show'=>'aways',  'disabled'=>false),
					));			
		}

		$this->_response_render(null, View::factory(Kohana::$config->load('system.admin_theme') . '/admin_default_form', array_merge($this->profile(), array('form'=>$form)))->render());
	}
	
	public function action_delete()
	{
		$this->check_permissions(Kohana::$config->load('rights.RIGHT_ROLE_DELETE.value'));

		$objc = ORM::factory('Role', $this->request->param('id'));
		
		if($objc->loaded())
		{
			try
			{
				$objc->delete();
				Session::instance()->set('result_message', array('success'=>true, 'message'=>_('Regra excluida com sucesso.')));
			}
			catch(ErrorException $e)
			{
				$errros = $e->errors('models');
				Session::instance()->set('result_message', array('success'=>false, 'message'=>$e->getmessage() ));
				$this->redirect( URL::site(Route::get( 'default' )->uri(array('directory'=>'admin','controller'=>'role','action'=>'edit','id'=>"{$objc->pk()}"))) );
			}
		}

		$this->redirect( URL::site(Route::get( 'default' )->uri(array('directory'=>'admin','controller'=>'role','action'=>'list'))) );
	}

} 
