<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Base extends Controller {
	
	protected $permissions_were_checked = false;
	
	public static function rights()
	{
		return null;
	}
	
	protected function check_permissions()
	{
		$this->permissions_were_checked = true;
		
		if(Auth::instance()->logged_in())
		{
			$user = Auth::instance()->get_user();
			
			$rgts = ORM::factory('RoleRight')->where('role_id','in', array_keys($user->roles->find_all()->as_array('id')))->find_all()->as_array('right');
			
			$rgts = array_intersect(array_keys($rgts), func_get_args());  
			
			if($rgts)
			{
				return $user;
			}
			else
			{
				throw new Oca_Exception('The logged user has no required rights to access this page');
			}
		}
		else
		{
			$continue = '?continue_to='.$_SERVER['REQUEST_URI'];
			$this->redirect(URL::site(Route::get('default')->uri(array('directory'=>'admin','controller'=>'login'))).$continue);
		}
		return null;
	}
	
	protected function _response_render($user, $inner_content)
	{
		$params = array('page_header' => View::factory(Kohana::$config->load('system.admin_theme') . '/_page_title', array('title'=>'Esportes :: Admin', 'desc'=>'', 'user'=>$user)));
								
		$base_params = array(
			'inner_view' => View::factory(Kohana::$config->load('system.admin_theme') . '/_grid_default', array(
				'inner_view'=>$inner_content, 
				'request'=>$this->request, 
				'user'=>Auth::instance()->get_user()))
		);
		
		$this->response->body(View::factory(Kohana::$config->load('system.admin_theme') . '/_base', $base_params)->render());
	}
	
	public function list_to_options($objects_list, $field_name, $labeled=FALSE)
	{
		$out = array();
		foreach($objects_list as $item)
		{
			$out[$item->pk()] = ($labeled)?array('value'=>$item->pk(),'label'=>$item->$field_name):$item->$field_name;
		}
		return $out;
	}


	public function action_message()
	{
		$base_params = array(
			'inner_view' => View::factory(Kohana::$config->load('system.admin_theme') . '/_grid_wide', 
								array(
									'inner_view'=> 1, #View::factory(Kohana::$config->load('system.admin_theme') . '/secao_action_history', $params), 
									'request'=>$this->request, 
									'user'=>Auth::instance()->get_user()
								)
							)
		);
		
		$this->response->body(View::factory(Kohana::$config->load('system.admin_theme') . '/_base', $base_params)->render());
	}
	
	public function action_error($code=500, $message=null)
	{
		$base_params = array(
			'inner_view' => View::factory(Kohana::$config->load('system.admin_theme') . '/_grid_wide', 
								array(
									'inner_view'=> View::factory(Kohana::$config->load('system.admin_theme') . '/_error_page', array( 'status'=>$code, 'message'=>$message )),
								)
							)
		);
		
		$this->response->body(View::factory(Kohana::$config->load('system.admin_theme') . '/_base', $base_params)->render());
	}
	
	public function after()
	{
		if( ! $this->permissions_were_checked)
		{
			throw new HTTP_Exception_500(_('An stupid developer forgot to check user rights'));
		}
	}
	
}
