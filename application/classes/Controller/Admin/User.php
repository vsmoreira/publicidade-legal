<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_User extends Controller_Admin_Base {
	
	public static function rights()
	{
		return array(
			'url' => URL::site(Route::get('default')->uri(array('directory'=>'admin', 'controller'=>'User', 'action'=>'list'))),
			'icon' => '/media/cupcake/img/icons/sidemenu/content.png',
			'label' => 'Usuários',
			'order' => '100800',
			'controller' => 'User',
			'requires'=>Kohana::$config->load('rights.RIGHT_USER_READ.value'),
		);
	}

	public static function profile()
	{
		return array(
			'page_title' => _('USUÁRIOS'),
			'page_subtitle' => _('Aqui você pode administrar todos os usuários deste sistema.'),
			'page_back_label' => _('Voltar para listagem'),
			'titleh_form' => _('Formulário de usuários'),
			'page_url_list' => URL::site(Route::get('default')->uri(array('directory'=>'admin', 'controller'=>'User', 'action'=>'list'))),
			'page_url_item' => URL::site(Route::get('default')->uri(array('directory'=>'admin', 'controller'=>'User', 'action'=>'edit'))),
		);
	}

	public function action_list()
	{
		$this->check_permissions(Kohana::$config->load('rights.RIGHT_USER_READ.value'));

		$lbls = ORM::factory('User')->labels();
		$list = ORM::factory('User')->where('status','=','ACTIVE')->order_by($lof = 'registration', $lod = 'desc');
		$lurl = URL::site(Route::get('default')->uri(array('directory'=>'admin', 'controller'=>'User', 'action'=>'edit')));
		
		if($resmsg = Session::instance()->get('result_message', null))
		{
			Session::instance()->delete('result_message');
		}

		if(isset($_GET['search']))
		{
			$s = htmlentities($_GET['search']);
			$list = $list->where('username','like',"%{$s}%")->or_where('email','like',"%{$s}%");
		}
		
		$list=$list->order_by('registration','desc')->find_all();
		
		$prms = array_merge($this->profile(), array('list'=>$list, 'labels'=>$lbls, 'url'=>$lurl, 'list_order_field'=>$lof, 'list_order_drctn'=>$lod, 'pagination'=>Util_Pagination::factory($list->count()), 'message'=>$resmsg));

		$this->_response_render(null, View::factory(Kohana::$config->load('system.admin_theme') . '/admin_users_inedex', $prms)->render());
	}

	public function action_edit()
	{
		$this->check_permissions(Kohana::$config->load('rights.RIGHT_USER_READ.value'), Kohana::$config->load('rights.RIGHT_USER_EDIT.value'));
		
		$errros = null;
		
		$objc = ORM::factory('User', $this->request->param('id'));
		
		if( (! $objc->loaded() && $this->request->param('id')) || ($objc->loaded() && $objc->status=='DELETED'))
		{
			throw new HTTP_Exception_404('Not found');
		}
		else
		{
			$roles_list = array_keys($objc->roles->find_all()->as_array('id'));
		}

		if($resmsg = Session::instance()->get('result_message', null))
		{
			Session::instance()->delete('result_message');
		}
		
		if($_POST)
		{
			try
			{
				$roles_list = Arr::get($_POST,'roles_list', array());
				
				if(isset($_POST['button-delete']))
				{
					return $this->action_delete();
				}
				if(! $objc->loaded())
				{
//					$newpasswd = hash('crc32', rand(), true);
					$newpasswd = crc32(rand());
					$objc->registration = date('Y-m-d H:i:s');
					$objc->password = $newpasswd;
					$objc->values(Arr::extract($_POST, array('username','email','name','type')));
					$objc->receive_avatar('avatar','avatar_delete',FALSE);
					$objc->save();
					
					$objc->send_confirmation_mail($newpasswd);
					
					Session::instance()->set('result_message', array('success'=>true, 'message'=>_('Usuário adicionado com sucesso.')));
				}
				else
				{
					$objc->values(Arr::extract($_POST, array('name','type')));
					$objc->save();

					Session::instance()->set('result_message', array('success'=>true, 'message'=>_('Usuário atualizado com sucesso.')));
				}
				
				$objc->receive_avatar('avatar','avatar_delete',FALSE);
				
				$rights_list = $objc->update_roles_list($roles_list);
				
				$this->redirect( URL::site(Route::get( 'default' )->uri(array('directory'=>'admin','controller'=>'user','action'=>'edit','id'=>"{$objc->pk()}"))) );
			}
			catch(ORM_Validation_Exception $e)
			{
				$errros = $e->errors('models');
				$resmsg = array('success'=>false, 'message'=>_('Erros em ') . implode(', ', array_values(Arr::extract($objc->labels(), array_keys($errros)))));
			}
		}
		
		$form = Util_Cupcakeform::factory($objc, $errros, $resmsg, array(
					'id'           => array('type'=>'text',     'show'=>'never',  'disabled'=>true),
					'status'       => array('type'=>'radio',    'show'=>'never',  'disabled'=>false, 'options'=>$objc->_status),
					'name'			=> array('type'=>'text',     'show'=>'aways',  'disabled'=>false),
					'username'     => array('type'=>'text',     'show'=>'aways',  'disabled'=>false),
					'email'        => array('type'=>'text',     'show'=>'aways',  'disabled'=>false),
					'type'     	   => array('type'=>'select',   'show'=>'aways', 'disabled'=>false, 'options'=>$objc->_types),
					'avatar'       => array('type'=>'image',    'show'=>'aways',  'disabled'=>false),
					'password'     => array('type'=>'password', 'show'=>'never',  'disabled'=>true),
					'registration' => array('type'=>'date',     'show'=>'loaded', 'disabled'=>true),
					'last_login'   => array('type'=>'date',     'show'=>'loaded', 'disabled'=>true),
					'logins'       => array('type'=>'text',     'show'=>'loaded', 'disabled'=>true),
					'roles_list'   => array('type'=>'swap', 'show'=>'loaded', 'disabled'=>false, '' .
											'target_label'=>_('Selected roles'), 'target_options'=>$roles_list, 
											'origin_label'=>_('Available roles'), 'origin_options'=>$this->list_to_options(ORM::factory('Role')->find_all(), 'name', true) ),
					));
					
		$this->_response_render(null, View::factory(Kohana::$config->load('system.admin_theme') . '/admin_default_form', array_merge($this->profile(), array('form'=>$form)))->render());

	}

	public function action_delete()
	{
		$this->check_permissions(Kohana::$config->load('rights.RIGHT_USER_DELETE.value'));

		$objc = ORM::factory('User', $this->request->param('id'));
		
		if($objc->loaded())
		{
			try
			{
				$objc->username = substr('DLTD'.date('YmdHis').'.'.$objc->username, 0, 32);
				$objc->email = substr('DLTD'.date('YmdHis').'.'.$objc->email, 0, 64);
				$objc->status = 'DELETED';
				$objc->save();
				
				Session::instance()->set('result_message', array('success'=>true, 'message'=>_('Usuário excluido com sucesso.')));
			}
			catch(Exception $e)
			{
				$errros = $e->errors('models');
				Session::instance()->set('result_message', array('success'=>false, 'message'=>$e->getmessage() .': '. implode(', ', array_values(Arr::extract($objc->labels(), array_keys($errros))))));
				$this->redirect( URL::site(Route::get( 'default' )->uri(array('directory'=>'admin','controller'=>'user','action'=>'edit','id'=>"{$objc->pk()}"))) );
			}
		}

		$this->redirect( URL::site(Route::get( 'default' )->uri(array('directory'=>'admin','controller'=>'user','action'=>'list'))) );
	}

} 
