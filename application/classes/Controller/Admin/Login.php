<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Login extends Controller
{
	
	public function __construct(Request $request, Response $response)
	{
		parent::__construct($request, $response);
	}
	
	public function action_index()
	{
		$result = null;
		
		if($_POST)
		{
			try
			{
				$post = Validation::factory(array('username'=>Arr::get($_POST,'login'), 'password'=>Arr::get($_POST,'senha')))
					->rule('username', 'not_empty')
					->rule('password', 'not_empty')
					->rule('password', 'min_length', array(':value', 3));
					
				if ($post->check())
				{
					if(Auth::instance()->login($post['username'], $post['password'], FALSE))
					{
						#return Auth::instance()->get_user();
						$goto = Arr::get($_GET,'continue_to', URL::site(Route::get( 'default' )->uri(array('directory'=>'admin'))));
						$this->redirect( $goto );
						$result = array('success'=>TRUE, 'message'=>'Sucesso ao logar-se');
					} else {
						$result = array('success'=>FALSE, 'message'=>_('Usuário ou senha inválidos'));
					}
				}
			}
			catch(ORM_Validation_Exception $e)
			{
				$result = array('success'=>FALSE, 'message'=>$e->getmessage());
			}
			catch(ErrorException $e)
			{
				$result = array('success'=>FALSE, 'message'=>$e->getmessage());
			}
		}
		
		$base_params = array('inner_view' => View::factory(Kohana::$config->load('system.admin_theme') . '/secao_action_login',array('message'=>$result)));
		
		$this->response->body(View::factory(Kohana::$config->load('system.admin_theme') . '/_base', $base_params)->render());
	}
	
	public function action_recuperar_senha()
	{
		if(Auth::instance()->get_user())
		{
			throw new Oca_Exception('Apenas usuários não logados pode acessar esta área');
		}
		
		if($this->request->param('id'))
		{
			return $this->action_recuperar_senha_validacao();
		}
		
		$result = NULL;
		
		if($_POST)
		{
			$login = Arr::get($_POST,'login');
			
			if($login)
			{
				$user = ORM::factory('User')->where('email','=',$login)->find();
				
				if($user->loaded())
				{
					try
					{
						$user->start_password_recover_process();
						$result = array('success'=>TRUE, 'message'=>'Dentro de instantes você receberá um email orientando-o a como recuperar sua senha');
					}
					catch(ORM_Validation_Exception $e)
					{
						$errros = $e->errors('models');
						$result = array('success'=>false, 'message'=>'<ul><li>'.implode('</li><li>',$errros).'</li></ul>');
					}	
					catch(Oca_Exception $e)
					{
						$result = array('success'=>false, 'message'=>$e->getmessage());
					}
				}
				else
				{
					$result = array('success'=>FALSE, 'message'=>'Email não cadastrado');
				}
			}
			else
			{
				$result = array('success'=>FALSE, 'message'=>'O email é obrigatório');
			}
		}
		
		$base_params = array('inner_view' => View::factory(Kohana::$config->load('system.admin_theme') . '/secao_action_login_recuperar_senha',array('message'=>$result)));
		
		$this->response->body(View::factory(Kohana::$config->load('system.admin_theme') . '/_base', $base_params)->render());
	}

	public function action_recuperar_senha_validacao()
	{
		if(Auth::instance()->get_user())
		{
			throw new Oca_Exception('Apenas usuários não logados pode acessar esta área');
		}

		$result = NULL;
		$user = NULL;
		
		try
		{
			$user = Model_User::password_recover_process_continue($this->request->param('id'));
			
			if($_POST)
			{
				$user->password_recover_process_finish($_POST);
				$result = array('success'=>TRUE, 'message'=>'Senha alterada com sucesso. <a href="/admin">Clique aqui para fazer login<a>');
			}
		}
		catch(Oca_Exception $e)
		{
			$result = array('success'=>FALSE, 'message'=>$e->getmessage());			
		}
		
		$base_params = array('inner_view' => View::factory(Kohana::$config->load('system.admin_theme') . '/secao_action_login_recuperar_senha_continue',array('message'=>$result, 'user'=>$user)));
		
		$this->response->body(View::factory(Kohana::$config->load('system.admin_theme') . '/_base', $base_params)->render());
	}
	
}
