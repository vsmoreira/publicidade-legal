<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Index extends Controller_Admin_Base {

	public function action_index()
	{
		$this->check_permissions('RIGHT_DASHBOARD_VIEW');
		
		$this->_response_render(null, View::factory(Kohana::$config->load('system.admin_theme') . '/admin_dashboard', array())->render());
	}
	
} 
